<?php

/*
 * Copyright 2013-2015, Theia Sticky Sidebar, WeCodePixels, http://wecodepixels.com
 */

class TssAdmin_General {
	public function echoPage() {
		if ( TssTemplates::getTemplate() !== null && TssTemplates::areSettingsDifferentFromTemplate() ) {
			if ( isset( $_POST['usePredefinedSettings'] ) ) {
				TssTemplates::useTemplate();
			} else {
				?>
				<div class="updated" style="margin: 14px 0 0 0; padding: 10px;">
					<form method="post" action="options-general.php?page=tss">
						There are predefined settings available for the current theme.
						<input type="submit"
						       name="usePredefinedSettings"
						       class="button-primary"
						       value="Use predefined settings"
						       style="margin-left: 5px">
					</form>
				</div>
			<?php
			}
		}
		?>

		<form method="post" action="options.php">
			<?php
			settings_fields( 'tss_options_general' );
			$options = get_option( 'tss_general' );
			$options = is_array( $options ) ? $options : array();
			?>
			<input type="hidden" name="tss_general[currentTheme]" value="<?php echo wp_get_theme()->Name ?>">

			<table class="form-table">
				<tr valign="top">
					<th scope="row">
						<label for="tss_sidebarSelector"><?php _e( "Sidebar CSS selector<p class='description'>(leave blank to try to guess automatically)</p>", 'theia-sticky-sidebar' ); ?></label>
					</th>
					<td>
						<input type="text"
						       id="tss_sidebarSelector"
						       name="tss_general[sidebarSelector]"
						       value="<?php echo $options['sidebarSelector']; ?>"
						       style="width: 400px">

						<p class="description">
							This is the selector used to identify the sidebar(s). It's usually something like "#sidebar"
							or "#secondary". You can add multiple selectors by separating them with a comma ",".
						</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="tss_containerSelector"><?php _e( "Container CSS selector<p class='description'>(leave blank to try to guess automatically)</p>", 'theia-sticky-sidebar' ); ?></label>
					</th>
					<td>
						<input type="text"
						       id="tss_containerSelector"
						       name="tss_general[containerSelector]"
						       value="<?php echo $options['containerSelector']; ?>"
						       style="width: 400px">

						<p class="description">
							This must identify the element that contains both the sidebar(s) and the page content. It's
							usually something like "#main" or "#main-content".
						</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="tss_additionalMarginTop"><?php _e( "Sidebar additional top margin (px)", 'theia-sticky-sidebar' ); ?></label>
					</th>
					<td>
						<input type="text"
						       id="tss_additionalMarginTop"
						       name="tss_general[additionalMarginTop]"
						       value="<?php echo $options['additionalMarginTop']; ?>">
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="tss_additionalMarginBottom"><?php _e( "Sidebar additional bottom margin (px)", 'theia-sticky-sidebar' ); ?></label>
					</th>
					<td>
						<input type="text"
						       id="tss_additionalMarginBottom"
						       name="tss_general[additionalMarginBottom]"
						       value="<?php echo $options['additionalMarginBottom']; ?>">
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="tss_minWidth"><?php _e( "Minimum width (px)", 'theia-sticky-sidebar' ); ?></label>
					</th>
					<td>
						<input type="text"
						       id="tss_minWidth"
						       name="tss_general[minWidth]"
						       value="<?php echo $options['minWidth']; ?>">

						<p class="description">
							The plugin will be disabled when the user's window width is below this value. Useful for
							responsive themes.
						</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<?php _e( "Troubleshooting:", 'theia-sticky-sidebar' ); ?>
					</th>
					<td>
						<p>
							<label>
								<?
								$checked = array_key_exists( 'updateSidebarHeight', $options ) && $options['updateSidebarHeight'];
								?>
								<input type='hidden' value='false' name='tss_general[updateSidebarHeight]'>
								<input type="checkbox"
								       value="true"
								       id="tss_updateSidebarHeight"
								       name="tss_general[updateSidebarHeight]"<?php echo $checked ? ' checked' : ''; ?>>
								Update sidebar height
							</label>
						</p>

						<p class="description">
							Try this if the sidebar loses its background color.
						</p>
						<p>
							<label>
								<?
								$checked = array_key_exists( 'disableOnHomePage', $options ) && $options['disableOnHomePage'];
								?>
								<input type='hidden' value='false' name='tss_general[disableOnHomePage]'>
								<input type="checkbox"
								       value="true"
								       id="tss_disableOnHomePage"
								       name="tss_general[disableOnHomePage]"<?php echo $checked ? ' checked' : ''; ?>>
								Disable on the home page
							</label>
						</p>

						<p>
							<label>
								<?
								$checked = array_key_exists( 'disableOnCategoryPages', $options ) && $options['disableOnCategoryPages'];
								?>
								<input type='hidden' value='false' name='tss_general[disableOnCategoryPages]'>
								<input type="checkbox"
								       value="true"
								       id="tss_disableOnCategoryPages"
								       name="tss_general[disableOnCategoryPages]"<?php echo $checked ? ' checked' : ''; ?>>
								Disable on category pages
							</label>
						</p>

						<p>
							<label>
								<?
								$checked = array_key_exists( 'disableOnPages', $options ) && $options['disableOnPages'];
								?>
								<input type='hidden' value='false' name='tss_general[disableOnPages]'>
								<input type="checkbox"
								       value="true"
								       id="tss_disableOnPages"
								       name="tss_general[disableOnPages]"<?php echo $checked ? ' checked' : ''; ?>>
								Disable on pages
							</label>
						</p>

						<p>
							<label>
								<?
								$checked = array_key_exists( 'disableOnPosts', $options ) && $options['disableOnPosts'];
								?>
								<input type='hidden' value='false' name='tss_general[disableOnPosts]'>
								<input type="checkbox"
								       value="true"
								       id="tss_disableOnPosts"
								       name="tss_general[disableOnPosts]"<?php echo $checked ? ' checked' : ''; ?>>
								Disable on posts
							</label>
						</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<?php _e( "Custom CSS:", 'theia-sticky-sidebar' ); ?>
					</th>
					<td>
						<textarea class="large-text code" rows="10" name="tss_general[customCss]"><?php echo $options['customCss']; ?></textarea>
					</td>
				</tr>
			</table>
			<p class="submit">
				<input type="submit"
				       class="button-primary"
				       value="<?php _e( 'Save Changes', 'theia-sticky-sidebar' ) ?>" />
			</p>
		</form>
	<?php
	}
}