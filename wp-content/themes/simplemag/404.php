<?php 
/**
 * 404 error page
 *
 * @package SimpleMag
 * @since 	SimpleMag 1.0
**/ 
get_header(); ?>
	
    <section id="content" role="main" class="clearfix animated">
    
    	<div class="wrapper">
    
            <article id="post-0" class="post error404 not-found">

Muzul must have it. Search again:</br><br>

<?php echo do_shortcode('[wpbsearch]'); ?>
                
                
            </article><!-- #post-0 .post .error404 .not-found -->
            
    	</div>
        
    </section><!-- #content -->
	
<?php get_footer(); ?>