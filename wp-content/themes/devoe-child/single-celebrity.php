<?php get_header(); ?>

<?php

						$photo = get_field('photo');
						$born_on = get_field('born_on');
						$birthplace = get_field('birthplace');
						$claim_to_fame = get_field('claim_to_fame');
						$nickname= get_field('nickname');
						$text = get_field('text');
						$place_of_birth = get_field('place_of_birth');
						$birth_date = get_field('birth_date');

		                                $celebname_field = get_field_object('celebname');
						$celebname_value = get_field('celebname');
						$celebname_label = $celebname_field['choices'][ $celebname_value ];

						// fill 'profession' variable
						//
						$profession_field = get_field_object('profession');
						$profession_value = get_field('profession');
						$profession_label = '';

						if(count($profession_value) < 2) {
						$profession_label = $profession_field['choices'][ $profession_value[0] ];
						} else {
						foreach($profession_value as $val) {
						$profession_label .= $profession_field['choices'][ $val ] . ' ';
						}
						}

// fill 'colors' variable
						//
						$colors_field = get_field_object('colors');
						$colors_value = get_field('colors');
						$colors_label = '';

						if(count($colors_value) < 2) {
						$colors_label = $colors_field['choices'][ $colors_value[0] ];
						} else {
						foreach($colors_value as $val) {
						$colors_label .= $colors_field['choices'][ $val ] . ' ';
						}
						}




// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

						// fill 'hairstyle' variable
						//
						$hairstyle_field = get_field_object('hairstyle');
						$hairstyle_value = get_field('hairstyle');
						$hairstyle_label = '';

						if(count($hairstyle_value) < 2) {
						$hairstyle_label = $hairstyle_field['choices'][ $hairstyle_value[0] ];
						} else {
						foreach($hairstyle_value as $val) {
						$hairstyle_label .= $hairstyle_field['choices'][ $val ] . ', ';
						}
						}

// fill 'hair_length' variable
						//
						$hair_length_field = get_field_object('hair_length');
						$hair_length_value = get_field('hair_length');
						$hair_length_label = '';

						if(count($hair_length_value) < 2) {
						$hair_length_label = $hair_length_field['choices'][ $hair_length_value[0] ];
						} else {
						foreach($hair_length_value as $val) {
						$hair_length_label .= $hair_length_field['choices'][ $val ] . ', ';
						}
						}

// fill 'make_up_style' variable
						//
						$make_up_style_field = get_field_object('make_up_style');
						$make_up_style_value = get_field('make_up_style');
						$make_up_style_label = '';

						if(count($make_up_style_value) < 2) {
						$make_up_style_label = $make_up_style_field['choices'][ $make_up_style_value[0] ];
						} else {
						foreach($make_up_style_value as $val) {
						$make_up_style_label .= $make_up_style_field['choices'][ $val ] . ', ';
						}
						}
						

	     // fill 'hair_type' variable
						//
						$hair_type_field = get_field_object('hair_type');
						$hair_type_value = get_field('hair_type');
						$hair_type_label = $hair_type_field['choices'][ $hair_type_value[0] ];

						
						//
						// get age from birth date
						//						     
						$date = new DateTime($birth_date);
						$now = new DateTime();
						$interval = $now->diff($date);
						$age = $interval->y;
						
						//
						// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

// fill 'nationality' variable
						//
						$nationality_field = get_field_object('nationality');
						$nationality_value = get_field('nationality');
						$nationality_label = '';

						if(count($nationality_value) < 2) {
						$nationality_label = $nationality_field['choices'][ $nationality_value[0] ];
						} else {
						foreach($nationality_value as $val) {
						$nationality_label .= $nationality_field['choices'][ $val ] . '- ';
						}
						}

						//
						// fill 'descript' variable
						//
						$descript_field = get_field_object('descript');
						$descript_value = get_field('descript');
						$descript_label = '';

						if(count($descript_value) < 2) {
						$descript_label = $descript_field['choices'][ $descript_value[0] ];
						} else {
						foreach($descript_value as $val) {
						$descript_label .= $descript_field['choices'][ $val ] . ' ';;
						}
						}

						// fill 'face' variable
						//
						$face_field = get_field_object('face');
						$face_value = get_field('face');
						$face_label = $face_field['choices'][ $face_value[0] ];

						//
						// fill 'eye' variable
						//
						$eye_field = get_field_object('eye');
						$eye_value = get_field('eye');
						$eye_label = $eye_field['choices'][ $eye_value[0] ];
						
						//
						// fill 'hair' variable
						//
						$hair_field = get_field_object('hair');
						$hair_value = get_field('hair');
						$hair_label = $hair_field['choices'][ $hair_value[0] ];

//
						// fill 'tone' variable
						//
						$tone_field = get_field_object('tone');
						$tone_value = get_field('tone');
						$tone_label = $tone_field['choices'][ $tone_value[0] ];


						// fill 'body' variable
						//
						$body_field = get_field_object('body');
						$body_value = get_field('body');
						$body_label = $body_field['choices'][ $body_value[0] ];

			// fill 'skin_type' variable
						//
						$skin_type_field = get_field_object('skin_type');
						$skin_type_value = get_field('skin_type');
						$skin_type_label = $skin_type_field['choices'][ $skin_type_value[0] ];

// fill 'love' variable
						//
						$love_field = get_field_object('love');
						$love_value = get_field('love');
						$love_label = '';

						if(count($love_value) < 2) {
						$love_label = $love_field['choices'][ $love_value[0] ];
						} else {
						foreach($love_value as $val) {
						$love_label .= $love_field['choices'][ $val ] . '- ';
						}
						}


// fill 'extra' variable
						//
						$extra_field = get_field_object('extra');
						$extra_value = get_field('extra');
						$extra_label = '';

						if(count($extra_value) < 2) {
						$extra_label = $love_field['choices'][ $extra_value[0] ];
						} else {
						foreach($extra_value as $val) {
						$extra_label .= $extra_field['choices'][ $val ] . '- ';
						}
						}

// fill 'distinct_feature' variable
						//
						$distinct_feature_field = get_field_object('distinct_feature');
						$distinct_feature_value = get_field('distinct_feature');
						$distinct_feature_label = '';

						if(count($distinct_feature_value) < 2) {
						$distinct_feature_label = $distinct_feature_field['choices'][ $distinct_feature_value[0] ];
						} else {
						foreach($distinct_feature_value as $val) {
						$distinct_feature_label .= $distinct_feature_field['choices'][ $val ] . '- ';
						}
						}





      $height = get_field('height');
						$weight = get_field('weight');
      $endorsements= get_field('endorsements');
						$brands= get_field('brands');
						$hairstyle_photo= get_field('hairstyle_photo');
						$make_up_photo= get_field('make_up_photo');
						$no_make_up_photo= get_field('no_make_up_photo');
						$net_worth= get_field('net_worth');
						$salary= get_field('salary');
						$colleagues= get_field('colleagues');
						$management= get_field('management');
						$fam_photo= get_field('fam_photo');
						$current_partner_text= get_field('current_partner_text');
      $style_photo= get_field('style_photo');
						$dress_size= get_field('dress_size');
						$shoe_size= get_field('shoe_size');
						$bra_size= get_field('bra_size');
						$biceps= get_field('biceps');
						$bust= get_field('bust');
						$waist= get_field('waist');
						$butt= get_field('butt');
						$beach= get_field('beach');
						$house= get_field('house');
			   $partners= get_field('partners');
						$smoking_caught= get_field('smoking_caught');
						$no_makeup= get_field('no_makeup');
						$since= get_field('since');
						
						//
						// get zodiac sign from birth date
						//	
						function zodiac($birthdate) {
						
							$zodiac = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiac = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiac = "Taurus"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiac = "Gemini"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiac = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiac = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiac = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiac = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiac = "Scorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiac = "Sagittarius"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiac = "Capricorn"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiac = "Aquarius"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiac = "Pisces"; } 
						     
							return $zodiac; 

						}
						
						$starsign_label = zodiac($birth_date, $sex);

// get zodiac sign from birth date
						//	
						function zodiacde($birthdate) {
						
							$zodiacde = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacde = "Widder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacde = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacde = "Zwillinge"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacde = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacde = "Löwe"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacde = "Jungfrau"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacde = "Waage"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacde = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacde = "Schütze"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacde = "Steinbock"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacde = "Wassermann"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacde = "Fische"; } 
						     
							return $zodiacde;

						}
						
						$starsignde_label = zodiacde($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacfr($birthdate) {
						
							$zodiacfr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacfr = "Bélier"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacfr = "Taureau"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacfr = "Gémaux"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacfr = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacfr = "Lion"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacfr = "Vierge"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacfr = "Balance"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacfr = "Scorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacfr = "Sagittaire"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacfr = "Capricorne"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacfr = "Verseau"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacfr = "Poisons"; } 
						     
							return $zodiacfr;

						}

						
						$starsignfr_label = zodiacfr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacbr($birthdate) {
						
							$zodiacbr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacbr = "Carneiro"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacbr = "Touro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacbr = "Gémeos"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacbr = "Caranguejo"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacbr = "Leão"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacbr = "Virgem"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacbr = "Balança"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacbr = "Escorpião"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacbr = "Sagitário"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacbr = "Capricórnio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacbr = "Aquário"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacbr = "Peixes"; } 
						     
							return $zodiacbr;

						}

						$starsignbr_label = zodiacbr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiaces($birthdate) {
						
							$zodiaces = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiaces = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiaces = "Tauro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiaces = "Geminis"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiaces = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiaces = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiaces = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiaces = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiaces = "Escorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiaces = "Sagitario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiaces = "Capricornio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiaces = "Acuario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiaces = "Piscis"; } 
						     
							return $zodiaces;

}
						$starsignes_label = zodiaces($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacpl($birthdate, $sex) {
						
							$zodiacpl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacpl = "Baran"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacpl = "Byk"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacpl = "Bliźnięta"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacpl = "Rak"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacpl = "Lew"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacpl = "Panna"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacpl = "Waga"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacpl = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacpl = "Strzelec"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacpl = "Koziorożec"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacpl = "Wodnik"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacpl = "Ryby"; } 
						     

						     
							return $zodiacpl;

}
			
						$starsignpl_label = zodiacpl($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacda($birthdate, $sex) {
						
							$zodiacda = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacda = "Vædder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacda = "Tyr"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacda = "Tvilling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacda = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacda = "Løve"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacda = "Jomfru"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacda = "Vægt"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacda = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacda = "Skytte"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacda = "Stenbuk"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacda = "Vandmand"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacda = "Fisk"; } 

						     

						     
							return $zodiacda;

}
			
						$starsignda_label = zodiacda($born_on, $sex);


// get zodiac sign from birth date
						//	
						function zodiacsv($birthdate, $sex) {
						
							$zodiacsv = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacsv = " Väduren "; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacsv = "Oxen"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacsv = " Tvillingarna "; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacsv = "Kräftan"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacsv = "Lejonet"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacsv = " Jungfrun "; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacsv = " Vågen "; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacsv = " Skorpionen "; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacsv = " Skytten "; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacsv = " Stenbocken "; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacsv = " Vattumannen "; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacsv = " Fiskarna "; } 
 

						     

						     
							return $zodiacsv;

}
			
						$starsignsv_label = zodiacsv($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacnb($birthdate, $sex) {
						
							$zodiacnb = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnb = "Væren"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnb = "Tyren"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnb = "Tvillingene"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnb = "Krepsen"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnb = "Løven"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnb = "Jomfruen"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnb = "Vekten"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnb = "Skorpionen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnb = "Skytten"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnb = "Stenbukken"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnb = "Vannmannen"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnb = "Fiskene"; } 

						     

						     
							return $zodiacnb;

}
			
						$starsignnb_label = zodiacnb($born_on, $sex);



// get zodiac sign from birth date
						//	
						function zodiacin($birthdate, $sex) {
						
							$zodiacin = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacin = "मेष"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacin = "वृषभ"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacin = "मिथुन"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacin = "कर्क"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacin = "सिंह"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacin = "कन्या"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacin = "तुला"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacin = "वृश्चिक"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacin = "धनु"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacin = "मकर"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacin = "कुंभ"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacin = "मीन"; } 
						     
							return $zodiacin;

}
			
						$starsignin_label = zodiacin($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiactr($birthdate, $sex) {
						
							$zodiactr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiactr = "Koç"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiactr = "Boğa"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiactr = "İkizler"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiactr = "Yengeç"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiactr = "Aslan"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiactr = "Başak"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiactr = "Terazi"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiactr = "Akrep"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiactr = "Yay"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiactr = "Oğlak"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiactr = "Kova"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiactr = "Balık"; } 
						     
							return $zodiactr;

}

						$starsigntr_label = zodiactr($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacnl($birthdate, $sex) {
						
							$zodiacnl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnl = "Ram"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnl = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnl = "Tweeling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnl = "Kreeft"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnl = "Leeuw"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnl = "Maagd"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnl = "Weegschaal"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnl = "Schorpioen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnl = "Boogschutter"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnl = "Steenbok"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnl = "Waterman"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnl = "Vissen"; } 
						     
							return $zodiacnl;

}

						$starsignnl_label = zodiacnl($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacit($birthdate, $sex) {
						
							$zodiacit = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacit = "Ariete"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacit = "Toro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacit = "Gemelli"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacit = "Cancro"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacit = "Leone"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacit = "Vergine"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacit = "Bilancia"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacit = "Scorpione"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacit = "Sagittario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacit = "Capricorno"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacit = "Aquario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacit = "Pesci"; } 
						     
							return $zodiacit;

}

						$starsignit_label = zodiacit($born_on, $sex);
						

						//$starsign_field = get_field_object('starsign');
						//$starsign_value = get_field('starsign');
						//$starsign_label = $starsign_field['choices'][ $starsign_value[0] ];	


						?>


<div id="main-content-contain" class="left relative infinite-content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div id="content-wrap" class="left relative"  itemscope itemtype="http://schema.org/Article">
<div class="sec-marg-out4 relative">
	<div class="sec-marg-in4">
					<article <?php post_class(); ?>>
						<div id="post-header" class="left relative">
							<div class="sec-marg-out relative">
								<div class="sec-marg-in">
									<div class="post-info-top left relative">
									
									</div><!--post-info-top-->
									<h1 class="post-title left" itemprop="name headline"><?php the_title(); ?></h1>
									
<span class="post-excerpt"><?php the_excerpt(); ?>
<span style='color: #777;
	float: left;
	font-size: 1.4rem;
	font-style: italic;
	font-weight: 400;
	line-height: 140%;
	width: 100%;
	'>
<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Get <?php echo $celebname_label; ?>’s clothing style - from her tattoos favorite jackets, shirts & jeans to shoes, jewelry & underwear. Learn from her height, weight & waist, butt & bra measurements & tips.
<?php else: ?> 
Get <?php echo $celebname_label; ?>’s clothing style - from his tattoos, favorite jackets, dresses, skirts & jeans to shoes, jewelry & underwear. Learn from his height, weight, body measurements & tips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Get <?php echo $celebname_label; ?>'s look - from mascara, eye liner & hair products to nail polish & fragrances. Check out her real hair and eye color & learn about her health (smoker?) & her beauty types & tips. 
<?php else: ?>
Get <?php echo $celebname_label; ?>'s look - from his wax, moisturizer & hair products to fragrances. Check out his real hair and eye color & learn about his health (smoker?) & his beauty types & tips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Celebrity homes - from their mother, father, kids & friends to their houses & cars, net worth & salary.
<?php endif; ?> 
<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Kleed je net zoals je favoriete beroemdheid – van jassen, jurken, rokjes & spijkerbroeken tot aan schoenen, sieraden & ondergoed. Kom meer te weten over kledingmaten & leer van fashion tips van beroemdheden.
<?php else: ?> 
Kleed je net zoals je favoriete beroemdheid – van jassen, t-shirts & spijkerbroeken tot aan schoenen, sieraden & ondergoed. Kom meer te weten over kledingmaten & leer van fashion tips van beroemdheden.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Neem de look van <?php echo $celebname_label; ?>! – van mascara, eye liner & haarproducten tot aan nagellak & geurtjes. Check haar echte haarkleur en oogkleur en lees over haar gezondheid (rookt ze?) & haar schoonheidstips.
<?php else: ?>
Neem de look van <?php echo $celebname_label; ?>! – van wax, gel, moisturizer & haarproducten tot aan geurtjes. Check zijn echte haarkleur en oogkleur en lees over zijn gezondheid (rookt hij?) & schoonheidstips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Huizen van sterren – van de moeder, de vader, de kinderen & de vrienden tot aan de huizen & auto’s, het vermogen & salaris van je favoriete beroemdheid.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Holen Sie sich einen Promi-Stil - von ihren Lieblings-Jacken, Kleidern, Röcken und Jeans zu Schuhen, Schmuck und Unterwäsche. Lernen Sie von ihren Maßen & Tipps.
<?php else: ?> 
Holen Sie sich den Kleidungsstil eines Promis - von ihren Lieblings-Jacken, Hemden und Jeans zu Schuhen, Schmuck und Unterwäsche. Lernen Sie von ihren Maßen & Tipps.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Holen Sie sich einen Promi-Look - von Wimperntusche, Eyeliner & Haarkosmetik zu Nagellack & Düften. Lernen Sie von ihren Schönheitstypen & Tipps.
<?php else: ?>
Holen Sie sich den Promi-Look - von Haarwachs, Feuchtigkeitscreme & Haarkosmetik zu Düften. Lernen Sie von ihren Schönheitstypen & Tipps.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Promi-Häuser - von ihren Müttern, Vätern, Kindern und Freunden zu ihren Häusern und Autos, Nettowert & Gehalt.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Obtenez un style de célébrité - depuis leur favoris vestes, 
 robes, jupes & jeans jusqu’aux chaussures, bijoux et sous-vêtements. S'inspirer de leurs mesures et conseils.
<?php else: ?> 
Obtenez un style de vêtements de célébrité - leurs vestes, chemises, jeans et chaussures préférés, leurs bijoux et sous-vêtements. S'inspirer de leurs mesures et conseils.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Obtenez un look de célébrité – mascara, eye-liner et produits capillaires  ainsi que vernis à ongles et parfums. S'inspirer de leurs types et conseils de beauté. 
<?php else: ?>
Obtenez le look de célébrité – depuis la crème pour cheveux, en passant par les crèmes hydratantes pour cheveux jusqu’aux parfums. S'inspirer de leurs types et conseils de beauté.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Maisons de célébrités - de leur mère, des père, des enfants et des amis à leurs maisons, valeur nette & voitures, salaire.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Consigue el estilo de una celebridad - de sus chaquetas, vestidos, faldas y jeans a joyerías y ropa interior favoritos. Aprende de sus tamaños y consejos.
<?php else: ?> 
Consigue ele estilo de ropa de una celebridad - de sus chaquetas, camisas y jeans a zapatos, joyerías y ropa interior favoritos. Aprende de sus tamaños y consejos.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Consigue la apariencia de una celebridad - de rimel, delineador y productos de cabello a pinturas de uña y fragancias. Aprende de sus tipos y consejos de belleza. 
<?php else: ?>
Consigue la apariencia de una celebridad - de cera de cabello, hidratantes y productos de cabello a fragancias. Aprende de sus tipos y consejos de belleza.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Casas de celebridades - de su madre, padre, hijos y amigos a sus casas y carros, coste total y salario. 
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Tenha o estilo de roupas de <?php echo $celebname_label; ?> - das suas tatuagens, jaquetas, vestidos e saias favoritos, a seus sapatos, jóias e roupa íntima. Aprenda de suas medidas de altura, peso e cintura, bunda e sutiã e dicas.
<?php else: ?> 
Tenha o estilo de roupas de <?php echo $celebname_label; ?> - das suas tatuagens, jaquetas, camisetas e jeans favoritos a seus sapatos, jóias e roupa íntima. Aprenda de suas medidas de altura, peso e corpo e dicas.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Tenha a aparência de <?php echo $celebname_label; ?> - de máscara, delineador de olhos e produtos de cabelo a polidores de unha e fragrâncias. Veja suas cores de cabelo e olhos reais e aprenda de sua saúde (fumante?) e seu tipo e dicas de beleza. 
<?php else: ?>
Tenha a aparência de <?php echo $celebname_label; ?> - de sua cera, hidratante e produtos de cabelo a fragrâncias. Veja suas cores de cabelo e olhos reais e aprenda de sua saúde (fumante?) e seu tipo e dicas de beleza.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Casas de celebridades – da sua mãe, pai, filhos e amigos a suas casas e carros, patrimônio líquido e salário. 
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Impara lo stile di abbigliamento di  <?php echo $celebname_label; ?> - come i suoi tatuaggi, le giacche preferite, abiti, gonne e jeans, per passare alle scarpe, gioielli e biancheria intima. . Impara dai consigli e dalla sua altezza, peso e girovita, misure della testa e di reggiseno.<?php else: ?>
Impara lo stile di abbigliamento di  <?php echo $celebname_label; ?> - come i suoi tatuaggi, le giacche preferite, abiti, pantaloni e jeans, per passare alle scarpe, gioielli e biancheria intima. Impara dai suoi consigli e dalla sua altezza, peso e misure del corpo.
 <?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Impara lo stile di abbigliamento di  <?php echo $celebname_label; ?> - come i suoi tatuaggi, le giacche preferite, abiti, gonne e jeans, per passare alle scarpe, gioielli e biancheria intima. . Impara dai consigli e dalla sua altezza, peso e girovita, misure della testa e di reggiseno.<?php else: ?>
Impara lo stile di abbigliamento di  <?php echo $celebname_label; ?> - come i suoi tatuaggi, le giacche preferite, abiti, pantaloni e jeans, per passare alle scarpe, gioielli e biancheria intima. Impara dai suoi consigli e dalla sua altezza, peso e misure del corpo.<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
La vita delle celebrità - dalle madri, ai padri, figli e amici, alle loro case, automobili, patrimonio netto e salario.
<?php endif; ?>
<?php endif; ?>
</span>
</span>
								</div><!--sec-marg-in-->
							</div><!--sec-marg-out-->
						</div><!--post-header-->
						<div id="post-area" class="left relative">
							<div class="post-body-out">
								<?php $mvp_author = get_option('mvp_author_box'); $mvp_social_box = get_option('mvp_social_box'); if ( $mvp_social_box == "false" && $mvp_author == "false" ) { } else { ?>
								<div class="post-info-left left relative">
									<div class="sec-marg-out relative">
										<div class="sec-marg-in">
									<div class="post-info-left-top left relative">
										<?php $mvp_author = get_option('mvp_author_box'); if ($mvp_author == "true") { ?>
											<div class="author-info left relative">
												<div class="author-img left relative">
													<?php echo get_avatar( get_the_author_meta('email'), '120' ); ?>
												</div><!--author-img-->
												<span class="author-name vcard fn" itemprop="author"><?php the_author_posts_link(); ?></span>
											</div><!--author-info-->
										<?php } ?>
										<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
											<?php mvp_share_count(); ?>
										<?php } ?>
									</div><!--post-info-left-top-->
									<div class="post-info-left-bot left relative">
									<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
								<div class="post-soc-out">
									<div class="post-soc-in">
										<div class="post-soc-share left relative">
											<div class="soc-count-mobi">
												<?php mvp_share_count(); ?>
											</div><!--soc-count-mobi-->
											<a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><div class="post-soc-fb left relative"><i class="fa fa-facebook fa-2"></i><span class="post-share-text"><?php _e( 'Share', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $fb_share = get_fb( $post_id ); if($fb_share==0) { } else { echo esc_html( $fb_share ); } ?></span></div><!--post-soc-fb--></a>
											<a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?> -&url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><div class="post-soc-twit left relative"><i class="fa fa-twitter fa-2"></i><span class="post-share-text"><?php _e( 'Tweet', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $twit_share = get_tweets( $post_id ); if($twit_share==0) { } else { echo esc_html( $twit_share ); } ?></span></div><!--post-soc-twit--></a>
											<a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><div class="post-soc-pin left relative"><i class="fa fa-pinterest fa-2"></i><span class="post-share-text"><?php _e( 'Pin', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $pin_share = get_pinterest( $post_id ); if($pin_share==0) { } else { echo esc_html( $pin_share ); } ?></span></div><!--post-soc-pin--></a>
										</div><!--post-soc-share-->
									</div><!--post-soc-in-->
								</div><!--post-soc-out-->
									<?php } ?>
									<?php $prev_next = get_option('mvp_prev_next'); if ($prev_next == "true") { ?>
										<div class="prev-next-posts left relative">
											<?php mvp_prev_next_links(); ?>
										</div><!--prev-next-posts-->
									<?php } ?>
											</div><!--post-info-left-bot-->
										</div><!--sec-marg-in-->
									</div><!--sec-marg-out-->
								</div><!--post-info-left-->
								<?php } ?>
								<div class="post-body-in">
									<div id="content-area" class="left relative" itemprop="articleBody">
										<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "wide" || $mvp_post_temp == "wide-full" ) { } else { ?>
										<?php $mvp_featured_img = get_option('mvp_featured_img'); if ($mvp_featured_img == "true") { ?>
						<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
							<div id="video-embed" class="left relative sec-feat">
								<?php echo get_post_meta($post->ID, "mvp_video_embed", true); ?>
							</div><!--video-embed-->
						<?php else: ?>
							<?php $mvp_show_hide = get_post_meta($post->ID, "mvp_featured_image", true); if ($mvp_show_hide == "hide") { ?>
							<?php } else { ?>
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div id="featured-image" class="left relative sec-feat">
										<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb', array( 'class' => 'unlazy' ) ); ?>
										<img itemprop="image" src="<?php echo $thumb['0']; ?>" />
										<?php if(get_post_meta($post->ID, "mvp_photo_credit", true)): ?>
											<div id="featured-caption">
												<?php echo get_post_meta($post->ID, "mvp_photo_credit", true); ?>
											</div><!--featured-caption-->
										<?php endif; ?>
									</div><!--featured-image-->
								<?php } ?>
							<?php } ?>
						<?php endif; ?>
										<?php } ?>
										<?php } ?>
									<div class="content-area-cont left relative">
										<div class="sec-marg-out relative">
											<div class="sec-marg-in">
										<div class="content-area-out">
											<div class="content-area-in">
												<div class="content-main left relative">
													<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "wide" || $mvp_post_temp == "wide-full" ) { } else { ?>
										<?php $mvp_featured_img = get_option('mvp_featured_img'); if ($mvp_featured_img == "true") { ?>
						<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
							<div id="video-embed" class="left relative prim-feat">
								<?php echo get_post_meta($post->ID, "mvp_video_embed", true); ?>
							</div><!--video-embed-->
						<?php else: ?>
							<?php $mvp_show_hide = get_post_meta($post->ID, "mvp_featured_image", true); if ($mvp_show_hide == "hide") { ?>
							<?php } else { ?>
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div id="featured-image" class="left relative prim-feat">
										<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb', array( 'class' => 'unlazy' ) ); ?>
										<img itemprop="image" src="<?php echo $thumb['0']; ?>" />
										<?php if(get_post_meta($post->ID, "mvp_photo_credit", true)): ?>
											<div id="featured-caption">
												<?php echo get_post_meta($post->ID, "mvp_photo_credit", true); ?>
											</div><!--featured-caption-->
										<?php endif; ?>
									</div><!--featured-image-->
								<?php } ?>
							<?php } ?>
						<?php endif; ?>
										<?php } ?>
										<?php } ?>

<center>                        		
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> earned a <?php echo $salary ?> million dollar salary, leaving the net worth at <?php echo $net_worth ?> million in <?php echo date("Y"); ?>'s ' "/></a>
<b><?php echo $celebname_label; ?> made <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php echo $net_worth ?> million dollar fortune with <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, and by endorsing brands like <?php echo $endorsements; ?><?php endif; ?>. But how does this <?php echo $descript_label; ?> <?php echo $profession_label; ?> from <?php echo $place_of_birth ?> spend <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> money? And how rich is <?php if(in_array('vrouw', get_field('love') )): ?>the husband of <?php echo $current_partner_text ?><?php elseif( (in_array('man', get_field('love') ) )): ?>the wife of <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde', get_field('love') ) )): ?>the fiancé of <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>the fiancée of <?php echo $current_partner_text ?><?php elseif( (in_array('vriend', get_field('love') ) )): ?>the girlfriend of <?php echo $current_partner_text ?><?php elseif( (in_array('vriendin', get_field('love') ) )): ?>the boyfriend of <?php echo $current_partner_text ?><?php else: ?>this single<?php endif; ?> in terms of friends & family?</b> 
</br><br>

<h2>Quick personal facts about <?php echo $celebname_label; ?> </br></h2>
<br>
<b>Origin:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Birth date:</b> <?php echo $birth_date ?></br>
<br>
<b>Estimated net worth:</b> <?php echo $net_worth ?> million</br>
<br>
<?php if( $salary ): ?><b>Salary:</b> <?php echo $salary ?> million<?php else: ?><b>Salary:</b> N/A</br>
<br><?php endif; ?>
<?php if(in_array('voetballer', get_field('profession') )): ?><b>Must-read:</b></br>- <a rel='nofollow' target="_blank" href="http://taddlr.com/top-15-highest-paid-soccer-players-2014/"><u>New: Top 15 highest paid footballers of 2015!</u></a></br><br><?php endif; ?>
<?php if(in_array('actrice', get_field('profession') )): ?>
<b>Must-read:</b></br>- <a href="http://muzul.com/the-10-richest-celebrities-in-hollywood-net-worth-salary/"><u>The 10 richest celebrities in Hollywood – Net worth & Salary</u></a></br><br><?php endif; ?>
<?php if(in_array('acteur', get_field('profession') )): ?>
<b>Must-read:</b></br>- <a href="http://muzul.com/the-10-richest-celebrities-in-hollywood-net-worth-salary/"><u>The 10 richest celebrities in Hollywood – Net worth & Salary</u></a></br><br><?php endif; ?>
<?php if( $nickname ): ?><b>Nickname:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Love life:</b> <?php echo $love_label; ?> - find out if he is on Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - find out if she is on Tinder?<?php else: ?><b><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Love history:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/celebrity/<?php echo $post->post_name;?>'>Full overview of <?php echo $celebname_label; ?>'s love life</a></br>
<br>
<b>Famous colleagues</b>: <?php echo $colleagues ?></br>
<br>
<b>Management:</b> <?php if( $management ): ?><?php echo $management ?><?php else: ?>N/A<?php endif; ?></br>
<br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Houses & Property</h2></div>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<br>
<?php if( have_rows('houses') ): ?>
Where does <?php echo $celebname_label; ?> live?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div>
<?php if(in_array('male', get_field('sex') )): ?>His <?php else: ?>Her<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: <?php echo $price; ?> million dollar • Includes: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no houses listed.
<?php endif; ?>
		</br>
- <a href='http://muzul.com/16-most-insanely-beautiful-and-expensive-celebrity-homes/'>
16 most insanely beautiful and expensive celebrity homes</a></br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Cars & Boats</h2></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>'s cars, boats and other vehicles. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> manufactured by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Currently no cars listed.</br><br>
<?php endif; ?>
- <a href='http://muzul.com/the-coolest-and-most-expensive-celebrity-cars/'>The 16 most expensive cars owned by celebrities</a></br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Family</h2></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Family photo of the <?php echo $profession_label; ?><?php if(in_array('vrouw', get_field('love') )): ?>, married to <?php echo $current_partner_text ?>,<?php elseif( (in_array('man', get_field('love') ) )): ?>, married to <?php echo $current_partner_text ?>, <?php elseif( (in_array('verloofde', get_field('love') ) )): ?>, engaged to <?php echo $current_partner_text ?>,<?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>, engaged to <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriend', get_field('love') ) )): ?>, dating <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriendin', get_field('love') ) )): ?>, dating <?php echo $current_partner_text ?>,<?php endif; ?> famous for <?php echo $claim_to_fame ?>. ' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php if(in_array('male', get_field('sex') )): ?>He <?php else: ?>She<?php endif; ?> might have befriended <?php echo $colleagues ?><?php if( $partners ): ?>, and dated <?php echo $partners ?><?php endif; ?>, but what about family?</br><br>
<b>Names of father, mother, kids, brothers & sisters. </b></br>

						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relation: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Friends</h2></div>
<br>
<b>Who does this <?php echo $starsign_label; ?>, age <?php echo $age ?> & born in <?php echo $birth_date ?> , hang out with?</b></br>
<?php if( have_rows('friends') ): ?>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his<?php else: ?>her<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
 <em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: 'inline-block' '>Know each other from: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

</br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Pets</h2></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?>'s pets and their names</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
 <em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no pets listed.
<?php endif; ?>

		</br>

- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Look & Beauty tips</a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Clothing style & tips</a></b></br>
Change salary or net worth? Add a house or a car? Tell us below.


<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> verdiende een salaris van <?php echo $salary ?> miljoen dollar, waardoor het vermogen <?php echo $net_worth ?> miljoen bedraagt in het jaar <?php echo date("Y"); ?>' "/></a>

<b><?php echo $celebname_label; ?> verdiende <?php if(in_array('male', get_field('sex') )): ?>zijn vermogen van<?php else: ?>haar vermogen van<?php endif; ?> <?php echo $net_worth ?> miljoen dollar<?php if( $endorsements ): ?>, naast met reclames voor merken zoals <?php echo $endorsements; ?>,<?php endif; ?> met <?php echo $claim_to_fame ?>. Maar waar geeft deze <?php echo $descript_label; ?> <?php echo $profession_label; ?> uit <?php echo $place_of_birth ?><?php if(in_array('male', get_field('sex') )): ?> zijn<?php else: ?> haar<?php endif; ?> geld aan uit? En hoe rijk is <?php if(in_array('vrouw', get_field('love') )): ?>de man van <?php echo $current_partner_text ?><?php elseif( (in_array('man', get_field('love') ) )): ?>de vrouw van <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde', get_field('love') ) )): ?>de verloofde van <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>de verloofde van <?php echo $current_partner_text ?><?php elseif( (in_array('vriend', get_field('love') ) )): ?>de vriendin van <?php echo $current_partner_text ?><?php elseif( (in_array('vriendin', get_field('love') ) )): ?>de vriend van <?php echo $current_partner_text ?><?php else: ?>deze vrijgezel<?php endif; ?> in vrienden & familie?</b>  
</br><br>
<h2>Persoonlijke feiten over <?php echo $celebname_label; ?> </br></h2>
<br>
<b>Geboorteplaats:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Geboortedatum:</b> <?php echo $birth_date ?></br>
<br>
<b>Geschatte vermogen:</b> <?php echo $net_worth ?> miljoen</br>
<br>
<?php if( $salary ): ?><b>Salaris:</b> <?php echo $salary ?> miljoen</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Bijnaam:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Liefdesleven:</b> <?php echo $love_label; ?> - ontdek of hij op Tinder zit?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - ontdek of zij op Tinder zit?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Ex-partners:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/nl/celebrity/<?php echo $post->post_name;?>'>Klik hier voor een volledig overzicht van <?php echo $celebname_label; ?>'s liefdesleven </a></br>
<br>
<b>Bekende collega’s</b>: <?php echo $colleagues ?></br>
<br>
<b>Management:</b> <?php echo $management ?></br>
<br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Huizen & Bezit</h2></div>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<br>
<?php if( have_rows('houses') ): ?>
Waar woont <?php echo $celebname_label; ?>?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Zijn <?php else: ?>Haar<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: <?php echo $price; ?> miljoen dollar • Inclusief: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Momenteel geen huizen bekend.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Auto's & Boten</h2></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>’s auto’s, boten en overige voertuigen. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> vervaardigd door <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Momenteel geen auto’s, boten of overige voertuigen bekend.
<?php endif; ?>
</br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Familie</h2></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Familiefoto van de <?php echo $profession_label; ?><?php if(in_array('vrouw', get_field('love') )): ?>, getrouwd met <?php echo $current_partner_text ?>,<?php elseif( (in_array('man', get_field('love') ) )): ?>, getrouwd met <?php echo $current_partner_text ?>, <?php elseif( (in_array('verloofde', get_field('love') ) )): ?>, verloofd met <?php echo $current_partner_text ?>,<?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>, verloofd met <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriend', get_field('love') ) )): ?>, een relatie met <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriendin', get_field('love') ) )): ?>, een relatie met <?php echo $current_partner_text ?>,<?php endif; ?> die beroemd is vanwege <?php echo $claim_to_fame ?>. ' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php if(in_array('male', get_field('sex') )): ?>Hij <?php else: ?>Ze<?php endif; ?> heeft misschien <?php echo $colleagues ?> bevriend<?php if( $partners ): ?>, en met <?php echo $partners ?> gedate<?php endif; ?>, maar hoe zit het met familie?</br><br>
<b>Namen van ouders, kinderen, broers & zussen.</b> </br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt=Foto van <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>zijn <?php else: ?>haar<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relatie: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Vrienden</h2></div>
<br>
<b>Met wie brengt deze <?php echo $starsign_label; ?>, <?php echo $age ?> jaar oud & geboren op <?php echo $birth_date ?>, tijd door?</b></br>
<?php if( have_rows('friends') ): ?>

						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt=Foto van <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>zijn<?php else: ?>haar<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Kennen elkaar van: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Huisdieren</h2></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?>’s huisdieren en bijbehorende namen:</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt=Foto van <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>zijn <?php else: ?>haar<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Momenteel geen huisdieren bekend.
<?php endif; ?>

		</br>

- <b><a href='http://muzul.com/nl/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Look & Schoonheidstips </a></br>
- <a href='http://muzul.com/nl/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Kledingstijl & tips</a></b></br>
</br><br>
Klopt er iets niet? Zoals het salaris of het vermogen? Of wil je graag iets toevoegen aan het profiel? Deel je suggesties door middel van een comment onderaan de pagina.</br><br>

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> verdiente <?php echo $salary ?> Millionen Dollar Gehalt und so entsprach der Nettowert <?php echo $net_worth ?> Millionen in <?php echo date("Y"); ?>'s ' "/></a>

<b><?php echo $celebname_label; ?> machte <?php if(in_array('male', get_field('sex') )): ?>sein <?php else: ?>ihr<?php endif; ?> <?php echo $net_worth ?> Millionen-Dollar-Vermögen mit <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, und  durch Marken wie <?php echo $endorsements; ?><?php endif; ?>. Aber wie gibt <?php if(in_array('male', get_field('sex') )): ?>dieser <?php else: ?>diese<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?> aus <?php echo $place_of_birth ?> <?php if(in_array('male', get_field('sex') )): ?>sein <?php else: ?>ihr <?php endif; ?> Geld aus, und wer sind Freunde und Familie für die <?php if(in_array('male', get_field('sex') )): ?>er<?php else: ?>sie<?php endif; ?> es ausgibt?</b> 
</br><br>
<h2>Schnelle persönliche Fakten über <?php echo $celebname_label; ?> </br></h2>
<br>
<b>Herkunft:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Geburtsdatum:</b> <?php echo $birth_date ?></br>
<br>
<b>Geschätzter Nettowert:</b> <?php echo $net_worth ?> Millionen</br>
<br>
<?php if( $salary ): ?><b>Gehalt:</b> <?php echo $salary ?> Millionen</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Spitzname:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Liebesleben:</b> <?php echo $love_label; ?> - finde heraus, ob er auf Tinder ist?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - finde heraus, ob sie auf Tinder ist?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Liebesgeschichte:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/de/celebrity/<?php echo $post->post_name;?>'>Vollständiger Überblick über <?php echo $celebname_label; ?>'s Liebesleben</a></br>
<br>
<b>berühmte Kollegen</b>: <?php echo $colleagues ?></br>
<br>
<b>Management:</b> <?php echo $management ?></br>
<br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Häuser & Eigentum</h2></div>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<br>
<?php if( have_rows('houses') ): ?>
Wo lebt <?php echo $celebname_label; ?> derzeit?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h2><?php echo $place; ?></h2></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Sein <?php else: ?>Ihr <?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: <?php echo $price; ?> Millionen Dollar • Beinhaltet: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Zur Zeit sind keine Häuser aufgeführt.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Autos & Boote</h2></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>'s Autos, Boote und andere Fahrzeuge. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> hergestellt von <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Zur Zeit sind keine Autos aufgeführt.
<?php endif; ?>
</br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Familie</h2></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Familienfoto' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php echo $celebname_label; ?>'s Familie: Namen von Vater, Mutter, Kinder, Brüder und Schwestern. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto von <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>seinen <?php else: ?>ihren<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Beziehung: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Freunden</h2></div>
<br>
<b>Mit wem hängt dieser <?php echo $age ?> -jährige und am <?php echo $birth_date ?> geboren wurde <?php echo $starsignde_label; ?> ab?</b></br>
<?php if( have_rows('friends') ): ?>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto von <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>seinen<?php else: ?>ihren<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Kennen sich aus: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Haustieren</h2></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?>'s Haustiere und ihre Namen</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Foto von <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>seinen <?php else: ?>ihren<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Zur Zeit sind keine Haustiere aufgeführt.
<?php endif; ?>

		</br>
- <b><a href='http://muzul.com/de/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Tipps für Aussehen & Schönheit</a></br>
- <a href='http://muzul.com/de/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Kleidungsstil & Tipps</a></b></br>
</br><br>
Etwas nicht in Ordnung? Wie z.B. das Gehalt oder der Nettowert? Oder wollen Sie einen Freund, ein Haus oder ein Auto hinzufügen? Sie können dies selbst bearbeiten oder Ihre Vorschläge im Kommentar-Formular unten teilen.</br><br>



<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> earned a <?php echo $salary ?> million dollar salary, leaving the net worth at <?php echo $net_worth ?> million in <?php echo date("Y"); ?>'s ' "/></a>

<b><?php echo $celebname_label; ?> boucle <?php if(in_array('male', get_field('sex') )): ?>sa fortune de <?php else: ?>sa<?php endif; ?> <?php echo $net_worth ?> million de dollar avec <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, et en soutenant des marques comme <?php echo $endorsements; ?><?php endif; ?>. Mais comment ce personnage <?php echo $descript_label; ?> <?php echo $profession_label; ?> <?php echo $place_of_birth ?> dépense <?php if(in_array('male', get_field('sex') )): ?>son <?php else: ?>son<?php endif; ?> argent, et qui sont ses amis & sa famille <?php if(in_array('male', get_field('sex') )): ?> <?php else: ?> <?php endif; ?>  ?</b> 
</br><br>
<h2> Faits personnels en bref sur <?php echo $celebname_label; ?> </br></h2>
<br>
<b> Origine:</b> <?php echo $place_of_birth ?></br>
<br>
<b> Date de naissance:</b> <?php echo $birth_date ?></br>
<br>
<b> Valeur nette estimée:</b> <?php echo $net_worth ?> million</br>
<br>
<?php if( $salary ): ?><b> Salaire:</b> <?php echo $salary ?> million</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b> Pseudo:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b> Vie amoureuse:</b> <?php echo $love_label; ?> - savoir s’il est sur Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - savoir si elle est sur Tinder?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b> Histoire d'amour:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/fr/celebrity/<?php echo $post->post_name;?>'> Vue d'ensemble de la vie amoureuse de <?php echo $celebname_label; ?> </a></br>
<br>
<b> Célèbres confrères </b>: <?php echo $colleagues ?></br>
<br>
<b> Gestion:</b> <?php echo $management ?></br>
<br>
<br>
<h2> Qui fait cela <?php echo $starsignfr_label; ?>, âge <?php echo $age ?>, passer du temps avec?</h2></br>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Famille</h2></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Family Photo' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
Famille de <?php echo $celebname_label; ?> : les noms du père, mère, enfants, frères et soeurs. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relation: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Amis</h2></div>
<br>
<?php if( have_rows('friends') ): ?>
Les amis de <?php echo $celebname_label; ?> </br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his<?php else: ?>her<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'> Se connaîssent mutuellement à travers: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Animaux de compagnie </h2></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?> Animaux de compagnie et leurs noms </br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actuellement aucun animal listé.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Maisons & Propriété</h2></div>
<br>
<?php if( have_rows('houses') ): ?>
Où est ce que <?php echo $celebname_label; ?> vit?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h2><?php echo $place; ?></h2></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Son<?php else: ?>Son<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: <?php echo $price; ?> million dollar • Comprend: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actuellement pas de maison listée.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Cars & Boats</h2></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>, ses voitures, bateaux et autres véhicules. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> fabriqué par <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Actuellement pas de voiture listée.
<?php endif; ?>
</br>
<br>
- <b><a href='http://muzul.com/fr/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Conseils de Look & de Beauté </a></br>
- <a href='http://muzul.com/fr/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Conseils & style vestimentaire </a></b></br>
</br><br>
Quelque chose pas correcte ? Comme le salaire ou la valeur nette ? Ou vous souhaitez ajouter un ami, une maison ou une voiture ? Vous pouvez modifier vous-même ou partager vos suggestions dans le formulaire de commentaire ci-dessous.</br><br>

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> recibió un salario de <?php echo $salary ?> millones de dólares, dejando un coste total de <?php echo $net_worth ?> millones en <?php echo date("Y"); ?> ' "/></a>

<b><?php echo $celebname_label; ?> hizo su fortuna de <?php echo $net_worth ?> millones de dólares con <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, y promocionando marcas como <?php echo $endorsements; ?><?php endif; ?>. Pero ¿como este <?php echo $descript_label; ?> <?php echo $profession_label; ?> de <?php echo $place_of_birth ?> gasta su dinero? ¿Y cuan <?php if(in_array('vrouw', get_field('love') )): ?> rico es el esposo de <?php echo $current_partner_text ?><?php elseif( (in_array('man', get_field('love') ) )): ?> rica es la esposa de <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde', get_field('love') ) )): ?> rico es el novio de <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?> rica es la novia de <?php echo $current_partner_text ?><?php elseif( (in_array('vriend', get_field('love') ) )): ?> rica es la enamorada de <?php echo $current_partner_text ?><?php elseif( (in_array('vriendin', get_field('love') ) )): ?> rico es el enamorado de <?php echo $current_partner_text ?><?php else: ?> <?php if(in_array('male', get_field('sex') )): ?> rico es este soltero <?php else: ?> rica es esta soltera<?php endif; ?> <?php endif; ?> en cuanto a amigos y família?</b> 
</br><br>

<h2>Informaciónes personales rápidas sobre <?php echo $celebname_label; ?> </br></h2>
<br>
<b>Origen:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Fecha de nacimiento:</b> <?php echo $birth_date ?></br>
<br>
<b>Coste total estimado:</b> <?php echo $net_worth ?> millones</br>
<br>
<?php if( $salary ): ?><b>Salario:</b> <?php echo $salary ?> millones</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Apodo:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descubrir si él está en Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descubrir si ella está enTinder?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Historial amoroso:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/celebrity/<?php echo $post->post_name;?>'>Resumen completo de la vida amorosa de <?php echo $celebname_label; ?></a></br>
<br>
<b>Colegas famosos</b>: <?php echo $colleagues ?></br>
<br>
<b>Dirección:</b> <?php echo $management ?></br>
<br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Casas y Propriedades </h2></div>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<?php if( have_rows('houses') ): ?>
Donde vive <?php echo $celebname_label; ?>?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Su <?php else: ?>Su<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> en <?php echo $place; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: <?php echo $price; ?> millones de dólares • Incluye: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actualmente no hay casas listadas.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Carros y Barcos</h2></div>
<br>
<?php if( have_rows('car') ): ?>
Carros, barcos y otros veículos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> fabricado por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Actualmente no hay carros listados.
<?php endif; ?>
</br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Família</h2></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Foto de la família del(de la) <?php echo $profession_label; ?><?php if(in_array('vrouw', get_field('love') )): ?>, casada con <?php echo $current_partner_text ?>,<?php elseif( (in_array('man', get_field('love') ) )): ?>, casado con <?php echo $current_partner_text ?>, <?php elseif( (in_array('verloofde', get_field('love') ) )): ?>, novia de <?php echo $current_partner_text ?>,<?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>, novio de <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriend', get_field('love') ) )): ?>, enamorada de <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriendin', get_field('love') ) )): ?>, enamorado de <?php echo $current_partner_text ?>,<?php endif; ?> famoso por <?php echo $claim_to_fame ?>. ' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php if(in_array('male', get_field('sex') )): ?>Él <?php else: ?>Ella<?php endif; ?> puede tener amistad con <?php echo $colleagues ?><?php if( $partners ): ?>, y noviado <?php echo $partners ?><?php endif; ?>, pero ¿y su famíla?</br><br>
<b>Nombres de su padre, madre, hijos, hermanos y hermanas. </b></br>

						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>su <?php else: ?>su<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relación: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Amigos</h2></div>
<br>
<b>¿Con quien esta persona del signo de <?php echo $starsign_label; ?>, <?php echo $age ?> años de edad y nascida en <?php echo $birth_date ?> , pasa su tiempo?</b></br>
<?php if( have_rows('friends') ): ?>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>su<?php else: ?>su<?php endif; ?> amigo, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Se conocen de: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Mascotas</h2></div>
<br>
<?php if( have_rows('pets') ): ?>
Las mascotas de <?php echo $celebname_label; ?> y sus nombres</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Foto de <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>su <?php else: ?>su<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actualmente no hay mascotas listadas.
<?php endif; ?>

		</br>
- <b><a href='http://muzul.com/es/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Consejos de Apariencia y Belleza </a></br>
- <a href='http://muzul.com/es/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Consejos y estilos de ropa </a></b></br>
</br><br>
¿Algo no está bien? ¿Como salario o coste total? ¿O quieres agregar un amigo, casa o carro? Usted mismo(a) puede editar esto o compartir sus sugerencias en el formulario de contacto abajo.</br><br>

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> recebeu um salário de <?php echo $salary ?> milhões de dólares, resultando em um patrimônio líquido de <?php echo $net_worth ?> milhões em <?php echo date("Y"); ?> ' "/></a>

<b><?php echo $celebname_label; ?> fez a sua fortuna de <?php echo $net_worth ?> milhões de dólares com <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, e promocionando marcas como <?php echo $endorsements; ?><?php endif; ?>. Mas como este <?php echo $descript_label; ?> <?php echo $profession_label; ?> de <?php echo $place_of_birth ?> gasta seu dinheiro? E quão <?php if(in_array('vrouw', get_field('love') )): ?> rico é o marido de <?php echo $current_partner_text ?><?php elseif( (in_array('man', get_field('love') ) )): ?> rica é a esposa de <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde', get_field('love') ) )): ?> rico é o noivo de <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?> rica é a noiva de <?php echo $current_partner_text ?><?php elseif( (in_array('vriend', get_field('love') ) )): ?> rica é a namorada de <?php echo $current_partner_text ?><?php elseif( (in_array('vriendin', get_field('love') ) )): ?> rico é o namorado de <?php echo $current_partner_text ?><?php else: ?> <?php if(in_array('male', get_field('sex') )): ?> rico é este solteiro <?php else: ?> rica é esta solteira<?php endif; ?> <?php endif; ?> em termos de amigos e família?</b> 
</br><br>

<h2>Dados pessoais rápidos sobre <?php echo $celebname_label; ?> </br></h2>
<br>
<b>Origem:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Data de nascimiento:</b> <?php echo $birth_date ?></br>
<br>
<b>Patrimônio Líquido estimado:</b> <?php echo $net_worth ?> milhões</br>
<br>
<?php if( $salary ): ?><b>Salário:</b> <?php echo $salary ?> milhões </br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Apelido:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descobrir si ele está no Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descobrir se ela está no Tinder?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Histórico amoroso:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/celebrity/<?php echo $post->post_name;?>'>Resumo completo da vida amorosa de <?php echo $celebname_label; ?></a></br>
<br>
<b>Colegas famosos</b>: <?php echo $colleagues ?></br>
<br>
<b>Direção:</b> <?php echo $management ?></br>
<br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Casas e Propriedades </h2></div>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<?php if( have_rows('houses') ): ?>
Onde vive <?php echo $celebname_label; ?>?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> em <?php echo $place; ?>  ' "/></a></div><?php endif; ?> Seu (sua) <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> em <?php echo $place; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: <?php echo $price; ?> milhões de dólares • Inclui: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Atualmente não há casas listadas.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Carros e Barcos</h2></div>
<br>
<?php if( have_rows('car') ): ?>
Carros, barcos e outros veículos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> fabricado por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Atualmente não há carros listados.
<?php endif; ?>
</br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Família</h2></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Foto de família do(a) <?php echo $profession_label; ?><?php if(in_array('vrouw', get_field('love') )): ?>, casada com <?php echo $current_partner_text ?>,<?php elseif( (in_array('man', get_field('love') ) )): ?>, casado com <?php echo $current_partner_text ?>, <?php elseif( (in_array('verloofde', get_field('love') ) )): ?>, noiva de <?php echo $current_partner_text ?>,<?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>, noivo de <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriend', get_field('love') ) )): ?>, namorada de <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriendin', get_field('love') ) )): ?>, namorado de <?php echo $current_partner_text ?>,<?php endif; ?> famoso por <?php echo $claim_to_fame ?>. ' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php if(in_array('male', get_field('sex') )): ?>Ele <?php else: ?>Ela<?php endif; ?> pode ter amizade com <?php echo $colleagues ?><?php if( $partners ): ?>, e namorado <?php echo $partners ?><?php endif; ?>, mas e sua famíla?</br><br>
<b>Nomes de seu pai, mãe, filhos, irmãos e irmãs. </b></br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> seu(sua) <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Parentesco: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Amigos</h2></div>
<b>Com quem esta pessoa do signo de <?php echo $starsign_label; ?>, <?php echo $age ?> anos de idade e nascida em <?php echo $birth_date ?> , passa seu tempo?</b></br>
<?php if( have_rows('friends') ): ?>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> e seu(sua) amigo(a), <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Se conhecem de: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Animais de estimação</h2></div>
<br>
<?php if( have_rows('pets') ): ?>
Os animais de estimação de <?php echo $celebname_label; ?> e seus nomes</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Foto de <?php echo $celebname_label; ?> e seu(sua) <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Atualmente não há animais de estimação listados.
<?php endif; ?>

		</br>
- <b><a href='http://muzul.com/pt-br/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Dicas de Aparência e Beleza </a></br>
- <a href='http://muzul.com/pt-br/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Dicas e estilos de roupa </a></b></br>
</br><br>
Algo não está certo? Como o salário ou patrimônio líquido? Ou quer adicionar um amigo, casa ou carro? Você mesmo(a) pode editar isto ou compartilhar suas sugestões no formulário de contato abaixo.</br><br>

<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> guadagnato <?php echo $salary ?> milioni di dollari, e come patrimonio netto <?php echo $net_worth ?> milioni in <?php echo date("Y"); ?>'s ' "/></a>

<b><?php echo $celebname_label; ?> ha guadagnato <?php if(in_array('male', get_field('sex') )): ?>i suoi <?php else: ?>i suoi<?php endif; ?> <?php echo $net_worth ?> milioni di dollari con il fortunato <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, e approvando marchi come <?php echo $endorsements; ?><?php endif; ?>. Ma come fa quest<?php if(in_array('male', get_field('sex') )): ?>o<?php else: ?>a<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?> di <?php echo $place_of_birth ?> a spendere <?php if(in_array('male', get_field('sex') )): ?>i suoi <?php else: ?>i suoi<?php endif; ?> soldi? E quanto è benestante <?php if(in_array('vrouw', get_field('love') )): ?>il marito di <?php echo $current_partner_text ?><?php elseif( (in_array('man', get_field('love') ) )): ?>la moglie di <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde', get_field('love') ) )): ?>il fidanzato di <?php echo $current_partner_text ?><?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>la fidanziata di <?php echo $current_partner_text ?><?php elseif( (in_array('vriend', get_field('love') ) )): ?>la ragazza di <?php echo $current_partner_text ?><?php elseif( (in_array('vriendin', get_field('love') ) )): ?>il ragazzo di <?php echo $current_partner_text ?><?php else: ?>questo single <?php endif; ?> nei riguardi di amici & famiglia?</b> 
</br><br>

<div style='float:left; margin-top:10px; padding-bottom: 7px; padding-top: 10px; padding-right: 5px; margin-right:5px;'><?php if (function_exists('adinj_print_ad')){ adinj_print_ad('random'); } ?></div>
<h2>Una veloce carrellata delle pricipali caratteristiche di <?php echo $celebname_label; ?> </br></h2>
<br>
<b>Luogo di origine:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Data di nascita:</b> <?php echo $birth_date ?></br>
<br>
<b>Patrimonio netto stimato:</b> <?php echo $net_worth ?> milioni</br>
<br>
<?php if( $salary ): ?><b>Salario:</b> <?php echo $salary ?> milioni </br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Nickname:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Vita sentimentale:</b> <?php echo $love_label; ?> - sai se la fiamma è ancora accesa?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - sai se la fiamma è ancora accesa?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Storia d’amore:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/celebrity/<?php echo $post->post_name;?>'>Panoramica sulla vita sentimentale di <?php echo $celebname_label; ?></a></br>
<br>
<b>Colleghi famosi</b>: <?php echo $colleagues ?></br>
<br>
<b>Dati personali:</b> <?php echo $management ?></br>
<br>
<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Case & Proprietà</h2></div>
<br>
<?php if( have_rows('houses') ): ?>
Dove vive <?php echo $celebname_label; ?>?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Ecco <?php else: ?>Ecco <?php endif; ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrizione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Prezzo: <?php echo $price; ?> milioni di dollari • Include: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Attualmente non sono elencate abitazioni.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Auto & Barche</h2></div>
<br>
<?php if( have_rows('car') ): ?>
Auto, barche e altri veicoli di <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> marca <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrizione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Colore: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Prezzo: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Attualmente non solo elencate automobili.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h2>Famiglia</h2></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Foto di famiglia di <?php echo $profession_label; ?><?php if(in_array('vrouw', get_field('love') )): ?>, sposata con <?php echo $current_partner_text ?>,<?php elseif( (in_array('man', get_field('love') ) )): ?>, sposato con <?php echo $current_partner_text ?>, <?php elseif( (in_array('verloofde', get_field('love') ) )): ?>, fidanzata con <?php echo $current_partner_text ?>,<?php elseif( (in_array('verloofde_m', get_field('love') ) )): ?>, fidanzato con <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriend', get_field('love') ) )): ?>, frequentato <?php echo $current_partner_text ?>,<?php elseif( (in_array('vriendin', get_field('love') ) )): ?>, frequentato <?php echo $current_partner_text ?>,<?php endif; ?> celebre per <?php echo $claim_to_fame ?>. ' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php if(in_array('male', get_field('sex') )): ?>Lui <?php else: ?>Lei<?php endif; ?> potrebbe aver stretto amicizia con <?php echo $colleagues ?><?php if( $partners ): ?>, e frequentato <?php echo $partners ?><?php endif; ?>, ma che dire della sua famiglia?</br><br>
<b>Nomi del padre, della madre, dei figli, fratelli & sorelle. </b></br>
<br>						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto di <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>il suo <?php else: ?>il suo<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relazione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Amici</h2></div>
<br>
<?php if( have_rows('friends') ): ?>
<b>Con chi esce questo <?php echo $starsign_label; ?>, di anni <?php echo $age ?> & nato il <?php echo $birth_date ?>?</b></br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto di <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>il suo<?php else: ?>il suo<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
 <em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Si conoscono da: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Animali</h2></div>
<br>
<?php if( have_rows('pets') ): ?>
Gli animali di <?php echo $celebname_label; ?> ed i loro nomi</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Foto di <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>il suo <?php else: ?>il suo<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
 <em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Attualmente non sono presenti animali.
<?php endif; ?>

		</br>

<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Consigli di Look & Bellezza</a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Stile di abbigliamento & consigli</a></b></br>
Cambia da stipendio a patrimonio netto? Aggiungi una casa o una macchina? Indicalo qui sotto.



<?php endif; ?>

<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
													<?php the_content(); ?>
										
													<div class="posts-nav-link">
														<?php posts_nav_link(); ?>
													</div><!--posts-nav-link--></center>
													<?php if(get_option('mvp_article_ad')) { ?>
														<div id="article-ad">
															<?php $articlead = get_option('mvp_article_ad'); if ($articlead) { echo html_entity_decode($articlead); } ?>
														</div><!--article-ad-->
													<?php } ?>
												
												</div><!--content-main-->
											</div><!--content-area-in-->
											<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "full" || $mvp_post_temp == "wide-full" ) { } else { ?>
												<?php get_sidebar(); ?>
											<?php } ?>
										</div><!--content-area-out-->
												</div><!--sec-marg-in-->
											</div><!--sec-marg-out-->
										</div><!--content-area-cont-->
									</div><!--content-area-->
								</div><!--post-body-in-->
							</div><!--post-body-out-->
						</div><!--post-area-->
					</article>
	</div><!--sec-marg-in4-->
</div><!--sec-marg-out4-->
				</div><!--content-wrap-->
				<?php $mvp_show_more = get_option('mvp_show_more'); if ($mvp_show_more == "true") { ?>
				<div class="content-bot-wrap left relative">
					<div class="sec-marg-out relative">
						<div class="sec-marg-in">
					<h4 class="post-header">
						<span class="post-header"><?php _e( 'More in', 'mvp-text' ); ?> <?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
					</h4>
				<div class="archive-list-wrap left relative">
					<div class="archive-list-out">
						<div class="archive-list-in">
							<div class="archive-list-left left relative">
								<ul class="archive-list">
									<?php $mvp_more_num = esc_html(get_option('mvp_more_num')); $category = get_the_category(); $current_cat = $category[0]->cat_ID; $recent = new WP_Query(array( 'cat' => $current_cat, 'posts_per_page' => $mvp_more_num, 'post__not_in' => array( $post->ID ) )); while($recent->have_posts()) : $recent->the_post(); ?>
									<li>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-3"></i>
													</div><!--feat-video-->
												<?php endif; ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-3"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; wp_reset_query(); ?>
								</ul>
							</div><!--archive-list-left-->
						</div><!--archive-list-in-->
						<?php $more_ad = get_option('mvp_more_ad'); if ($more_ad) { ?>
							<div class="archive-ad-wrap relative">
								<?php $more_ad = get_option('mvp_more_ad'); if ($more_ad) { echo html_entity_decode($more_ad); } ?>
							</div><!--archive-ad-wrap-->
						<?php } ?>
					</div><!--archive-list-out-->
				</div><!--archive-list-wrap-->
					</div><!--sec-marg-in-->
				</div><!--sec-marg-out-->
			</div><!--content-bot-wrap-->
		<?php } ?>
		<?php setCrunchifyPostViews(get_the_ID()); ?>
	<?php endwhile; endif; ?>
</div><!--main-content-contain-->
<?php get_footer(); ?>