<?php get_header(); ?>

<?php

						$photo = get_field('photo');
						$born_on = get_field('born_on');
						$birthplace = get_field('birthplace');
						$claim_to_fame = get_field('claim_to_fame');
						$nickname= get_field('nickname');
						$text = get_field('text');
						$place_of_birth = get_field('place_of_birth');
						$birth_date = get_field('birth_date');

		                                $celebname_field = get_field_object('celebname');
						$celebname_value = get_field('celebname');
						$celebname_label = $celebname_field['choices'][ $celebname_value ];

						// fill 'smoker' variable
						//
						$smoker_field = get_field_object('smoker');
						$smoker_value = get_field('smoker');
						$smoker_label = '';

						if(count($smoker_value) < 2) {
						$smoker_label = $smoker_field['choices'][ $smoker_value[0] ];
						} else {
						foreach($smoker_value as $val) {
						$smoker_label .= $smoker_field['choices'][ $val ] . ' ';
						}
						}

						

						// fill 'profession' variable
						//
						$profession_field = get_field_object('profession');
						$profession_value = get_field('profession');
						$profession_label = '';

						if(count($profession_value) < 2) {
						$profession_label = $profession_field['choices'][ $profession_value[0] ];
						} else {
						foreach($profession_value as $val) {
						$profession_label .= $profession_field['choices'][ $val ] . ' ';
						}
						}

// fill 'colors' variable
						//
						$colors_field = get_field_object('colors');
						$colors_value = get_field('colors');
						$colors_label = '';

						if(count($colors_value) < 2) {
						$colors_label = $colors_field['choices'][ $colors_value[0] ];
						} else {
						foreach($colors_value as $val) {
						$colors_label .= $colors_field['choices'][ $val ] . ' ';
						}
						}




// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

						// fill 'hairstyle' variable
						//
						$hairstyle_field = get_field_object('hairstyle');
						$hairstyle_value = get_field('hairstyle');
						$hairstyle_label = '';

						if(count($hairstyle_value) < 2) {
						$hairstyle_label = $hairstyle_field['choices'][ $hairstyle_value[0] ];
						} else {
						foreach($hairstyle_value as $val) {
						$hairstyle_label .= $hairstyle_field['choices'][ $val ] . ', ';
						}
						}

// fill 'facial_hair' variable
						//
						$facial_hair_field = get_field_object('facial_hair');
						$facial_hair_value = get_field('facial_hair');
						$facial_hair_label = '';

						if(count($facial_hair_value) < 2) {
						$facial_hair_label = $facial_hair_field['choices'][ $facial_hair_value[0] ];
						} else {
						foreach($facial_hair_value as $val) {
						$facial_hair_label .= $facial_hair_field['choices'][ $val ] . ', ';
						}
						}

// fill 'hair_length' variable
						//
						$hair_length_field = get_field_object('hair_length');
						$hair_length_value = get_field('hair_length');
						$hair_length_label = '';

						if(count($hair_length_value) < 2) {
						$hair_length_label = $hair_length_field['choices'][ $hair_length_value[0] ];
						} else {
						foreach($hair_length_value as $val) {
						$hair_length_label .= $hair_length_field['choices'][ $val ] . ', ';
						}
						}

// fill 'make_up_style' variable
						//
						$make_up_style_field = get_field_object('make_up_style');
						$make_up_style_value = get_field('make_up_style');
						$make_up_style_label = '';

						if(count($make_up_style_value) < 2) {
						$make_up_style_label = $make_up_style_field['choices'][ $make_up_style_value[0] ];
						} else {
						foreach($make_up_style_value as $val) {
						$make_up_style_label .= $make_up_style_field['choices'][ $val ] . ', ';
						}
						}
						

	     // fill 'hair_type' variable
						//
						$hair_type_field = get_field_object('hair_type');
						$hair_type_value = get_field('hair_type');
						$hair_type_label = $hair_type_field['choices'][ $hair_type_value[0] ];

						
						//
						// get age from birth date
						//						     
						$date = new DateTime($birth_date);
						$now = new DateTime();
						$interval = $now->diff($date);
						$age = $interval->y;
						
						//
						// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

// fill 'nationality' variable
						//
						$nationality_field = get_field_object('nationality');
						$nationality_value = get_field('nationality');
						$nationality_label = '';

						if(count($nationality_value) < 2) {
						$nationality_label = $nationality_field['choices'][ $nationality_value[0] ];
						} else {
						foreach($nationality_value as $val) {
						$nationality_label .= $nationality_field['choices'][ $val ] . '- ';
						}
						}

						//
						// fill 'descript' variable
						//
						$descript_field = get_field_object('descript');
						$descript_value = get_field('descript');
						$descript_label = '';

						if(count($descript_value) < 2) {
						$descript_label = $descript_field['choices'][ $descript_value[0] ];
						} else {
						foreach($descript_value as $val) {
						$descript_label .= $descript_field['choices'][ $val ] . ' ';;
						}
						}

						// fill 'face' variable
						//
						$face_field = get_field_object('face');
						$face_value = get_field('face');
						$face_label = $face_field['choices'][ $face_value[0] ];

						//
						// fill 'eye' variable
						//
						$eye_field = get_field_object('eye');
						$eye_value = get_field('eye');
						$eye_label = $eye_field['choices'][ $eye_value[0] ];
						
						//
						// fill 'hair' variable
						//
						$hair_field = get_field_object('hair');
						$hair_value = get_field('hair');
						$hair_label = $hair_field['choices'][ $hair_value[0] ];

//
						// fill 'tone' variable
						//
						$tone_field = get_field_object('tone');
						$tone_value = get_field('tone');
						$tone_label = $tone_field['choices'][ $tone_value[0] ];


						// fill 'body' variable
						//
						$body_field = get_field_object('body');
						$body_value = get_field('body');
						$body_label = $body_field['choices'][ $body_value[0] ];

			// fill 'skin_type' variable
						//
						$skin_type_field = get_field_object('skin_type');
						$skin_type_value = get_field('skin_type');
						$skin_type_label = $skin_type_field['choices'][ $skin_type_value[0] ];

// fill 'love' variable
						//
						$love_field = get_field_object('love');
						$love_value = get_field('love');
						$love_label = '';

						if(count($love_value) < 2) {
						$love_label = $love_field['choices'][ $love_value[0] ];
						} else {
						foreach($love_value as $val) {
						$love_label .= $love_field['choices'][ $val ] . '- ';
						}
						}


// fill 'extra' variable
						//
						$extra_field = get_field_object('extra');
						$extra_value = get_field('extra');
						$extra_label = '';

						if(count($extra_value) < 2) {
						$extra_label = $love_field['choices'][ $extra_value[0] ];
						} else {
						foreach($extra_value as $val) {
						$extra_label .= $extra_field['choices'][ $val ] . '- ';
						}
						}

// fill 'distinct_feature' variable
						//
						$distinct_feature_field = get_field_object('distinct_feature');
						$distinct_feature_value = get_field('distinct_feature');
						$distinct_feature_label = '';

						if(count($distinct_feature_value) < 2) {
						$distinct_feature_label = $distinct_feature_field['choices'][ $distinct_feature_value[0] ];
						} else {
						foreach($distinct_feature_value as $val) {
						$distinct_feature_label .= $distinct_feature_field['choices'][ $val ] . '- ';
						}
						}





      $height = get_field('height');
						$weight = get_field('weight');
      $endorsements= get_field('endorsements');
						$brands= get_field('brands');
						$hairstyle_photo= get_field('hairstyle_photo');
						$make_up_photo= get_field('make_up_photo');
						$no_make_up_photo= get_field('no_make_up_photo');
						$net_worth= get_field('net_worth');
						$salary= get_field('salary');
						$colleagues= get_field('colleagues');
						$management= get_field('management');
						$fam_photo= get_field('fam_photo');
						$current_partner_text= get_field('current_partner_text');
      $style_photo= get_field('style_photo');
						$dress_size= get_field('dress_size');
						$shoe_size= get_field('shoe_size');
						$bra_size= get_field('bra_size');
						$biceps= get_field('biceps');
						$bust= get_field('bust');
						$waist= get_field('waist');
						$butt= get_field('butt');
						$beach= get_field('beach');
						$house= get_field('house');
			   $partners= get_field('partners');
						$smoking_photo= get_field('smoking_photo');
						$no_makeup= get_field('no_makeup');
						$since= get_field('since');

//
						// get zodiac sign from birth date
						//	
						function zodiac($birthdate) {
						
							$zodiac = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiac = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiac = "Taurus"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiac = "Gemini"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiac = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiac = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiac = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiac = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiac = "Scorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiac = "Sagittarius"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiac = "Capricorn"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiac = "Aquarius"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiac = "Pisces"; } 
						     
							return $zodiac; 

						}
						
						$starsign_label = zodiac($birth_date, $sex);

// get zodiac sign from birth date
						//	
						function zodiacde($birthdate) {
						
							$zodiacde = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacde = "Widder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacde = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacde = "Zwillinge"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacde = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacde = "Löwe"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacde = "Jungfrau"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacde = "Waage"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacde = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacde = "Schütze"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacde = "Steinbock"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacde = "Wassermann"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacde = "Fische"; } 
						     
							return $zodiacde;

						}
						
						$starsignde_label = zodiacde($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacfr($birthdate) {
						
							$zodiacfr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacfr = "Bélier"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacfr = "Taureau"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacfr = "Gémaux"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacfr = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacfr = "Lion"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacfr = "Vierge"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacfr = "Balance"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacfr = "Scorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacfr = "Sagittaire"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacfr = "Capricorne"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacfr = "Verseau"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacfr = "Poisons"; } 
						     
							return $zodiacfr;

						}

						
						$starsignfr_label = zodiacfr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacbr($birthdate) {
						
							$zodiacbr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacbr = "Carneiro"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacbr = "Touro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacbr = "Gémeos"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacbr = "Caranguejo"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacbr = "Leão"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacbr = "Virgem"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacbr = "Balança"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacbr = "Escorpião"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacbr = "Sagitário"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacbr = "Capricórnio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacbr = "Aquário"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacbr = "Peixes"; } 
						     
							return $zodiacbr;

						}

						$starsignbr_label = zodiacbr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiaces($birthdate) {
						
							$zodiaces = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiaces = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiaces = "Tauro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiaces = "Geminis"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiaces = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiaces = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiaces = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiaces = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiaces = "Escorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiaces = "Sagitario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiaces = "Capricornio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiaces = "Acuario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiaces = "Piscis"; } 
						     
							return $zodiaces;

}
						$starsignes_label = zodiaces($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacpl($birthdate, $sex) {
						
							$zodiacpl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacpl = "Baran"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacpl = "Byk"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacpl = "Bliźnięta"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacpl = "Rak"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacpl = "Lew"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacpl = "Panna"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacpl = "Waga"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacpl = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacpl = "Strzelec"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacpl = "Koziorożec"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacpl = "Wodnik"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacpl = "Ryby"; } 
						     

						     
							return $zodiacpl;

}
			
						$starsignpl_label = zodiacpl($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacda($birthdate, $sex) {
						
							$zodiacda = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacda = "Vædder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacda = "Tyr"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacda = "Tvilling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacda = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacda = "Løve"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacda = "Jomfru"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacda = "Vægt"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacda = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacda = "Skytte"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacda = "Stenbuk"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacda = "Vandmand"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacda = "Fisk"; } 

						     

						     
							return $zodiacda;

}
			
						$starsignda_label = zodiacda($born_on, $sex);


// get zodiac sign from birth date
						//	
						function zodiacsv($birthdate, $sex) {
						
							$zodiacsv = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacsv = " Väduren "; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacsv = "Oxen"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacsv = " Tvillingarna "; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacsv = "Kräftan"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacsv = "Lejonet"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacsv = " Jungfrun "; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacsv = " Vågen "; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacsv = " Skorpionen "; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacsv = " Skytten "; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacsv = " Stenbocken "; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacsv = " Vattumannen "; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacsv = " Fiskarna "; } 
 

						     

						     
							return $zodiacsv;

}
			
						$starsignsv_label = zodiacsv($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacnb($birthdate, $sex) {
						
							$zodiacnb = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnb = "Væren"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnb = "Tyren"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnb = "Tvillingene"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnb = "Krepsen"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnb = "Løven"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnb = "Jomfruen"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnb = "Vekten"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnb = "Skorpionen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnb = "Skytten"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnb = "Stenbukken"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnb = "Vannmannen"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnb = "Fiskene"; } 

						     

						     
							return $zodiacnb;

}
			
						$starsignnb_label = zodiacnb($born_on, $sex);



// get zodiac sign from birth date
						//	
						function zodiacin($birthdate, $sex) {
						
							$zodiacin = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacin = "मेष"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacin = "वृषभ"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacin = "मिथुन"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacin = "कर्क"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacin = "सिंह"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacin = "कन्या"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacin = "तुला"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacin = "वृश्चिक"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacin = "धनु"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacin = "मकर"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacin = "कुंभ"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacin = "मीन"; } 
						     
							return $zodiacin;

}
			
						$starsignin_label = zodiacin($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiactr($birthdate, $sex) {
						
							$zodiactr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiactr = "Koç"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiactr = "Boğa"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiactr = "İkizler"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiactr = "Yengeç"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiactr = "Aslan"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiactr = "Başak"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiactr = "Terazi"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiactr = "Akrep"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiactr = "Yay"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiactr = "Oğlak"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiactr = "Kova"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiactr = "Balık"; } 
						     
							return $zodiactr;

}

						$starsigntr_label = zodiactr($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacnl($birthdate, $sex) {
						
							$zodiacnl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnl = "Ram"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnl = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnl = "Tweeling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnl = "Kreeft"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnl = "Leeuw"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnl = "Maagd"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnl = "Weegschaal"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnl = "Schorpioen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnl = "Boogschutter"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnl = "Steenbok"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnl = "Waterman"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnl = "Vissen"; } 
						     
							return $zodiacnl;

}

						$starsignnl_label = zodiacnl($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacit($birthdate, $sex) {
						
							$zodiacit = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacit = "Ariete"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacit = "Toro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacit = "Gemelli"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacit = "Cancro"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacit = "Leone"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacit = "Vergine"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacit = "Bilancia"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacit = "Scorpione"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacit = "Sagittario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacit = "Capricorno"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacit = "Aquario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacit = "Pesci"; } 
						     
							return $zodiacit;

}

						$starsignit_label = zodiacit($born_on, $sex);
						

						//$starsign_field = get_field_object('starsign');
						//$starsign_value = get_field('starsign');
						//$starsign_label = $starsign_field['choices'][ $starsign_value[0] ];	


						?>
						




<div id="main-content-contain" class="left relative infinite-content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div id="content-wrap" class="left relative"  itemscope itemtype="http://schema.org/Article">
<div class="sec-marg-out4 relative">
	<div class="sec-marg-in4">
					<article <?php post_class(); ?>>
						<div id="post-header" class="left relative">
							<div class="sec-marg-out relative">
								<div class="sec-marg-in">
									<div class="post-info-top left relative">
									</div><!--post-info-top-->
									<h1 class="post-title left" itemprop="name headline"><?php the_title(); ?></h1>

									<span class="post-excerpt"><?php the_excerpt(); ?>
<span style='color: #777;
	float: left;
	font-size: 1.4rem;
	font-style: italic;
	font-weight: 400;
	line-height: 140%;
	width: 100%;
	'>
<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Get <?php echo $celebname_label; ?>’s clothing style - from her tattoos favorite jackets, shirts & jeans to shoes, jewelry & underwear. Learn from her height, weight & waist, butt & bra measurements & tips.
<?php else: ?> 
Get <?php echo $celebname_label; ?>’s clothing style - from his tattoos, favorite jackets, dresses, skirts & jeans to shoes, jewelry & underwear. Learn from his height, weight, body measurements & tips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Get <?php echo $celebname_label; ?>'s look - from mascara, eye liner & hair products to nail polish & fragrances. Check out her real hair and eye color & learn about her health (smoker?) & her beauty types & tips. 
<?php else: ?>
Get <?php echo $celebname_label; ?>'s look - from his wax, moisturizer & hair products to fragrances. Check out his real hair and eye color & learn about his health (smoker?) & his beauty types & tips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Celebrity homes - from their mother, father, kids & friends to their houses & cars, net worth & salary.
<?php endif; ?> 
<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Kleed je net zoals je favoriete beroemdheid – van jassen, jurken, rokjes & spijkerbroeken tot aan schoenen, sieraden & ondergoed. Kom meer te weten over kledingmaten & leer van fashion tips van beroemdheden.
<?php else: ?> 
Kleed je net zoals je favoriete beroemdheid – van jassen, t-shirts & spijkerbroeken tot aan schoenen, sieraden & ondergoed. Kom meer te weten over kledingmaten & leer van fashion tips van beroemdheden.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Neem de look van <?php echo $celebname_label; ?>! – van mascara, eye liner & haarproducten tot aan nagellak & geurtjes. Check haar echte haarkleur en oogkleur en lees over haar gezondheid (rookt ze?) & haar schoonheidstips.
<?php else: ?>
Neem de look van <?php echo $celebname_label; ?>! – van wax, gel, moisturizer & haarproducten tot aan geurtjes. Check zijn echte haarkleur en oogkleur en lees over zijn gezondheid (rookt hij?) & schoonheidstips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Huizen van sterren – van de moeder, de vader, de kinderen & de vrienden tot aan de huizen & auto’s, het vermogen & salaris van je favoriete beroemdheid.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Holen Sie sich einen Promi-Stil - von ihren Lieblings-Jacken, Kleidern, Röcken und Jeans zu Schuhen, Schmuck und Unterwäsche. Lernen Sie von ihren Maßen & Tipps.
<?php else: ?> 
Holen Sie sich den Kleidungsstil eines Promis - von ihren Lieblings-Jacken, Hemden und Jeans zu Schuhen, Schmuck und Unterwäsche. Lernen Sie von ihren Maßen & Tipps.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Holen Sie sich einen Promi-Look - von Wimperntusche, Eyeliner & Haarkosmetik zu Nagellack & Düften. Lernen Sie von ihren Schönheitstypen & Tipps.
<?php else: ?>
Holen Sie sich den Promi-Look - von Haarwachs, Feuchtigkeitscreme & Haarkosmetik zu Düften. Lernen Sie von ihren Schönheitstypen & Tipps.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Promi-Häuser - von ihren Müttern, Vätern, Kindern und Freunden zu ihren Häusern und Autos, Nettowert & Gehalt.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Obtenez un style de célébrité - depuis leur favoris vestes, 
 robes, jupes & jeans jusqu’aux chaussures, bijoux et sous-vêtements. S'inspirer de leurs mesures et conseils.
<?php else: ?> 
Obtenez un style de vêtements de célébrité - leurs vestes, chemises, jeans et chaussures préférés, leurs bijoux et sous-vêtements. S'inspirer de leurs mesures et conseils.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Obtenez un look de célébrité – mascara, eye-liner et produits capillaires  ainsi que vernis à ongles et parfums. S'inspirer de leurs types et conseils de beauté. 
<?php else: ?>
Obtenez le look de célébrité – depuis la crème pour cheveux, en passant par les crèmes hydratantes pour cheveux jusqu’aux parfums. S'inspirer de leurs types et conseils de beauté.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Maisons de célébrités - de leur mère, des père, des enfants et des amis à leurs maisons, valeur nette & voitures, salaire.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Tenga el estilo de ropas de <?php echo $celebname_label; ?> - de sus tatuajes, chaquetas, vestidos y faldas favoritas, a sus zapatos, joyas y ropa íntima. Aprende de sus medidas de altura, peso y cintura, trasero y sostén y consejos.
<?php else: ?> 
Tenga el estilo de ropas de <?php echo $celebname_label; ?> - de sus tatuajes, chaquetas, camisetas y jeans favoritos a sus zapatos, joyas y ropa íntima. Aprende de sus medidas de altura, peso y cuerpo y consejos.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Tenga la apariencia de <?php echo $celebname_label; ?> - de máscara, delineador de ojos y productos de cabello a pulidores de uña y fragancias. Vea sus colores de cabello y ojos reales y aprende de su salud (¿fumante?) y su tipo y consejos de belleza. 
<?php else: ?>
Tenga la apariencia de <?php echo $celebname_label; ?> - de su cera, hidratante y productos de cabello a fragancias. Vea sus colores de cabello y ojos reales y aprende de su salud (¿fumante?) y su tipo y consejos de belleza. 
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Casas de celebridades – de su madre, padre, hijos y amigos a sus casas y carros, coste total e salario. 
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Tenha o estilo de roupas de <?php echo $celebname_label; ?> - das suas tatuagens, jaquetas, vestidos e saias favoritos, a seus sapatos, jóias e roupa íntima. Aprenda de suas medidas de altura, peso e cintura, bunda e sutiã e dicas.
<?php else: ?> 
Tenha o estilo de roupas de <?php echo $celebname_label; ?> - das suas tatuagens, jaquetas, camisetas e jeans favoritos a seus sapatos, jóias e roupa íntima. Aprenda de suas medidas de altura, peso e corpo e dicas.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Tenha a aparência de <?php echo $celebname_label; ?> - de máscara, delineador de olhos e produtos de cabelo a polidores de unha e fragrâncias. Veja suas cores de cabelo e olhos reais e aprenda de sua saúde (fumante?) e seu tipo e dicas de beleza. 
<?php else: ?>
Tenha a aparência de <?php echo $celebname_label; ?> - de sua cera, hidratante e produtos de cabelo a fragrâncias. Veja suas cores de cabelo e olhos reais e aprenda de sua saúde (fumante?) e seu tipo e dicas de beleza.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Casas de celebridades – da sua mãe, pai, filhos e amigos a suas casas e carros, patrimônio líquido e salário. 
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Impara lo stile di abbigliamento di  <?php echo $celebname_label; ?> - come i suoi tatuaggi, le giacche preferite, abiti, gonne e jeans, per passare alle scarpe, gioielli e biancheria intima. . Impara dai consigli e dalla sua altezza, peso e girovita, misure della testa e di reggiseno.<?php else: ?>
Impara lo stile di abbigliamento di  <?php echo $celebname_label; ?> - come i suoi tatuaggi, le giacche preferite, abiti, pantaloni e jeans, per passare alle scarpe, gioielli e biancheria intima. Impara dai suoi consigli e dalla sua altezza, peso e misure del corpo.
 <?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Prendi spunto dal look di <?php echo $celebname_label; ?> - dai prodotti come mascara, eye liner e per capelli agli smalti per unghie e fragranze. Scopri il suo vero colore dei capelli e degli occhi e conosci di più sulla sua salute (fuma?) ed il suo stile e consigli di bellezza.<?php else: ?>
Prendi spunto dal look di <?php echo $celebname_label; ?> - dalla sua cera, alle creme idratanti e prodotti per capelli, alle fragranze. Scopri il suo vero colore dei capelli e degli occhi e conosci di più sulla sua salute (fuma?) ed il suo stile e consigli di bellezza.<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
La vita delle celebrità - dalle madri, ai padri, figli e amici, alle loro case, automobili, patrimonio netto e salario.
<?php endif; ?>
<?php endif; ?>
</span>
</span>
								
								</div><!--sec-marg-in-->
							</div><!--sec-marg-out-->
						</div><!--post-header-->
						<div id="post-area" class="left relative">
							<div class="post-body-out">
								<?php $mvp_author = get_option('mvp_author_box'); $mvp_social_box = get_option('mvp_social_box'); if ( $mvp_social_box == "false" && $mvp_author == "false" ) { } else { ?>
								<div class="post-info-left left relative">
									<div class="sec-marg-out relative">
										<div class="sec-marg-in">
									<div class="post-info-left-top left relative">
										<?php $mvp_author = get_option('mvp_author_box'); if ($mvp_author == "true") { ?>
											<div class="author-info left relative">
												<div class="author-img left relative">
													<?php echo get_avatar( get_the_author_meta('email'), '120' ); ?>
												</div><!--author-img-->
												<span class="author-name vcard fn" itemprop="author"><?php the_author_posts_link(); ?></span>
											</div><!--author-info-->
										<?php } ?>
										<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
											<?php mvp_share_count(); ?>
										<?php } ?>
									</div><!--post-info-left-top-->
									<div class="post-info-left-bot left relative">
									<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
								<div class="post-soc-out">
									<div class="post-soc-in">
										<div class="post-soc-share left relative">
											<div class="soc-count-mobi">
												<?php mvp_share_count(); ?>
											</div><!--soc-count-mobi-->
											<a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><div class="post-soc-fb left relative"><i class="fa fa-facebook fa-2"></i><span class="post-share-text"><?php _e( 'Share', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $fb_share = get_fb( $post_id ); if($fb_share==0) { } else { echo esc_html( $fb_share ); } ?></span></div><!--post-soc-fb--></a>
											<a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?> -&url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><div class="post-soc-twit left relative"><i class="fa fa-twitter fa-2"></i><span class="post-share-text"><?php _e( 'Tweet', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $twit_share = get_tweets( $post_id ); if($twit_share==0) { } else { echo esc_html( $twit_share ); } ?></span></div><!--post-soc-twit--></a>
											<a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><div class="post-soc-pin left relative"><i class="fa fa-pinterest fa-2"></i><span class="post-share-text"><?php _e( 'Pin', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $pin_share = get_pinterest( $post_id ); if($pin_share==0) { } else { echo esc_html( $pin_share ); } ?></span></div><!--post-soc-pin--></a>
										</div><!--post-soc-share-->
									</div><!--post-soc-in-->
								</div><!--post-soc-out-->
									<?php } ?>
									<?php $prev_next = get_option('mvp_prev_next'); if ($prev_next == "true") { ?>
										<div class="prev-next-posts left relative">
											<?php mvp_prev_next_links(); ?>
										</div><!--prev-next-posts-->
									<?php } ?>
											</div><!--post-info-left-bot-->
										</div><!--sec-marg-in-->
									</div><!--sec-marg-out-->
								</div><!--post-info-left-->
								<?php } ?>
								<div class="post-body-in">
									<div id="content-area" class="left relative" itemprop="articleBody">
										<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "wide" || $mvp_post_temp == "wide-full" ) { } else { ?>
										<?php $mvp_featured_img = get_option('mvp_featured_img'); if ($mvp_featured_img == "true") { ?>
						<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
							<div id="video-embed" class="left relative sec-feat">
								<?php echo get_post_meta($post->ID, "mvp_video_embed", true); ?>
							</div><!--video-embed-->
						<?php else: ?>
							<?php $mvp_show_hide = get_post_meta($post->ID, "mvp_featured_image", true); if ($mvp_show_hide == "hide") { ?>
							<?php } else { ?>
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div id="featured-image" class="left relative sec-feat">
										<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb', array( 'class' => 'unlazy' ) ); ?>
										<img itemprop="image" src="<?php echo $thumb['0']; ?>" />
										<?php if(get_post_meta($post->ID, "mvp_photo_credit", true)): ?>
											<div id="featured-caption">
												<?php echo get_post_meta($post->ID, "mvp_photo_credit", true); ?>
											</div><!--featured-caption-->
										<?php endif; ?>
									</div><!--featured-image-->
								<?php } ?>
							<?php } ?>
						<?php endif; ?>
										<?php } ?>
										<?php } ?>
									<div class="content-area-cont left relative">
										<div class="sec-marg-out relative">
											<div class="sec-marg-in">
										<div class="content-area-out">
											<div class="content-area-in">
												<div class="content-main left relative">
													<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "wide" || $mvp_post_temp == "wide-full" ) { } else { ?>
										<?php $mvp_featured_img = get_option('mvp_featured_img'); if ($mvp_featured_img == "true") { ?>
						<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
							<div id="video-embed" class="left relative prim-feat">
								<?php echo get_post_meta($post->ID, "mvp_video_embed", true); ?>
							</div><!--video-embed-->
						<?php else: ?>
							<?php $mvp_show_hide = get_post_meta($post->ID, "mvp_featured_image", true); if ($mvp_show_hide == "hide") { ?>
							<?php } else { ?>
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div id="featured-image" class="left relative prim-feat">
										<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb', array( 'class' => 'unlazy' ) ); ?>
										<img itemprop="image" src="<?php echo $thumb['0']; ?>" />
										<?php if(get_post_meta($post->ID, "mvp_photo_credit", true)): ?>
											<div id="featured-caption">
												<?php echo get_post_meta($post->ID, "mvp_photo_credit", true); ?>
											</div><!--featured-caption-->
										<?php endif; ?>
									</div><!--featured-image-->
								<?php } ?>
							<?php } ?>
						<?php endif; ?>
										<?php } ?>
										<?php } ?>
<center>                        		
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='Tips: <?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s <?php echo $hairstyle_label; ?> hair style of the <?php echo $descript_label; ?> <?php echo $profession_label; ?>' "/></a>
<b><?php echo $celebname_label; ?> <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> beauty & look are characterized by <?php if(in_array('male', get_field('sex') )): ?>his<?php else: ?>her<?php endif; ?> <?php echo $hair_length_label; ?> & <?php echo $hairstyle_label; ?> hairstyle. <?php if(in_array('male', get_field('sex') )): ?>Ladies<?php else: ?>Men<?php endif; ?> love <?php if(in_array('male', get_field('sex') )): ?>him<?php else: ?>her<?php endif; ?> for <?php if(in_array('male', get_field('sex') )): ?>his<?php else: ?>her<?php endif; ?> <?php echo $distinct_feature_label; ?><?php if(in_array('male', get_field('sex') )): ?>.<?php else: ?>, and <?php echo $make_up_style_label; ?> make-up style.<?php endif; ?> <?php echo $celebname_label; ?>, the <?php echo $age ?>-year old <?php echo $nationality_label; ?> <?php echo $profession_label; ?> from <?php echo $place_of_birth ?> was born on <b><?php echo $birth_date ?></b>. You might know <?php if(in_array('male', get_field('sex') )): ?>him <?php else: ?>her<?php endif; ?> from <?php echo $claim_to_fame ?>.
</br><br>
Want to look like <?php echo $celebname_label; ?>? </br></b>
<i>Here is what you need to know:</i>
<br>
<br>
This <?php echo $descript_label; ?> <?php echo $profession_label; ?> makes commercials for <?php echo $endorsements; ?>, but actually uses: <?php echo $brands ?>. </br><br>
<b>Face shape/type:</b> <?php echo $face_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "facial_hair", true) ): ?>
<b>Beard or Mustache:</b> <?php echo $facial_hair_label; ?></br>
<br>
<?php endif; ?>
<b>Distinct feature:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Hair Type:</b> <?php echo $hair_type_label; ?></br>
<br>
<b>Hair Color:</b> <?php echo $hair_label; ?></br>
<br>
<b>Eye Color:</b> <?php echo $eye_label; ?></br>
<br>
<b>Skin Tone/Complexion:</b> <?php echo $tone_label; ?></br>
<br>
<b>Skin Type:</b> <?php echo $skin_type_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "bra_size", true) ): ?>
<b>Bra-size:</b> <?php echo $bra_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Biceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Buste:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Waist:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Butt:</b> <?php echo $butt ?></br><br>
<?php endif; ?>
<b>Health // Does <?php echo $celebname_label; ?> smoke?</b> <?php if( get_post_meta($post->ID, "smoker", true) ): ?><?php echo $smoker_label; ?><?php else: ?>No smoking records<?php endif; ?></br><br>
<?php if( $smoking_photo ): ?>
<a rel="nofollow" href="<?php echo $smoking_photo?>"><img src="<?php echo $smoking_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Photo of the in <?php echo $place_of_birth ?> born <?php echo $starsign_label; ?>, <?php echo $celebname_label; ?> smoking in <?php echo date("Y"); ?> at age <?php echo $age ?>' "/></a></br><br><?php endif; ?>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> favorite beauty products & tips</h2>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Skincare & makeup </h2></div>
<br>
<br>
<?php if( $make_up_photo ): ?>
<b>With makeup:</b></br><a rel="nofollow" href="<?php echo $make_up_photo?>"><img src="<?php echo $make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Photo of <?php echo $celebname_label; ?> and her <?php echo date("Y"); ?> <?php echo $make_up_style_label; ?> make up style, products & tips' "/></a></br>
<br>
<?php endif; ?>
<?php if( $no_make_up_photo ): ?>
<b>Without make-up:</b></br><a rel="nofollow" href="<?php echo $no_make_up_photo?>"><img src="<?php echo $no_make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Photo of the <?php echo $descript_label; ?> <?php echo $celebname_label; ?> from <?php echo $place_of_birth ?> without makeup' "/></a></br>
<br>
<b>Must-read:</br></b>
<a href='http://muzul.com/10-huge-celebrity-make-up-mistakes/'>- <b>Top 10 Huge Celebrity make-up disasters</b></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('make_up') ): ?>
<?php echo $celebname_label; ?>'s make-up & skin products, like moisturizers <?php if(in_array('male', get_field('sex') )): ?>& foundations.<?php else: ?>, foundations & lipstick.<?php endif; ?>
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('make_up') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Currently no make-up & skin products listed.
<?php endif; ?>
		</br><br>

<?php if(in_array('male', get_field('sex') )): ?><?php else: ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Eye Make-up</h2></div>
<br>
<?php if( have_rows('eye_products') ): ?>
Our selection of <?php echo $celebname_label; ?>'s favorite mascara, eye liner & other eye make-up. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('eye_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"> <h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no Eye makeup & eye products listed.
<?php endif; ?>
		</br>

<br>

<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Hair products</h2></div>
<br>
<br>
<?php if( $hairstyle_photo ): ?><a rel="nofollow" href="<?php echo $hairstyle_photo?>"><img src="<?php echo $hairstyle_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='<?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> <?php echo $hair_label; ?> hair & <?php echo $hairstyle_label; ?> hair style. Current length:  <?php echo $hair_length_label; ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('hair_products') ): ?>
<?php echo $celebname_label; ?>'s wax, hairspray & shampoo's etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('hair_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no wax, hairspray & shampoo's etc listed.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Fragrances & perfumes</h2></div>
<br>
<?php if( have_rows('fragrances') ): ?>
<?php echo $celebname_label; ?>'s fragrances & perfumes </br>
<br>
						<ul class="slides">

						<?php while( have_rows('fragrances') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
		
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no fragrances & perfumes listed.
<?php endif; ?>
		</br>
<br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2><?php if(in_array('male', get_field('sex') )): ?>Body products, trimming & shavers
<?php else: ?>Body products & Nail Polish<?php endif; ?></h2></div>
<br>
<?php if( have_rows('body_products') ): ?>
<?php echo $celebname_label; ?>'s nail polish, shavers & body products</br>
<br>
						<ul class="slides">

						<?php while( have_rows('body_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Currently no body products like shavers & trimmers & nail polish listed.
<?php endif; ?>
</div>
<?php if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } ?>
- <b><a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Clothing style & tips</a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Lifestyle, friends & family</a></b></br>
Share smoking photos or styling & beauty tips to look like <?php echo $celebname_label; ?>, below:
</br>

<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='Tips: <?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s <?php echo $hairstyle_label; ?> hair style of the <?php echo $descript_label; ?> <?php echo $profession_label; ?>' "/></a>

<b><?php echo $celebname_label; ?> <?php if(in_array('male', get_field('sex') )): ?>zijn <?php else: ?>haar<?php endif; ?> schoonheid & uiterlijk worden gekarakteriseerd door <?php if(in_array('male', get_field('sex') )): ?>zijn<?php else: ?>haar<?php endif; ?> <?php echo $hair_length_label; ?> & <?php echo $hairstyle_label; ?> haarstijl. <?php if(in_array('male', get_field('sex') )): ?>Vrouwen<?php else: ?>Mannen<?php endif; ?> houden van <?php if(in_array('male', get_field('sex') )): ?>hem<?php else: ?>haar<?php endif; ?> vanwege <?php if(in_array('male', get_field('sex') )): ?>zijn<?php else: ?>haar<?php endif; ?> <?php echo $distinct_feature_label; ?><?php if(in_array('male', get_field('sex') )): ?>.<?php else: ?>, en <?php echo $make_up_style_label; ?> make-up stijl.<?php endif; ?> <?php echo $celebname_label; ?>, de <?php echo $age ?> jaar oude <?php echo $nationality_label; ?> <?php echo $profession_label; ?> uit <?php echo $place_of_birth ?> werd geboren op <b><?php echo $birth_date ?></b>. Je kent <?php if(in_array('male', get_field('sex') )): ?>hem <?php else: ?>haar<?php endif; ?> misschien van <?php echo $claim_to_fame ?>.
</br><br>
Wil je er net zo uit zien als <?php echo $celebname_label; ?>? </br></b>
<i>Dan moet je het volgende weten:</i>
<br>
<br>
Deze <?php echo $descript_label; ?> <?php echo $profession_label; ?> maakt reclame voor <?php echo $endorsements; ?>, maar gebruikt zelf merken zoals <?php echo $brands ?>. </br><br>
<b>Gezichtsvorm/type:</b> <?php echo $face_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "facial_hair", true) ): ?>
<b>Baard of snor:</b> <?php echo $facial_hair_label; ?></br>
<br>
<?php endif; ?>
<b>Meest kenmerkende uiterlijke eigenschap:</b> <?php echo $distinct_feature_label; ?></br>
<br>
<b>Haartype:</b> <?php echo $hair_type_label; ?></br>
<br>
<b>Haarkleur:</b> <?php echo $hair_label; ?></br>
<br>
<b>Oogkleur:</b> <?php echo $eye_label; ?></br>
<br>
<b>Huidtint/kleur:</b> <?php echo $tone_label; ?></br>
<br>
<b>Huidtype:</b> <?php echo $skin_type_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "bra_size", true) ): ?>
<b>Beha-maat:</b> <?php echo $bra_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Biceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Buste:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Taille:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Billen:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<b>Gezondheid //Rookt <?php echo $celebname_label; ?>?</b> <?php if( get_post_meta($post->ID, "smoker", true) ): ?><?php echo $smoker_label; ?><?php else: ?>Geen bewijs van roken<?php endif; ?></br><br>
<?php if( $smoking_photo ): ?>
<a rel="nofollow" href="<?php echo $smoking_photo?>"><img src="<?php echo $smoking_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto van de in <?php echo $place_of_birth ?> geboren <?php echo $starsign_label; ?>, <?php echo $celebname_label; ?> die rookt in <?php echo date("Y"); ?> op <?php echo $age ?>-jarige leeftijd' "/></a></br><br><?php endif; ?>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> favoriete beautyproducten & tips</h2>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<br></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Huidverzorging & make-up </h2></div>
<br>
<br>
<?php if( $make_up_photo ): ?>
<b>Met make-up:</b></br><a rel="nofollow" href="<?php echo $make_up_photo?>"><img src="<?php echo $make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt=Foto van <?php echo $celebname_label; ?> en haar <?php echo date("Y"); ?> <?php echo $make_up_style_label; ?> make-up stijl, producten & tips' "/></a></br>
<br>
<?php endif; ?>
<?php if( $no_make_up_photo ): ?>
<b>Zonder make-up:</b></br><a rel="nofollow" href="<?php echo $no_make_up_photo?>"><img src="<?php echo $no_make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt=Foto van de <?php echo $descript_label; ?> <?php echo $celebname_label; ?> uit <?php echo $place_of_birth ?> zonder make-up' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('make_up') ): ?>
<?php echo $celebname_label; ?>'s make-up & huidverzorgingsproducten, zoals moisturizers <?php if(in_array('male', get_field('sex') )): ?>& foundations.<?php else: ?>, foundations & lipstick.<?php endif; ?>
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('make_up') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> gebruikt <?php echo $name; ?> als <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product ' "/></a>
<em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product.<br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Op dit moment nog geen make-up of huidproducten bekend.
<?php endif; ?>
		</br><br>

<?php if(in_array('male', get_field('sex') )): ?><?php else: ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Oog Make-up</h2></div>
<br>
<?php if( have_rows('eye_products') ): ?>
Onze selectie van <?php echo $celebname_label; ?>'s favoriete mascara, eye liner & overige oog make-up. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('eye_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> gebruikt <?php echo $name; ?> zoals <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product ' "/></a>
<em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product.<br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Op dit moment nog geen oog make-up & oogverzorging producten bekend.
<?php endif; ?>
		</br>

<br>

<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Haarproducten</h2></div>
<br>
<br>
<?php if( $hairstyle_photo ): ?><a rel="nofollow" href="<?php echo $hairstyle_photo?>"><img src="<?php echo $hairstyle_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='<?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> <?php echo $hair_label; ?> haar & <?php echo $hairstyle_label; ?> haarstijl. Huidige lengte:  <?php echo $hair_length_label; ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('hair_products') ): ?>
<?php echo $celebname_label; ?>'s wax, haarspray & shampoo's etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('hair_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> gebruikt <?php echo $name; ?> zoals <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product ' "/></a>
<em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product.<br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Op dit moment nog geen wax, haarspray of shampoo’s etc. bekend.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Geurtjes & parfums</h2></div>
<br>
<?php if( have_rows('fragrances') ): ?>
<?php echo $celebname_label; ?>’s geurtjes & parfums </br>
<br>
						<ul class="slides">

						<?php while( have_rows('fragrances') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
		
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> gebruikt <?php echo $name; ?> zoals <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product ' "/></a>
<em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product.<br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Op dit moment nog geen geurtjes & parfums bekend.
<?php endif; ?>
		</br>
<br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2><?php if(in_array('male', get_field('sex') )): ?>Lichaamsproducten & scheerapparaten
<?php else: ?>Lichaamsproducten & Nagellak<?php endif; ?></h2></div>
<br>
<?php if( have_rows('body_products') ): ?>
<?php echo $celebname_label; ?>'s nagellak, scheerapparaten & lichaamsproducten </br>
<br>
						<ul class="slides">

						<?php while( have_rows('body_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> gebruikt <?php echo $name; ?> zoals <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product ' "/></a>
<em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> product.<br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Op dit moment nog geen lichaamsproducten zoals scheerapparaten & trimmers bekend.
<?php endif; ?>
		</br>
</div>
<?php if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } ?>
- <b><a href='http://muzul.com/nl/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Kledingstijl & tips</a></br>
- <a href='http://muzul.com/nl/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Lifestyle, vrienden & familie</a></b></br><br>
<i>Laatste update: <?php the_modified_date(); ?></i>
</br>
<br>
Deel rook foto's en styling- & schoonheidstips om te lijken op <?php echo $celebname_label; ?> hieronder:
</br>

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='Tips: <?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s <?php echo $hairstyle_label; ?> hair style of the <?php echo $descript_label; ?> <?php echo $profession_label; ?>' "/></a>

<b><?php echo $celebname_label; ?><?php if(in_array('male', get_field('sex') )): ?>’s<?php else: ?>’s<?php endif; ?> Schönheit & Aussehen ist gekennzeichnet durch <?php if(in_array('male', get_field('sex') )): ?>sein<?php else: ?>ihr<?php endif; ?> <?php echo $hair_length_label; ?> & <?php echo $hairstyle_label; ?> Haarstil. <?php if(in_array('male', get_field('sex') )): ?>Damen<?php else: ?>Herren<?php endif; ?> lieben <?php if(in_array('male', get_field('sex') )): ?>ihn<?php else: ?>sie<?php endif; ?> für <?php if(in_array('male', get_field('sex') )): ?>seine<?php else: ?>ihre<?php endif; ?> <?php echo $distinct_feature_label; ?><?php if(in_array('male', get_field('sex') )): ?>.<?php else: ?>, und <?php echo $make_up_style_label; ?> dem Make-up Stil.<?php endif; ?> <?php echo $celebname_label; ?>, der <?php echo $age ?> Jahre alte <?php echo $profession_label; ?> aus <?php echo $place_of_birth ?> wurde geboren am <b><?php echo $birth_date ?></b>. Sie mögen <?php if(in_array('male', get_field('sex') )): ?>ihn <?php else: ?>sie<?php endif; ?> kennen von <?php echo $claim_to_fame ?>.
</br><br>
Wollen Sie aussehen wie <?php echo $celebname_label; ?>? </br></b>
<i>Das ist was Sie wissen müssen:</i>
<br>
<br>
Dieser <?php echo $descript_label; ?> <?php echo $profession_label; ?> macht Werbung für <?php echo $endorsements; ?>, und benutzt tatsächlich: <?php echo $brands ?>. </br><br>
<b>Gesichtsform/-typ:</b> <?php echo $face_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "facial_hair", true) ): ?>
<b>Bart oder Schnurrbart:</b> <?php echo $facial_hair_label; ?></br>
<br>
<?php endif; ?>
<b>Ausgeprägtes Merkmal:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Haartyp:</b> <?php echo $hair_type_label; ?></br>
<br>
<b>Haarfarbe:</b> <?php echo $hair_label; ?></br>
<br>
<b>Augenfarbe:</b> <?php echo $eye_label; ?></br>
<br>
<b>Hautton/Teint:</b> <?php echo $tone_label; ?></br>
<br>
<b>Hauttyp:</b> <?php echo $skin_type_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "bra_size", true) ): ?>
<b>Büstenhalter-Größe:</b> <?php echo $bra_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Bizeps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Busen:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Taille:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Po:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> Lieblings-Beauty-Produkte & Tipps</h2>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Hautpflege und Make-up </h2></div>
<br>
<?php if( have_rows('make_up') ): ?>
<br>
<?php if( $make_up_photo ): ?>
<b>Mit Make--up:</b></br><a rel="nofollow" href="<?php echo $make_up_photo?>"><img src="<?php echo $make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto von <?php echo $celebname_label; ?> und ihrem <?php echo date("Y"); ?> <?php echo $make_up_style_label; ?> Make-up-Stil, sowie Produkten & Tipps' "/></a></br>
<br>
<?php endif; ?>
<?php if( $no_make_up_photo ): ?>
<b>Ohne Make-up:</b></br><a rel="nofollow" href="<?php echo $no_make_up_photo?>"><img src="<?php echo $no_make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto von <?php echo $descript_label; ?> <?php echo $celebname_label; ?> von <?php echo $place_of_birth ?> ohne Make-up' "/></a></br>
<br>
<?php endif; ?>
<?php echo $celebname_label; ?>'s Make-up & Hautpflegeprodukte, wie Feuchtigkeitscremes <?php if(in_array('male', get_field('sex') )): ?>& Grundierungen.<?php else: ?>, Grundierungen und Lippenstift.<?php endif; ?>
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('make_up') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> benutzt <?php echo $name; ?> als <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> Produkt<?php if( $brand ): ?> nach <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Derzeit keine Make-up & Hautprodukte aufgeführt.
<?php endif; ?>
		</br><br>

<?php if(in_array('male', get_field('sex') )): ?><?php else: ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Augen-Make-up</h2></div>
<br>	
<?php if( have_rows('eye_products') ): ?>
Unsere Auswahl aus <?php echo $celebname_label; ?>'s Lieblings-Wimperntusche, Eyeliner und anderem Augen-Make-up. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('eye_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> benutzt <?php echo $name; ?> als <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> Produkt ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> Produkt<?php if( $brand ): ?> nach <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Zur Zeit sind keine Augen-Make-up & Augen-Produkte aufgeführt.
<?php endif; ?>
		</br>

<br>

<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Haarprodukte</h2></div>
<br>
<br>
<?php if( $hairstyle_photo ): ?><a rel="nofollow" href="<?php echo $hairstyle_photo?>"><img src="<?php echo $hairstyle_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='<?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> <?php echo $hair_label; ?> Haar & <?php echo $hairstyle_label; ?> Frisur. Aktuelle Länge:  <?php echo $hair_length_label; ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('hair_products') ): ?>
<?php echo $celebname_label; ?>'s Wachs, Haarspray & Haarwaschmittel etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('hair_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> benutzt <?php echo $name; ?> als <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> Produkt ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Zur Zeit kein Wachs, Haarspray & Haarwaschmittel etc. aufgeführt.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Düfte & Parfüme</h2></div>
<br>
<?php if( have_rows('fragrances') ): ?>
<?php echo $celebname_label; ?>'s Düfte & Parfüme </br>
<br>
						<ul class="slides">

						<?php while( have_rows('fragrances') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
		
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> benutzt <?php echo $name; ?> als <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> Produkt ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Zur Zeit keine Düfte und Parfüme aufgeführt.
<?php endif; ?>
		</br>
<br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2><?php if(in_array('male', get_field('sex') )): ?>Körperprodukte, Schneideprodukte & Rasierer
<?php else: ?>Körperprodukte und Nagellack<?php endif; ?></h2></div>
<br>
<?php if( have_rows('body_products') ): ?>
<?php echo $celebname_label; ?>'s Nagellack, Rasierer und Körperprodukte</br>
<br>
						<ul class="slides">

						<?php while( have_rows('body_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> benutzt <?php echo $name; ?> als <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> Produkt ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Derzeit keine Körperprodukte wie Rasierer & Schneideprodukte und Nagellack aufgeführt.
<?php endif; ?>
		</br>
<br>
</div>
- <b><a href='http://muzul.com/de/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Kleidungsstil & Tipps</a></br>
- <a href='http://muzul.com/de/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Lifestyle, Freunde und Familie</a></b></br><br>
<i>Zuletzt aktualisiert: <?php the_modified_date(); ?></i>
</br>
<br>
Teile Styling & Beauty-Tipps um auszusehen wie <?php echo $celebname_label; ?> unten:
</br>


<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='Tips: <?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s <?php echo $hairstyle_label; ?> hair style of the <?php echo $descript_label; ?> <?php echo $profession_label; ?>' "/></a>

<b><?php echo $celebname_label; ?> <?php if(in_array('male', get_field('sex') )): ?>son <?php else: ?>son<?php endif; ?> look & beauté se caractérisent par <?php if(in_array('male', get_field('sex') )): ?>sa<?php else: ?>sa<?php endif; ?> <?php echo $hair_length_label; ?> & <?php echo $hairstyle_label; ?> coupe. <?php if(in_array('male', get_field('sex') )): ?> Dames <?php else: ?> Hommes <?php endif; ?> amour <?php if(in_array('male', get_field('sex') )): ?>lui<?php else: ?>sa<?php endif; ?> pour <?php if(in_array('male', get_field('sex') )): ?>sa<?php else: ?>sa<?php endif; ?> <?php echo $distinct_feature_label; ?><?php if(in_array('male', get_field('sex') )): ?>.<?php else: ?>, et <?php echo $make_up_style_label; ?> style de maquillage.<?php endif; ?> <?php echo $celebname_label; ?>, Agé de <?php echo $age ?>-ans <?php echo $nationality_label; ?> <?php echo $profession_label; ?> du <?php echo $place_of_birth ?> est né le <b><?php echo $birth_date ?></b>. Vous devez savoir <?php if(in_array('male', get_field('sex') )): ?>sa <?php else: ?>sa<?php endif; ?> à partir de <?php echo $claim_to_fame ?>.
</br><br>
Envie de ressembler à <?php echo $celebname_label; ?>? </br></b>
<i> Voici ce que vous devez savoir:</i>
<br>
<br>
C’est un <?php echo $descript_label; ?> <?php echo $profession_label; ?> fait des publicités pour <?php echo $endorsements; ?>, mais utilise en fait : <?php echo $brands ?>. </br><br>
<b> Type/forme de visage:</b> <?php echo $face_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "facial_hair", true) ): ?>
<b> Barbe ou moustache:</b> <?php echo $facial_hair_label; ?></br>
<br>
<?php endif; ?>
<b> Particularité:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b> Type de cheveux:</b> <?php echo $hair_type_label; ?></br>
<br>
<b> Couleur des cheveux:</b> <?php echo $hair_label; ?></br>
<br>
<b> Couleur des yeux:</b> <?php echo $eye_label; ?></br>
<br>
<b> Teint de peau:</b> <?php echo $tone_label; ?></br>
<br>
<b> Type de peau:</b> <?php echo $skin_type_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "bra_size", true) ): ?>
<b> Taille soutien-gorge:</b> <?php echo $bra_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Biceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Buste:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b> taille:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Butt:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br><br>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> conseils & produits de beauté préférés </h2>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2> Soin visage & maquillage </h2></div>
<br>
<?php if( have_rows('make_up') ): ?>
<br>
<?php if( $make_up_photo ): ?>
<b>Avec maquillage:</b></br><a rel="nofollow" href="<?php echo $make_up_photo?>"><img src="<?php echo $make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Photo of <?php echo $celebname_label; ?> and her <?php echo date("Y"); ?> <?php echo $make_up_style_label; ?> make up style, products & tips' "/></a></br>
<br>
<?php endif; ?>
<?php if( $no_make_up_photo ): ?>
<b>Sans maquillage:</b></br><a rel="nofollow" href="<?php echo $no_make_up_photo?>"><img src="<?php echo $no_make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Photo of the <?php echo $descript_label; ?> <?php echo $celebname_label; ?> from <?php echo $place_of_birth ?> without makeup' "/></a></br>
<br>
<?php endif; ?>
<?php echo $celebname_label; ?> produits de maquillage et de la peau, comme hydratants <?php if(in_array('male', get_field('sex') )): ?>& fond de teint.<?php else: ?>, fond de teint & rouge à lèvres.<?php endif; ?>
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('make_up') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Actuellement aucun produit de maquillage ou de peau listés.
<?php endif; ?>
		</br><br>

<?php if(in_array('male', get_field('sex') )): ?><?php else: ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2> Maquillage pour les yeux </h2></div>
<br>
<?php if( have_rows('eye_products') ): ?>
Notre sélection de <?php echo $celebname_label; ?> favoris mascara, eye liner & autre maquillage. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('eye_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actuellement aucun maquillage pour les yeux ni produits oculaires listés.
<?php endif; ?>
		</br>

<br>

<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2> Produits capillaires </h2></div>
<br>
<br>
<?php if( $hairstyle_photo ): ?><a rel="nofollow" href="<?php echo $hairstyle_photo?>"><img src="<?php echo $hairstyle_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='<?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> <?php echo $hair_label; ?> hair & <?php echo $hairstyle_label; ?> hair style. Current length:  <?php echo $hair_length_label; ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('hair_products') ): ?>
<?php echo $celebname_label; ?> cire, laque et shampooing etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('hair_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actuellement aucune cire, laque ou shampooing listés.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2> Fragrances et parfums </h2></div>
<br>
<?php if( have_rows('fragrances') ): ?>
<?php echo $celebname_label; ?> fragrances et parfums </br>
<br>
						<ul class="slides">

						<?php while( have_rows('fragrances') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
		
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actuellement aucune fragrance ni parfums listés.
<?php endif; ?>
		</br>
<br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2><?php if(in_array('male', get_field('sex') )): ?> produits corporels, tondeuse & rasoirs
<?php else: ?> produits corporels & vernis à ongles <?php endif; ?></h2></div>
<br>
<?php if( have_rows('body_products') ): ?>
<?php echo $celebname_label; ?> vernis à ongles, rasoirs et produits corporels </br>
<br>
						<ul class="slides">

						<?php while( have_rows('body_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> uses <?php echo $name; ?> as <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Actuellement ne figure aucun produit de corps comme des rasoirs, tondeuses ou vernis à ongles. 
<?php endif; ?>
		</br>
<br>
</div>
- <b><a href='http://muzul.com/fr/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Conseils & style vestimentaire </a></br>
- <a href='http://muzul.com/fr/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Mode de vie, amis & famille </a></b></br><br>
<i> Mise à jour: <?php the_modified_date(); ?></i>
</br>
<br>
Partager un style & conseils de beauté pour ressembler <?php echo $celebname_label; ?> ci-dessous:
</br>

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='Tips: <?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s <?php echo $hairstyle_label; ?> hair style of the <?php echo $descript_label; ?> <?php echo $profession_label; ?>' "/></a>

<b><?php echo $celebname_label; ?> <?php if(in_array('male', get_field('sex') )): ?>su<?php else: ?>su<?php endif; ?> belleza y apariencia son caracterizados por <?php if(in_array('male', get_field('sex') )): ?>su<?php else: ?>su<?php endif; ?> <?php echo $hair_length_label; ?> & peinado <?php echo $hairstyle_label; ?>. <?php if(in_array('male', get_field('sex') )): ?>A las mujeres<?php else: ?>A los hombres<?php endif; ?> les encanta <?php if(in_array('male', get_field('sex') )): ?><?php else: ?><?php endif; ?> por <?php if(in_array('male', get_field('sex') )): ?>su<?php else: ?>su<?php endif; ?> <?php echo $distinct_feature_label; ?><?php if(in_array('male', get_field('sex') )): ?>.<?php else: ?>, y estilo de maquillaje <?php echo $make_up_style_label; ?>.<?php endif; ?> <?php echo $celebname_label; ?>, <?php if(in_array('male', get_field('sex') )): ?>el<?php else: ?>la<?php endif; ?> <?php echo $profession_label; ?> de <?php echo $age ?> años de edad, <?php echo $nationality_label; ?> de <?php echo $place_of_birth ?> nasció en <b><?php echo $birth_date ?></b>. <?php if(in_array('male', get_field('sex') )): ?>Lo<?php else: ?>La<?php endif; ?> puedes conocer de <?php echo $claim_to_fame ?>.
</br><br>
¿Quieres parecerte con <?php echo $celebname_label; ?>? </br></b>
<i>Aquí está lo que necesitas saber:</i>
<br>
<br>
<?php if(in_array('male', get_field('sex') )): ?>Este<?php else: ?>Esta<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?> hace propaganda para <?php echo $endorsements; ?>, pero en realidad usa: <?php echo $brands ?>. </br><br>
<b>Forma/tipo de rostro:</b> <?php echo $face_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "facial_hair", true) ): ?>
<b>Barba o Bigote:</b> <?php echo $facial_hair_label; ?></br>
<br>
<?php endif; ?>
<b>Característica distinta:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Tipo de Cabello:</b> <?php echo $hair_type_label; ?></br>
<br>
<b>Color de Cabello:</b> <?php echo $hair_label; ?></br>
<br>
<b>Color de Ojo:</b> <?php echo $eye_label; ?></br>
<br>
<b>Tez/Complexión de Piel:</b> <?php echo $tone_label; ?></br>
<br>
<b>Tipo de Piel:</b> <?php echo $skin_type_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "bra_size", true) ): ?>
<b>Tamaño de sostén:</b> <?php echo $bra_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Bíceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Busto:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Cintura:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Trasero:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<b>Salud // ¿<?php echo $celebname_label; ?> fuma?</b> <?php if( get_post_meta($post->ID, "smoker", true) ): ?><?php echo $smoker_label; ?><?php else: ?>No hay registros de fumante<?php endif; ?></br><br>
<?php if( $smoking_photo ): ?>
<a rel="nofollow" href="<?php echo $smoking_photo?>"><img src="<?php echo $smoking_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto de <?php echo $celebname_label; ?>, nascido en <?php echo $place_of_birth ?>, signo <?php echo $starsign_label; ?>, fumando en  <?php echo date("Y"); ?> en la edad edad de <?php echo $age ?>' "/></a></br><br><?php endif; ?>
<br>
<h2>Productos y consejos favoritos de <?php echo $celebname_label; ?> para <?php echo date("Y"); ?></h2>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Cuidados de piel y maquillaje </h2></div>
<br>
<?php if( have_rows('make_up') ): ?>
<br>
<?php if( $make_up_photo ): ?>
<b>Con maquillaje:</b></br><a rel="nofollow" href="<?php echo $make_up_photo?>"><img src="<?php echo $make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto de <?php echo $celebname_label; ?> y su estilo de maquillaje <?php echo $make_up_style_label; ?>, productos y consejos para <?php echo date("Y"); ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( $no_make_up_photo ): ?>
<b>Sin maquillaje:</b></br><a rel="nofollow" href="<?php echo $no_make_up_photo?>"><img src="<?php echo $no_make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto <?php if(in_array('male', get_field('sex') )): ?>del<?php else: ?>de la<?php endif; ?> <?php echo $descript_label; ?> <?php echo $celebname_label; ?> de <?php echo $place_of_birth ?> sin maquillaje' "/></a></br>
<br>
<?php endif; ?>
Productos de maquillaje y piel de <?php echo $celebname_label; ?>, como hidratantes <?php if(in_array('male', get_field('sex') )): ?>y bases.<?php else: ?>, bases y lápices labiales.<?php endif; ?>
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('make_up') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Actualmente no hay productos de maquillaje y piel listados.
<?php endif; ?>
		</br><br>

<?php if(in_array('male', get_field('sex') )): ?><?php else: ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Maquillaje de Ojos</h2></div>
<br>
<?php if( have_rows('eye_products') ): ?>
Nuestra selección de los rímeles, delineadores y otros cosméticos para ojos favoritos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('eye_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actualmente no hay productos de maquillaje y ojos listados.
<?php endif; ?>
		</br>

<br>

<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Productos de cabello</h2></div>
<br>
<br>
<?php if( $hairstyle_photo ): ?><a rel="nofollow" href="<?php echo $hairstyle_photo?>"><img src="<?php echo $hairstyle_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Cabello <?php echo $hair_label; ?> y peinado <?php echo $hairstyle_label; ?> de <?php echo $celebname_label; ?> en <?php echo date("Y"); ?>. Largo actual:  <?php echo $hair_length_label; ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('hair_products') ): ?>
Ceras, lacas, shampoos, etc. de<?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('hair_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actualmente no hay ceras, lacas, shampoos, etc. listados
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Fragancias y perfumes</h2></div>
<br>
<?php if( have_rows('fragrances') ): ?>
Fragancias y perfumes de <?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('fragrances') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
		
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?>por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actualmente no hay fragancias y perfumes listados.
<?php endif; ?>
		</br>
<br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2><?php if(in_array('male', get_field('sex') )): ?>Productos corporales, cortapelos y afeitadores
<?php else: ?>Productos corporales y Pintura de Uñas <?php endif; ?></h2></div>
<br>
<?php if( have_rows('body_products') ): ?>
Pinturas de uña, afeitadores y productos corporales de <?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('body_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Actualmente no hay productos corporales como afeitadores y cortapelos y pinturas de uñas listados.
<?php endif; ?>
		</br>
<br>
</div>
- <b><a href='http://muzul.com/es/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Estilo de ropa y consejos </a></br>
- <a href='http://muzul.com/es/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Estilo de vida, amigos y família </a></b></br><br>
<i>Última actualización: <?php the_modified_date(); ?></i>
</br>
<br>
Comparte los consejos de estilo y belleza para parecerse con <?php echo $celebname_label; ?> abajo:
</br>

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='right' hspace="10" alt='Tips: <?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s <?php echo $hairstyle_label; ?> hair style of the <?php echo $descript_label; ?> <?php echo $profession_label; ?>' "/></a>

<b><?php echo $celebname_label; ?>, <?php if(in_array('male', get_field('sex') )): ?>sua<?php else: ?>sua<?php endif; ?> beleza e aparência são caracterizados por seu <?php echo $hair_length_label; ?> e penteado <?php echo $hairstyle_label; ?>. <?php if(in_array('male', get_field('sex') )): ?>As mulheres o adoram<?php else: ?>Os homens a adoram<?php endif; ?> por seu(sua) <?php echo $distinct_feature_label; ?><?php if(in_array('male', get_field('sex') )): ?>.<?php else: ?>, E estilo de maquiagem <?php echo $make_up_style_label; ?>.<?php endif; ?> <?php echo $celebname_label; ?>, <?php if(in_array('male', get_field('sex') )): ?>o<?php else: ?>a<?php endif; ?> <?php echo $profession_label; ?> de <?php echo $age ?> anos de idade, <?php echo $nationality_label; ?> de <?php echo $place_of_birth ?> nasceu em <b><?php echo $birth_date ?></b>. Talvez você <?php if(in_array('male', get_field('sex') )): ?> o <?php else: ?> a <?php endif; ?> conheça de <?php echo $claim_to_fame ?>.
</br><br>
Quer se parecer com <?php echo $celebname_label; ?>? </br></b>
<i>Aqui está o que você precisa saber:</i>
<br>
<br>
<?php if(in_array('male', get_field('sex') )): ?>Este<?php else: ?>Esta<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?> faz propaganda para <?php echo $endorsements; ?>, mas na verdade usa: <?php echo $brands ?>. </br><br>
<b>Forma/tipo de rosto:</b> <?php echo $face_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "facial_hair", true) ): ?>
<b>Barba ou Bigode:</b> <?php echo $facial_hair_label; ?></br>
<br>
<?php endif; ?>
<b>Característica distinta:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Tipo de Cabelo:</b> <?php echo $hair_type_label; ?></br>
<br>
<b>Cor de Cabelo:</b> <?php echo $hair_label; ?></br>
<br>
<b>Cor dos Olhos:</b> <?php echo $eye_label; ?></br>
<br>
<b>Tez/Cor de Pele:</b> <?php echo $tone_label; ?></br>
<br>
<b>Tipo de Pele:</b> <?php echo $skin_type_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "bra_size", true) ): ?>
<b>Tamanho de sutiã:</b> <?php echo $bra_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Bíceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Busto:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Cintura:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Bunda:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<b>Saúde // <?php echo $celebname_label; ?> fuma?</b> <?php if( get_post_meta($post->ID, "smoker", true) ): ?><?php echo $smoker_label; ?><?php else: ?>Não há registros de fumante<?php endif; ?></br><br>
<?php if( $smoking_photo ): ?>
<a rel="nofollow" href="<?php echo $smoking_photo?>"><img src="<?php echo $smoking_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto de <?php echo $celebname_label; ?>, nascido em <?php echo $place_of_birth ?>, signo <?php echo $starsign_label; ?>, fumando em  <?php echo date("Y"); ?>na idade de <?php echo $age ?>' "/></a></br><br><?php endif; ?>
<h2>Produtos e dicas  favoritas de <?php echo $celebname_label; ?> para <?php echo date("Y"); ?></h2>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Cuidados da pele e maquiagem </h2></div>
<br>
<?php if( have_rows('make_up') ): ?>
<br>
<?php if( $make_up_photo ): ?>
<b>Com maquiagem:</b></br><a rel="nofollow" href="<?php echo $make_up_photo?>"><img src="<?php echo $make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto de <?php echo $celebname_label; ?> e seu estilo de maquiagem <?php echo $make_up_style_label; ?>, produtos e dicas para <?php echo date("Y"); ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( $no_make_up_photo ): ?>
<b>Sem maquiagem:</b></br><a rel="nofollow" href="<?php echo $no_make_up_photo?>"><img src="<?php echo $no_make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto <?php if(in_array('male', get_field('sex') )): ?>do<?php else: ?>da<?php endif; ?> <?php echo $descript_label; ?> <?php echo $celebname_label; ?> de <?php echo $place_of_birth ?> sem maquiagem' "/></a></br>
<br>
<?php endif; ?>
Produtos de maquiagem e pele de <?php echo $celebname_label; ?>, como hidratantes <?php if(in_array('male', get_field('sex') )): ?>e bases.<?php else: ?>, bases e batons.<?php endif; ?>
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('make_up') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Atualmente não há produtos de maquiagem e pele listados.
<?php endif; ?>
		</br><br>

<?php if(in_array('male', get_field('sex') )): ?><?php else: ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Maquiagem de Olhos</h2></div>
<br>
<?php if( have_rows('eye_products') ): ?>
Nossa seleção dos rímeis, delineadores e outros cosmétivos para olhos favoritos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('eye_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Atualmente não há produtos de maquiagem e olhos listados.
<?php endif; ?>
		</br>

<br>

<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Produtos de cabelo</h2></div>
<br>
<br>
<?php if( $hairstyle_photo ): ?><a rel="nofollow" href="<?php echo $hairstyle_photo?>"><img src="<?php echo $hairstyle_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Cabelo <?php echo $hair_label; ?> e penteado <?php echo $hairstyle_label; ?> de <?php echo $celebname_label; ?> em <?php echo date("Y"); ?>. Comprimento atual:  <?php echo $hair_length_label; ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('hair_products') ): ?>
Ceras, sprays de cabelo, shampoos, etc. de<?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('hair_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Atualmente não há ceras, sprays de cabelo, shampoos, etc. listados
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Fragrâncias e perfumes</h2></div>
<br>
<?php if( have_rows('fragrances') ): ?>
Fragrâncias e perfumes de <?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('fragrances') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
		
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Atualmente não há fragrâncias e perfumes listados.
<?php endif; ?>
		</br>
<br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2><?php if(in_array('male', get_field('sex') )): ?>Produtos corporais, aparadores e barbeadores
<?php else: ?>Produtos corporais e Pintura de Unhas <?php endif; ?></h2></div>
<br>
<?php if( have_rows('body_products') ): ?>
Pinturas de unha, barbeadores e produtos corporais de <?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('body_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> usa <?php echo $name; ?> como <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> ' "/></a>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Atualmente não há produtos corporais como barbeadores e aparadores e pinturas de unhas listados.
<?php endif; ?>
		</br>
<br>
</div>
- <b><a href='http://muzul.com/pt-br/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Estilo de roupa e dicas</a></br>
- <a href='http://muzul.com/pt-br/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Estilo de vida, amigos e família </a></b></br><br>
<i>Última atualização: <?php the_modified_date(); ?></i>
</br>
<br>
Compartilhe as dicas de estilo e beleza para se parecer com <?php echo $celebname_label; ?> abaixo:
</br>

<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='right' hspace="10" alt='Tips: <?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s <?php echo $hairstyle_label; ?> hair style of the <?php echo $descript_label; ?> <?php echo $profession_label; ?>' "/></a>
<b>Bellezza & look di <?php echo $celebname_label; ?> sono caratterizzati da un taglio <?php echo $hair_length_label; ?> & <?php echo $hairstyle_label; ?>. <?php if(in_array('male', get_field('sex') )): ?>Le donne<?php else: ?>Gli uomini<?php endif; ?> l’adorano <?php if(in_array('male', get_field('sex') )): ?> <?php else: ?><?php endif; ?> per <?php echo $distinct_feature_label; ?><?php if(in_array('male', get_field('sex') )): ?>.<?php else: ?>, e <?php echo $make_up_style_label; ?> il modo di truccarsi.<?php endif; ?> <?php echo $celebname_label; ?> ha <?php echo $age ?> anni ed è <?php echo $profession_label; ?> <?php echo $nationality_label; ?>, viene da <?php echo $place_of_birth ?> e la sua data di nascità è il <b><?php echo $birth_date ?></b>. Potresti conoscere <?php if(in_array('male', get_field('sex') )): ?>il suo <?php else: ?>il suo<?php endif; ?><?php echo $claim_to_fame ?>.
</br><br>
Per apparire come <?php echo $celebname_label; ?>? </br></b>
<i>Qui c’è tutto quello che devi sapere:</i>
<br>
<br>
Questo <?php echo $descript_label; ?> <?php echo $profession_label; ?> commercializza <?php echo $endorsements; ?>, ma attualmente utilizza: <?php echo $brands ?>. </br><br>
<b>Forma del viso:</b> <?php echo $face_label; ?></br>
<br>
<?php if( get_post_meta($post->ID, "facial_hair", true) ): ?>
<b>Barba o Baffi:</b> <?php echo $facial_hair_label; ?></br>
<br>
<?php endif; ?>
<b>Segni caratteristici:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Tipo di capelli:</b> <?php echo $hair_type_label; ?></br>
<br>
<b>Colore dei capelli:</b> <?php echo $hair_label; ?></br>
<br>
<b>Colore degli occhi:</b> <?php echo $eye_label; ?></br>
<br>
<b>Carnagione:</b> <?php echo $tone_label; ?></br>
<br>
<b>Tipo di pelle:</b> <?php echo $skin_type_label; ?></br>
<br>
<b>Salute // <?php echo $celebname_label; ?> fuma?</b> <?php if( get_post_meta($post->ID, "smoker", true) ): ?><?php echo $smoker_label; ?><?php else: ?>Non c’è nulla sul fumo<?php endif; ?></br><br>
<?php if( $smoking_photo ): ?>
<a rel="nofollow" href="<?php echo $smoking_photo?>"><img src="<?php echo $smoking_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto a <?php echo $place_of_birth ?> luogo di nascita <?php echo $starsign_label; ?>, <?php echo $celebname_label; ?> sta fumando nel <?php echo date("Y"); ?> all’età di <?php echo $age ?>' "/></a></br><br><?php endif; ?>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> consigli & prodotti di bellezza preferiti</h2>
<br></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Cura della pelle & trucco </h2></div>
<br>
<?php if( have_rows('make_up') ): ?>
<br>
<?php if( $make_up_photo ): ?>
<b>Con trucco:</b></br><a rel="nofollow" href="<?php echo $make_up_photo?>"><img src="<?php echo $make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto di <?php echo $celebname_label; ?> ed il suo <?php echo date("Y"); ?> <?php echo $make_up_style_label; ?> stile di make up, prodotti & consigli' "/></a></br>
<br>
<?php endif; ?>
<?php if( $no_make_up_photo ): ?>
<b>Senza trucco:</b></br><a rel="nofollow" href="<?php echo $no_make_up_photo?>"><img src="<?php echo $no_make_up_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='Foto di <?php echo $descript_label; ?> <?php echo $celebname_label; ?> di <?php echo $place_of_birth ?> senza trucco' "/></a></br>
<br>
<?php endif; ?>
Prodotti per il trucco & la cura della pelle di <?php echo $celebname_label; ?>, come basi idratanti <?php if(in_array('male', get_field('sex') )): ?>& fondotinta.<?php else: ?>, fondotinta & rossetto.<?php endif; ?>
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('make_up') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> utilizza <?php echo $name; ?> come <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> prodotto ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> di <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrizione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Prezzo: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Attualmente non sono presenti trucchi & prodotti per la cura della pelle.
<?php endif; ?>
		</br><br>

<?php if(in_array('male', get_field('sex') )): ?><?php else: ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Trucco per occhi</h2></div>
<br>
<?php if( have_rows('eye_products') ): ?>
La nostra selezione dei mascara, eye liner & altri trucchi per occhi preferiti da <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('eye_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> utilizza <?php echo $name; ?> come <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> prodotto ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> di <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrizione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Prezzo: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Attualmente non sono presenti Trucchi per occhi & prodotti per occhi.
<?php endif; ?>
		</br>

<br>

<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Prodotti per capelli</h2></div>
<br>
<br>
<?php if( $hairstyle_photo ): ?><a rel="nofollow" href="<?php echo $hairstyle_photo?>"><img src="<?php echo $hairstyle_photo ?>" rel='nofollow' width="150" height="" border="0" hspace="10" alt='<?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> <?php echo $hair_label; ?> capelli & <?php echo $hairstyle_label; ?> stile dei capelli. Lunghezza attuale:  <?php echo $hair_length_label; ?>' "/></a></br>
<br>
<?php endif; ?>
<?php if( have_rows('hair_products') ): ?>
Cere, lacche per capelli, shampoo e simili di <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('hair_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> utilizza <?php echo $name; ?> come <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> prodotto ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> di <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrizione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Prezzo: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Attualmente non sono presenti cere, lacche per capelli, shampoo e simili.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2>Fragranze & profumi</h2></div>
<br>
<?php if( have_rows('fragrances') ): ?>
Fragranze & profumi di <?php echo $celebname_label; ?> </br>
<br>
						<ul class="slides">

						<?php while( have_rows('fragrances') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
		
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> utilizza <?php echo $name; ?> come <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> prodotto ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> di <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrizione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Prezzo: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Attualmente non sono presenti fragranze & profumi.
<?php endif; ?>
		</br>
<br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h2><?php if(in_array('male', get_field('sex') )): ?>Prodotti per il corpo, rasoi & lamette
<?php else: ?>Prodotti per il corpo & Smalti per unghie<?php endif; ?></h2></div>
<br>
<?php if( have_rows('body_products') ): ?>
Smalto per unghie, rasoi & prodotti per il corpo di <?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('body_products') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
       $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div><div style='padding: 10px;'>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> utilizza <?php echo $name; ?> come <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> prodotto ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> di <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrizione: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>  • Prezzo: $<?php echo $price; ?> </div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Attualmente non sono presenti prodotti per il corpo come rasoi & lamette & smalti per unghie.
<?php endif; ?>
		</br>
<br>
</div>
- <b><a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Stile di abbigliamento & Consigli</a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Stile di vita, Amici & Famiglia</a></b></br><br>
<i>Ultimo aggiornamento: <?php the_modified_date(); ?></i>
</br>
<br>
Condividi i consigli di stile & bellezza per essere come <?php echo $celebname_label; ?>, qui sotto:


<?php endif; ?>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>

													<?php the_content(); ?>
													<?php wp_link_pages(); ?>
													<div class="posts-nav-link">
														<?php posts_nav_link(); ?>
													</div><!--posts-nav-link--></center>
													
													<?php if(get_option('mvp_article_ad')) { ?>
														<div id="article-ad">
															<?php $articlead = get_option('mvp_article_ad'); if ($articlead) { echo html_entity_decode($articlead); } ?>
														</div><!--article-ad-->
													<?php } ?>
													
												</div><!--content-main-->
											</div><!--content-area-in-->
											<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "full" || $mvp_post_temp == "wide-full" ) { } else { ?>
												<?php get_sidebar(); ?>
											<?php } ?>
										</div><!--content-area-out-->
												</div><!--sec-marg-in-->
											</div><!--sec-marg-out-->
										</div><!--content-area-cont-->
									</div><!--content-area-->
								</div><!--post-body-in-->
							</div><!--post-body-out-->
						</div><!--post-area-->
					</article>
	</div><!--sec-marg-in4-->
</div><!--sec-marg-out4-->
				</div><!--content-wrap-->
				<?php $mvp_show_more = get_option('mvp_show_more'); if ($mvp_show_more == "true") { ?>
				<div class="content-bot-wrap left relative">
					<div class="sec-marg-out relative">
						<div class="sec-marg-in">
					<h4 class="post-header">
						<span class="post-header"><?php _e( 'More in', 'mvp-text' ); ?> <?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
					</h4>
				<div class="archive-list-wrap left relative">
					<div class="archive-list-out">
						<div class="archive-list-in">
							<div class="archive-list-left left relative">
								<ul class="archive-list">
									<?php $mvp_more_num = esc_html(get_option('mvp_more_num')); $category = get_the_category(); $current_cat = $category[0]->cat_ID; $recent = new WP_Query(array( 'cat' => $current_cat, 'posts_per_page' => $mvp_more_num, 'post__not_in' => array( $post->ID ) )); while($recent->have_posts()) : $recent->the_post(); ?>
									<li>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-3"></i>
													</div><!--feat-video-->
												<?php endif; ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-3"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; wp_reset_query(); ?>
								</ul>
							</div><!--archive-list-left-->
						</div><!--archive-list-in-->
						<?php $more_ad = get_option('mvp_more_ad'); if ($more_ad) { ?>
							<div class="archive-ad-wrap relative">
								<?php $more_ad = get_option('mvp_more_ad'); if ($more_ad) { echo html_entity_decode($more_ad); } ?>
							</div><!--archive-ad-wrap-->
						<?php } ?>
					</div><!--archive-list-out-->
				</div><!--archive-list-wrap-->
					</div><!--sec-marg-in-->
				</div><!--sec-marg-out-->
			</div><!--content-bot-wrap-->
		<?php } ?>
		<?php setCrunchifyPostViews(get_the_ID()); ?>
	<?php endwhile; endif; ?>
</div><!--main-content-contain-->
<?php get_footer(); ?>