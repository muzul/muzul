<?php

// disables post metabox when editing posts.
add_filter('postmeta_form_keys', function($keys, $post){
	if($post->post_type == 'post'){
		return [];
	}
}, 10, 2);
?>
