<?php 
/**
 * The Template for displaying all single blog posts
 *
 * @package SimpleMag
 * @since 	SimpleMag 1.0
**/
get_header(); 
global $ti_option;
$single_sidebar = get_post_meta( $post->ID, 'post_sidebar', true );
?>

<?php

						$photo = get_field('photo');
						$born_on = get_field('born_on');
						$birthplace = get_field('birthplace');
						$claim_to_fame = get_field('claim_to_fame');
						$nickname= get_field('nickname');
						$text = get_field('text');
						$place_of_birth = get_field('place_of_birth');
						$birth_date = get_field('birth_date');

                                                $celebname_field = get_field_object('celebname');
						$celebname_value = get_field('celebname');
						$celebname_label = $celebname_field['choices'][ $celebname_value ];

						// fill 'profession' variable
						//
						$profession_field = get_field_object('profession');
						$profession_value = get_field('profession');
						$profession_label = '';

						if(count($profession_value) < 2) {
						$profession_label = $profession_field['choices'][ $profession_value[0] ];
						} else {
						foreach($profession_value as $val) {
						$profession_label .= $profession_field['choices'][ $val ] . ' & ';
						}
						}

// fill 'colors' variable
						//
						$colors_field = get_field_object('colors');
						$colors_value = get_field('colors');
						$colors_label = '';

						if(count($colors_value) < 2) {
						$colors_label = $colors_field['choices'][ $colors_value[0] ];
						} else {
						foreach($colors_value as $val) {
						$colors_label .= $colors_field['choices'][ $val ] . ' ';
						}
						}

// fill 'style' variable
						//
						$style_field = get_field_object('style');
						$style_value = get_field('style');
						$style_label = '';

						if(count($style_value) < 2) {
						$style_label = $style_field['choices'][ $style_value[0] ];
						} else {
						foreach($style_value as $val) {
						$style_label .= $style_field['choices'][ $val ] . ' ';
						}
						}


// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

						// fill 'hairstyle' variable
						//
						$hairstyle_field = get_field_object('hairstyle');
						$hairstyle_value = get_field('hairstyle');
						$hairstyle_label = '';

						if(count($hairstyle_value) < 2) {
						$hairstyle_label = $hairstyle_field['choices'][ $hairstyle_value[0] ];
						} else {
						foreach($hairstyle_value as $val) {
						$hairstyle_label .= $hairstyle_field['choices'][ $val ] . ', ';
						}
						}

// fill 'make-up_style' variable
						

	     // fill 'hair_type' variable
						//
						$hair_type_field = get_field_object('hair_type');
						$hair_type_value = get_field('hair_type');
						$hair_type_label = $hair_type_field['choices'][ $hair_type_value[0] ];

						
						//
						// get age from birth date
						//						     
						$date = new DateTime($birth_date);
						$now = new DateTime();
						$interval = $now->diff($date);
						$age = $interval->y;
						
						//
						// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

// fill 'nationality' variable
						//
						$nationality_field = get_field_object('nationality');
						$nationality_value = get_field('nationality');
						$nationality_label = '';

						if(count($nationality_value) < 2) {
						$nationality_label = $nationality_field['choices'][ $nationality_value[0] ];
						} else {
						foreach($nationality_value as $val) {
						$nationality_label .= $nationality_field['choices'][ $val ] . '- ';
						}
						}

						//
						// fill 'descript' variable
						//
						$descript_field = get_field_object('descript');
						$descript_value = get_field('descript');
						$descript_label = '';

						if(count($descript_value) < 2) {
						$descript_label = $descript_field['choices'][ $descript_value[0] ];
						} else {
						foreach($descript_value as $val) {
						$descript_label .= $descript_field['choices'][ $val ] . ' ';;
						}
						}

						// fill 'face' variable
						//
						$face_field = get_field_object('face');
						$face_value = get_field('face');
						$face_label = $face_field['choices'][ $face_value[0] ];

						//
						// fill 'eye' variable
						//
						$eye_field = get_field_object('eye');
						$eye_value = get_field('eye');
						$eye_label = $eye_field['choices'][ $eye_value[0] ];
						
						//
						// fill 'hair' variable
						//
						$hair_field = get_field_object('hair');
						$hair_value = get_field('hair');
						$hair_label = $hair_field['choices'][ $hair_value[0] ];

//
						// fill 'tone' variable
						//
						$tone_field = get_field_object('tone');
						$tone_value = get_field('tone');
						$tone_label = $tone_field['choices'][ $tone_value[0] ];


						// fill 'body' variable
						//
						$body_field = get_field_object('body');
						$body_value = get_field('body');
						$body_label = $body_field['choices'][ $body_value[0] ];

			// fill 'skin_type' variable
						//
						$skin_type_field = get_field_object('skin_type');
						$skin_type_value = get_field('skin_type');
						$skin_type_label = $skin_type_field['choices'][ $skin_type_value[0] ];

// fill 'love' variable
						//
						$love_field = get_field_object('love');
						$love_value = get_field('love_type');
						$love_label = $love_field['choices'][ $love_value[0] ];

// fill 'distinct_feature' variable
						//
						$distinct_feature_field = get_field_object('distinct_feature');
						$distinct_feature_value = get_field('distinct_feature');
						$distinct_feature_label = '';

						if(count($distinct_feature_value) < 2) {
						$distinct_feature_label = $distinct_feature_field['choices'][ $distinct_feature_value[0] ];
						} else {
						foreach($distinct_feature_value as $val) {
						$distinct_feature_label .= $distinct_feature_field['choices'][ $val ] . '- ';
						}
						}





      $height = get_field('height');
						$weight = get_field('weight');
      $endorsements= get_field('endorsements');
						$brands= get_field('brands');
						$hairstyle_photo= get_field('hairstyle_photo');
						$make_up_photo= get_field('make_up_photo');
						$no_make_up_photo= get_field('no_make_up_photo');
						$net_worth= get_field('net_worth');
						$salary= get_field('salary');
						$colleagues= get_field('colleagues');
						$mother= get_field('management');
						$family_photo= get_field('family_photo');
						$current_partner_text= get_field('current_partner_text');
      $style_photo= get_field('style_photo');
						$dress_size= get_field('dress_size');
						$shoe_size= get_field('shoe_size');
						$bra= get_field('bra');
						$biceps= get_field('biceps');
						$bust= get_field('bust');
						$waist= get_field('waist');
						$butt= get_field('butt');
						$beach= get_field('beach');
						$house= get_field('house');
						$smoking_caught= get_field('smoking_caught');
						$no_makeup= get_field('no_makeup');
						$since= get_field('since');
						
						//
						// get zodiac sign from birth date
						//	
						function zodiac($birthdate) {
						
							$zodiac = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiac = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiac = "Taurus"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiac = "Gemini"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiac = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiac = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiac = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiac = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiac = "Scorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiac = "Sagittarius"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiac = "Capricorn"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiac = "Aquarius"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiac = "Pisces"; } 
						     
							return $zodiac; 

						}
						
						$starsign_label = zodiac($birth_date, $sex);

// get zodiac sign from birth date
						//	
						function zodiacde($birthdate) {
						
							$zodiacde = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacde = "Widder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacde = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacde = "Zwillinge"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacde = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacde = "Löwe"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacde = "Jungfrau"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacde = "Waage"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacde = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacde = "Schütze"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacde = "Steinbock"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacde = "Wassermann"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacde = "Fische"; } 
						     
							return $zodiacde;

						}
						
						$starsignde_label = zodiacde($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacfr($birthdate) {
						
							$zodiacfr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacfr = "Bélier"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacfr = "Taureau"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacfr = "Gémaux"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacfr = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacfr = "Lion"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacfr = "Vierge"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacfr = "Balance"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacfr = "Scorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacfr = "Sagittaire"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacfr = "Capricorne"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacfr = "Verseau"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacfr = "Poisons"; } 
						     
							return $zodiacfr;

						}

						
						$starsignfr_label = zodiacfr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacbr($birthdate) {
						
							$zodiacbr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacbr = "Carneiro"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacbr = "Touro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacbr = "Gémeos"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacbr = "Caranguejo"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacbr = "Leão"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacbr = "Virgem"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacbr = "Balança"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacbr = "Escorpião"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacbr = "Sagitário"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacbr = "Capricórnio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacbr = "Aquário"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacbr = "Peixes"; } 
						     
							return $zodiacbr;

						}

						$starsignbr_label = zodiacbr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiaces($birthdate) {
						
							$zodiaces = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiaces = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiaces = "Tauro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiaces = "Geminis"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiaces = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiaces = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiaces = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiaces = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiaces = "Escorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiaces = "Sagitario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiaces = "Capricornio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiaces = "Acuario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiaces = "Piscis"; } 
						     
							return $zodiaces;

}
						$starsignes_label = zodiaces($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacpl($birthdate, $sex) {
						
							$zodiacpl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacpl = "Baran"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacpl = "Byk"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacpl = "Bliźnięta"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacpl = "Rak"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacpl = "Lew"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacpl = "Panna"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacpl = "Waga"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacpl = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacpl = "Strzelec"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacpl = "Koziorożec"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacpl = "Wodnik"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacpl = "Ryby"; } 
						     

						     
							return $zodiacpl;

}
			
						$starsignpl_label = zodiacpl($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacda($birthdate, $sex) {
						
							$zodiacda = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacda = "Vædder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacda = "Tyr"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacda = "Tvilling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacda = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacda = "Løve"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacda = "Jomfru"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacda = "Vægt"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacda = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacda = "Skytte"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacda = "Stenbuk"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacda = "Vandmand"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacda = "Fisk"; } 

						     

						     
							return $zodiacda;

}
			
						$starsignda_label = zodiacda($born_on, $sex);


// get zodiac sign from birth date
						//	
						function zodiacsv($birthdate, $sex) {
						
							$zodiacsv = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacsv = " Väduren "; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacsv = "Oxen"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacsv = " Tvillingarna "; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacsv = "Kräftan"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacsv = "Lejonet"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacsv = " Jungfrun "; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacsv = " Vågen "; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacsv = " Skorpionen "; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacsv = " Skytten "; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacsv = " Stenbocken "; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacsv = " Vattumannen "; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacsv = " Fiskarna "; } 
 

						     

						     
							return $zodiacsv;

}
			
						$starsignsv_label = zodiacsv($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacnb($birthdate, $sex) {
						
							$zodiacnb = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnb = "Væren"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnb = "Tyren"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnb = "Tvillingene"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnb = "Krepsen"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnb = "Løven"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnb = "Jomfruen"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnb = "Vekten"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnb = "Skorpionen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnb = "Skytten"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnb = "Stenbukken"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnb = "Vannmannen"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnb = "Fiskene"; } 

						     

						     
							return $zodiacnb;

}
			
						$starsignnb_label = zodiacnb($born_on, $sex);



// get zodiac sign from birth date
						//	
						function zodiacin($birthdate, $sex) {
						
							$zodiacin = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacin = "मेष"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacin = "वृषभ"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacin = "मिथुन"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacin = "कर्क"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacin = "सिंह"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacin = "कन्या"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacin = "तुला"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacin = "वृश्चिक"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacin = "धनु"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacin = "मकर"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacin = "कुंभ"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacin = "मीन"; } 
						     
							return $zodiacin;

}
			
						$starsignin_label = zodiacin($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiactr($birthdate, $sex) {
						
							$zodiactr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiactr = "Koç"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiactr = "Boğa"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiactr = "İkizler"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiactr = "Yengeç"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiactr = "Aslan"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiactr = "Başak"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiactr = "Terazi"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiactr = "Akrep"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiactr = "Yay"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiactr = "Oğlak"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiactr = "Kova"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiactr = "Balık"; } 
						     
							return $zodiactr;

}

						$starsigntr_label = zodiactr($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacnl($birthdate, $sex) {
						
							$zodiacnl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnl = "Ram"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnl = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnl = "Tweeling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnl = "Kreeft"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnl = "Leeuw"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnl = "Maagd"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnl = "Weegschaal"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnl = "Schorpioen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnl = "Boogschutter"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnl = "Steenbok"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnl = "Waterman"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnl = "Vissen"; } 
						     
							return $zodiacnl;

}

						$starsignnl_label = zodiacnl($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacit($birthdate, $sex) {
						
							$zodiacit = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacit = "Ariete"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacit = "Toro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacit = "Gemelli"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacit = "Cancro"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacit = "Leone"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacit = "Vergine"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacit = "Bilancia"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacit = "Scorpione"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacit = "Sagittario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacit = "Capricorno"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacit = "Aquario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacit = "Pesci"; } 
						     
							return $zodiacit;

}

						$starsignit_label = zodiacit($born_on, $sex);
						

						//$starsign_field = get_field_object('starsign');
						//$starsign_value = get_field('starsign');
						//$starsign_label = $starsign_field['choices'][ $starsign_value[0] ];	


						?>

    <main id="content" class="clearfix animated" role="main" itemprop="mainContentOfPage" itemscope itemtype="http://schema.org/Blog">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">

<center>

            <header class="wrapper entry-header page-header">
                
                    <div style='text-align: center;'><h1><?php echo $celebname_label; ?></h1></div>
            </header>

            <?php
            // Output media only on first page if the post have pagination
            if ( $paged == 1 || $page == 1 ) {
                // Output media from every post by Full Width option
                if ( $ti_option['single_media_position'] == 'useperpost' && get_post_meta( $post->ID, 'post_media_position', true ) == 'media_full_width' || $ti_option['single_media_position'] == 'fullwidth' ){
                ?>
                <div class="entry-media">
                    <?php
                    if ( ! get_post_format() ): // Standard
                        get_template_part( 'formats/format', 'standard' );
                    elseif ( 'gallery' == get_post_format() ): // Gallery
                        get_template_part( 'formats/format', 'gallery' );
                    elseif ( 'video' == get_post_format() ): // Video
                        get_template_part( 'formats/format', 'video' );
                    elseif ( 'audio' == get_post_format() ): // Audio
                        get_template_part( 'formats/format', 'audio' );
                    endif;
                    ?>
                </div>
                <?php } else { ?>
                    <?php if ( 'gallery' == get_post_format() ) { ?>
                    <div class="entry-media">
                        <?php get_template_part( 'formats/format', 'gallery' ); ?>
                    </div>
                <?php } ?>
            <?php } } ?>

            <div class="wrapper">
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>

                    <?php
                    // Output media only on first page if the post have pagination
                    if ( $paged == 1 || $page == 1 ) {
                        // Output media from every post by Above The Content option
                        if ( $ti_option['single_media_position'] == 'useperpost' && get_post_meta( $post->ID, 'post_media_position', true ) == 'media_above_content' || $ti_option['single_media_position'] == 'abovecontent' ) {
                        ?>
                        <div class="entry-media">
                            <?php 
                            if ( ! get_post_format() ): // Standard
                                get_template_part( 'formats/format', 'standard' );
                            elseif ( 'video' == get_post_format() ): // Video
                                get_template_part( 'formats/format', 'video' );
                            elseif ( 'audio' == get_post_format() ): // Audio
                                get_template_part( 'formats/format', 'audio' );
                            endif;
                            ?>
                        </div>
                    <?php } } ?>

                    <?php
                    // Post Rating
                    if ( $ti_option['single_rating_box'] == 'rating_top' ) {
                        get_template_part( 'inc/single', 'rating' );
                    }
                    ?>

                    <?php 
                    // Ad Unit
                    if ( $ti_option['single_image_top_ad']['url'] == true || $ti_option['single_code_top_ad'] == true ) { ?>
                    <div class="advertisement">
                        <?php
                        $single_banner_top = $ti_option['single_image_top_ad'];
                        // Image Ad
                        if ( $single_banner_top['url'] == true ) { ?>
                            <a href="<?php echo $ti_option['single_image_top_ad_url']; ?>" rel="nofollow" target="_blank">
                                <img src="<?php echo $single_banner_top['url']; ?>" width="<?php echo $single_banner_top['width']; ?>" height="<?php echo $single_banner_top['height']; ?>" alt="<?php _e( 'Advertisement', 'themetext' ); ?>" />
                            </a>
                        <?php 
                        // Code Ad
                        } elseif( $ti_option['single_code_top_ad'] == true ) {
                            echo $ti_option['single_code_top_ad'];
                        } ?>
                    </div><!-- .advertisment -->
                    <?php } ?>

                    <div class="single-box clearfix entry-content" itemprop="articleBody">
                        			


<?php if(ICL_LANGUAGE_CODE=='en'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s clothing style & tips of the <?php echo $descript_label; ?> <?php echo $profession_label; ?> & <?php echo $starsign_label; ?>' "/></a>

<?php echo $celebname_label; ?>'s clothing style is <?php echo $style_label; ?>, judging by <?php if(in_array('male', get_field('sex') )): ?>his<?php else: ?>her<?php endif; ?> <?php echo $distinct_feature_label; ?>, and taste for <?php echo $colors_label; ?> colors. The  <?php echo $age ?>-year old <?php echo $profession_label; ?> famous for <?php echo $claim_to_fame ?>, is born in <?php echo $place_of_birth ?>, on <?php echo $birth_date ?>. 
</br><br>
Want <?php echo $celebname_label; ?>'s clothing style? </br></b>
<i>Here is what you need to know:</i>
<hr>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
</br>
<a rel="nofollow" href="<?php echo $style_photo?>"><img src="<?php echo $style_photo ?>" rel='nofollow' width="300" height="" border="0" hspace="40" alt='<?php echo $celebname_label; ?> in favorite outfit' "/></a></br>
This <?php echo $descript_label; ?> <?php echo $profession_label; ?><?php if( $endorsements ): ?> endorses brands like <?php echo $endorsements; ?>, and<?php endif; ?> wears: <?php echo $brands ?>. </br><br>
<b>Body Type:</b> <?php echo $body_label; ?></br>
<br>
<b>Typical outfit:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Height:</b> <?php echo $height ?> cm</br>
<br>
<b>Weight:</b> <?php echo $weight ?> kilo</br>
<br>
<?php if( get_post_meta($post->ID, "shoe_size", true) ): ?>
<b>Shoe size:</b> <?php echo $shoe_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "dress_size", true) ): ?>
<b>Dress size:</b> <?php echo $dress_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "bra", true) ): ?>
<b>Bra-size:</b> <?php echo $bra ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Biceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Buste:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Waist:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Butt:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> iconic clothing items & tips</h2>
<br>
Tips on how to dress like <?php echo $celebname_label; ?>.<br></br>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jackets & Coats</h3></div>
<br>
<?php if( have_rows('jacket') ): ?>
Our selection of <?php echo $celebname_label; ?>'s coolest jackets, coats, etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('jacket') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" display: 'inline-block' border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a></br>
<br>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display:inline-block; '>Price: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?><br></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no Jackets & coats listed.
<?php endif; ?>
		</br><br>


<?php if(in_array('male', get_field('sex') )): ?><?php else: ?><div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Skirts & Dresses</h3></div>
<br>
<?php if( have_rows('skirts') ): ?>
<?php echo $celebname_label; ?>'s favorite mini skirt & dresses.
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('skirts') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br>
<div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Currently no mini-skirts & dresses listed.
<?php endif; ?><br>

		</br><?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jeans & Trousers</h3></div>
<br>
<?php if( have_rows('jeans') ): ?>
Our selection of <?php echo $celebname_label; ?>'s jeans, leggings & trousers etc.</br>
<br>
						<ul class="slides">

						<?php while( have_rows('jeans') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; '>Price: $<?php echo $price; ?> • <?php if( $materialValue ): ?>Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no jeans & trousers listed.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>T-shirts, tops & sweatshirts</h3></div>
<br>
<?php if( have_rows('tops') ): ?>
Our selection of <?php echo $celebname_label; ?>'s T-shirts, tops & sweatshirts </br>
<br>
						<ul class="slides">

						<?php while( have_rows('tops') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Currently no T-Shirts, Tops & Sweatshirts listed.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Suits, Leisure & Swimwear</h3></div>
<br>
<?php if( have_rows('suits') ): ?>
Our selection of <?php echo $celebname_label; ?>'s suits, blazers, trunks or bikini's. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('suits') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?> <?php if( $materialValue ): ?> • Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no Suits, Leisure or Swimwear listed.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Accessories, Shoes & Lingerie/Underwear</h3></div>
<br>
<?php if( have_rows('accessories') ): ?>
Our selection of <?php echo $celebname_label; ?>'s Accessories, Shoes & Lingerie/Underwear </br>
<br>
						<ul class="slides">

						<?php while( have_rows('accessories') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br>
<div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no Accessories, Jewelry or Shoes listed.
<?php endif; ?>
		</br>
<?php if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } ?>
<div id="taboola-below-article-thumbnails"></div>
<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({
    mode: 'thumbnails-a',
    container: 'taboola-below-article-thumbnails',
    placement: 'Below Article Thumbnails',
    target_type: 'mix'
  });
</script>

- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Look & Beauty tips</a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Lifestyle, friends & family</a></b></br>
<i>Last updated: <?php the_modified_date(); ?></i></br>
Share <?php echo $celebname_label; ?> clothing & fashion tips below:
</br>

<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s kledingstijl & tips van de <?php echo $descript_label; ?> <?php echo $profession_label; ?> & <?php echo $starsign_label; ?>' "/></a>

<?php echo $celebname_label; ?> heeft een <?php echo $style_label; ?> kledingstijl, dit is te zien aan <?php if(in_array('male', get_field('sex') )): ?>zijn<?php else: ?>haar<?php endif; ?> <?php echo $distinct_feature_label; ?>, en voorkeur voor <?php echo $colors_label; ?> kleuren. De uit <?php echo $place_of_birth ?> afkomstige <?php echo $age ?> jaar oude <?php echo $profession_label; ?>, bekend van <?php echo $claim_to_fame ?>, is geboren op <b><?php echo $birth_date ?></b>. 
</br><br>
Wil je dezelfde kledingstijl als <?php echo $celebname_label; ?>? </br></b>
<i>Hier is alles wat je moet weten:</i>
<br>
<br>
<a rel="nofollow" href="<?php echo $style_photo?>"><img src="<?php echo $style_photo ?>" rel='nofollow' width="300" height="" border="0" align='right' hspace="40" alt='<?php echo $celebname_label; ?> in favoriete outfit' "/></a>Deze <?php echo $descript_label; ?> <?php echo $profession_label; ?><?php if( $endorsements ): ?> verschijnt in commercials van merken zoals <?php echo $endorsements; ?>, en<?php endif; ?> draagt zelf onder andere: <?php echo $brands ?>. </br><br>
<b>Lichaamsbouw:</b> <?php echo $body_label; ?></br>
<br>
<b>Kenmerkend kledingstuk:</b> <?php echo $distinct_feature_label; ?></br>
<br>
<b>Lengte:</b> <?php echo $height ?> cm</br>
<br>
<b>Gewicht:</b> <?php echo $weight ?> kilo</br>
<br>
<?php if( get_post_meta($post->ID, "shoe_size", true) ): ?>
<b>Schoenmaat:</b> <?php echo $shoe_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "dress_size", true) ): ?>
<b>Jurk maat:</b> <?php echo $dress_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "bra", true) ): ?>
<b>Beha maat:</b> <?php echo $bra ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Biceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Buste:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Taille:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Billen:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> iconische kledingstukken & style tips</h2>
<br>
Tips om je net zo te kleden als <?php echo $celebname_label; ?>:<br></br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jassen & Jacks</h3></div>
<br>
<?php if( have_rows('jacket') ): ?>
Onze selectie van <?php echo $celebname_label; ?>’s coolste jassen, jacks, etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('jacket') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> draagt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<b><?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>:</b> <em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Materiaal: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Nog geen jassen & jacks bekend.
<?php endif; ?>
		</br><br>


<?php if(in_array('male', get_field('sex') )): ?><?php else: ?><div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Rokken & Jurken</h3></div>
<br>
<?php if( have_rows('skirts') ): ?>
<?php echo $celebname_label; ?>’s favoriete mini rokken & jurken.
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('skirts') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> draagt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<b><?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>:</b> <em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?> <?php if( $materialValue ): ?> • Materiaal: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div><?php endif; ?></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Nog geen mini-rokjes & jurken bekend.
<?php endif; ?><br>

		</br><?php endif; ?>


<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Spijkerbroeken, Pantalons & Broeken</h3></div>
<br>
<?php if( have_rows('jeans') ): ?>
Onze selectie van <?php echo $celebname_label; ?>'s spijkerbroeken, pantalons, leggings & broeken etc.</br>
<br>
						<ul class="slides">

						<?php while( have_rows('jeans') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> draagt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<b><?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>:</b> <em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?>.</br><br>
<div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?> • <?php if( $materialValue ): ?>Materiaal: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Nog geen spijkerbroeken, pantalons & broeken bekend.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>T-shirts, tops & sweaters</h3></div>
<br>
<?php if( have_rows('tops') ): ?>
Onze selectie van <?php echo $celebname_label; ?>'s t-shirts, tops & sweaters </br>
<br>
						<ul class="slides">

						<?php while( have_rows('tops') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> draagt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<b><?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>:</b> <em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Materiaal: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Nog geen t-shirts, tops & sweaters bekend.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Maatpakken, vrijetijdskleding & zwemkleding</h3></div>
<br>
<?php if( have_rows('suits') ): ?>
Onze selectie van <?php echo $celebname_label; ?>’s favoriete maatpakken, blazers, korte broeken of bikini's. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('suits') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> draagt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<b><?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>:</b> <em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?> <?php if( $materialValue ): ?> • Materiaal: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Nog geen pakken, vrijetijdskleding of zwemkleding bekend.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Accessoires, schoenen & lingerie/ondergoed</h3></div>
<br>
<?php if( have_rows('accessories') ): ?>
Onze selectie van <?php echo $celebname_label; ?>'s accessoires, schoenen & lingerie/ondergoed </br>
<br>
						<ul class="slides">

						<?php while( have_rows('accessories') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br>
<div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> draagt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<b><?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>:</b> <em><?php echo $name; ?></em><?php if( $brand ): ?> van <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Materiaal: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Nog geen accessoires, sieraden of schoenen bekend.
<?php endif; ?>
		</br>

</br><br>
<i>Laatste update: <?php the_modified_date(); ?></i>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Look & schoonheidstips </a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Lifestyle, vrienden & familie</a></b></br>
<br>
Deel <?php echo $celebname_label; ?>’s fashion & kledingtips hieronder:
</br><br>

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s clothing style & tips of the <?php echo $descript_label; ?> <?php echo $profession_label; ?> & <?php echo $starsign_label; ?>' "/></a>

<?php echo $celebname_label; ?>'s Kleidungsstil ist <?php echo $style_label; ?>, nach Beurteilung <?php if(in_array('male', get_field('sex') )): ?>seiner<?php else: ?>ihrer<?php endif; ?> <?php echo $distinct_feature_label; ?>, und dem Geschmack für <?php echo $colors_label; ?> Farben. Der in <?php echo $place_of_birth ?> geborene <?php echo $age ?>-jährige <?php echo $profession_label; ?> bekannt für <?php echo $claim_to_fame ?> ist geboren am <b><?php echo $birth_date ?></b>. 
</br><br>
Wollen Sie <?php echo $celebname_label; ?>'s Kleidungsstil? </br></b>
<i>Dies ist was Sie wissen müssen:</i>
<br>
<br>
<a rel="nofollow" href="<?php echo $style_photo?>"><img src="<?php echo $style_photo ?>" rel='nofollow' width="300" height="" border="0" align='right' hspace="40" alt='<?php echo $celebname_label; ?> in Lieblings-Outfit' "/></a>
Dieser <?php echo $descript_label; ?> <?php echo $profession_label; ?><?php if( $endorsements ): ?> billigt Marken wie <?php echo $endorsements; ?>, und<?php endif; ?> trägt: <?php echo $brands ?>. </br><br>
<b>Körpertyp:</b> <?php echo $body_label; ?></br>
<br>
<b>Typisches Outfit:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Höhe:</b> <?php echo $height ?> cm</br>
<br>
<b>Gewicht:</b> <?php echo $weight ?> Kilo</br>
<br>
<?php if( get_post_meta($post->ID, "shoe_size", true) ): ?>
<b>Schuhgröße:</b> <?php echo $shoe_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "dress_size", true) ): ?>
<b>Kleidergröße:</b> <?php echo $dress_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "bra", true) ): ?>
<b>BH-Größe:</b> <?php echo $bra ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Bizeps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Busen:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Taille:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Po:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> ikonische Kleidungsstücke & Tipps</h2>
<br>
Tipps zum Kleiden wie <?php echo $celebname_label; ?>.<br></br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jacken & Mäntel</h3></div>
<br>
<?php if( have_rows('jacket') ): ?>
Unsere Auswahl aus <?php echo $celebname_label; ?>'s coolsten Jacken, Mänteln, etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('jacket') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> trägt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Derzeit keine Jacken & Mäntel aufgeführt.
<?php endif; ?>
		</br><br>


<?php if(in_array('male', get_field('sex') )): ?><?php else: ?><div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Röcke & Kleider</h3></div>
<br>
<?php if( have_rows('skirts') ): ?>
<?php echo $celebname_label; ?>'s Lieblings-Minirock & Lieblings-Kleider.
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('skirts') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> trägt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?> <?php if( $materialValue ): ?> • Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div><?php endif; ?></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Zur Zeit sind keine Miniröcke & Kleider aufgeführt.
<?php endif; ?><br>

		</br><?php endif; ?>


<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jeans & Hosen</h3></div>
<br>
<?php if( have_rows('jeans') ): ?>
Unsere Auswahl aus <?php echo $celebname_label; ?>'s Jeans, Leggings & Hosen etc.</br>
<br>
						<ul class="slides">

						<?php while( have_rows('jeans') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> trägt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?> • <?php if( $materialValue ): ?>Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Zur Zeit sind keine Jeans & Hosen aufgeführt.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>T-Shirts, Tops & Sweatshirts</h3></div>
<br>
<?php if( have_rows('tops') ): ?>
Unsere Auswahl aus <?php echo $celebname_label; ?>'s T-Shirts, Tops & Sweatshirts </br>
<br>
						<ul class="slides">

						<?php while( have_rows('tops') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> trägt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Zur Zeit sind keine T-Shirts, Tops & Sweatshirts aufgeführt.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Anzüge, Freizeit- & Badekleidung</h3></div>
<br>
<?php if( have_rows('suits') ): ?>
Unsere Auswahl aus <?php echo $celebname_label; ?>'s Anzügen, Blazer, Badehosen oder Bikinis. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('suits') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> trägt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?> <?php if( $materialValue ): ?> • Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Derzeit sind keine Anzüge, Freizeitkleidung oder Bademode aufgeführt.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Accessoires, Schuhe & Dessous / Unterwäsche</h3></div>
<br>
<?php if( have_rows('accessories') ): ?>
Unsere Auswahl aus <?php echo $celebname_label; ?>'s Accessoires, Schuhe & Dessous / Unterwäsche </br>
<br>
						<ul class="slides">

						<?php while( have_rows('accessories') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br>
<div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> trägt <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produkt<?php if( $brand ): ?> von <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Derzeit keine Accessoires, Schmuck oder Schuhe aufgeführt.
<?php endif; ?>
		</br>

</br><br>
<i>Zuletzt aktualisiert: <?php the_modified_date(); ?></i>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Tipps für Aussehen & Schönheit</a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Lifestyle, Freunde & Familie</a></b></br>
<br>
Teile <?php echo $celebname_label; ?>'s Kleidungs- & Mode-Tipps unten:
</br><br>

<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?>, <?php echo date("Y"); ?>'s clothing style & tips of the <?php echo $descript_label; ?> <?php echo $profession_label; ?> & <?php echo $starsign_label; ?>' "/></a>

<?php echo $celebname_label; ?> a un style de vêtements <?php echo $style_label; ?>, à constater par <?php if(in_array('male', get_field('sex') )): ?>son<?php else: ?>sa<?php endif; ?> <?php echo $distinct_feature_label; ?>, et son goût pour les <?php echo $colors_label; ?> couleurs. Il est né à <?php echo $place_of_birth ?> àgé de <?php echo $age ?>- ans <?php echo $profession_label; ?> célèbre pour <?php echo $claim_to_fame ?> et né le  <b><?php echo $birth_date ?></b>. 
</br><br>
Besoin d’obtenir le style de vêtements de <?php echo $celebname_label; ?> ? </br></b>
<i> Voici ce que vous devez savoir:</i>
<br>
<br>
<a rel="nofollow" href="<?php echo $style_photo?>"><img src="<?php echo $style_photo ?>" rel='nofollow' width="300" height="" border="0" align='right' hspace="40" alt='<?php echo $celebname_label; ?> in favorite outfit' "/></a>
C’est un  <?php echo $descript_label; ?> <?php echo $profession_label; ?><?php if( $endorsements ): ?> par les marques approuvées comme <?php echo $endorsements; ?>, et<?php endif; ?> porte: <?php echo $brands ?>. </br><br>
<b> Corpulence:</b> <?php echo $body_label; ?></br>
<br>
<b> Costume typique:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b> Taille:</b> <?php echo $height ?> cm</br>
<br>
<b> Poids:</b> <?php echo $weight ?> kilo</br>
<br>
<?php if( get_post_meta($post->ID, "shoe_size", true) ): ?>
<b> Pointure:</b> <?php echo $shoe_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "dress_size", true) ): ?>
<b> Taille de robe:</b> <?php echo $dress_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "bra", true) ): ?>
<b> Taille soutien-gorge:</b> <?php echo $bra ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Biceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Buste:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b> Taille:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Butt:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br>
<h2><?php echo $celebname_label; ?>'s <?php echo date("Y"); ?> conseils et articles d'habillement emblématique </h2>
<br>
Conseils sur la façon de s'habiller comme <?php echo $celebname_label; ?>.<br></br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3> Vestes & manteaux </h3></div>
<br>
<?php if( have_rows('jacket') ): ?>
Notre séléction de <?php echo $celebname_label; ?> de cool vestes, de manteaux, etc. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('jacket') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Matériel: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actuellement aucune veste ni manteau listés.
<?php endif; ?>
		</br><br>


<?php if(in_array('male', get_field('sex') )): ?><?php else: ?><div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3> Mini jupe & Robes </h3></div>
<br>
<?php if( have_rows('skirts') ): ?>
<?php echo $celebname_label; ?> Mini jupe & Robes préférées.
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('skirts') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?> <?php if( $materialValue ): ?> • Matériel: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div><?php endif; ?></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Actuellement aucune mini-jupe ni robe listées.
<?php endif; ?><br>

		</br><?php endif; ?>


<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3> Jeans & pantalons </h3></div>
<br>
<?php if( have_rows('jeans') ): ?>
Notre sélection de <?php echo $celebname_label; ?> jeans, leggings & pantalons etc.</br>
<br>
						<ul class="slides">

						<?php while( have_rows('jeans') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?> • <?php if( $materialValue ): ?>Matériel: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actuellement aucun jeans ni pantalons listés.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>T-shirts, tops et pulls </h3></div>
<br>
<?php if( have_rows('tops') ): ?>
Notre sélection de <?php echo $celebname_label; ?> T-shirts, tops et pulls </br>
<br>
						<ul class="slides">

						<?php while( have_rows('tops') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Matériel: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Actuellement pas de T-Shirts, top ou pulls listés.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3> Costumes, loisirs & maillots de bain </h3></div>
<br>
<?php if( have_rows('suits') ): ?>
Notre sélection de <?php echo $celebname_label; ?> costumes, blazers ou bikini. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('suits') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?> <?php if( $materialValue ): ?> • Matériel: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actuellement pas de costumes de loisirs ou de maillots de bain listés.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3> Chaussures, accessoires et Lingerie/sous-vêtements </h3></div>
<br>
<?php if( have_rows('accessories') ): ?>
Notre sélection de <?php echo $celebname_label; ?> chaussures, accessoires et Lingerie/sous-vêtements </br>
<br>
						<ul class="slides">

						<?php while( have_rows('accessories') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br>
<div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> wears <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> produit<?php if( $brand ): ?> par <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Matériel: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actuellement pas d'accessoires, de bijoux ou de chaussures listés.
<?php endif; ?>
		</br>

</br><br>
<i> Mise à jour: <?php the_modified_date(); ?></i>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Conseils en Look & beauté </a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - mode de vie, amis & famille </a></b></br>
<br>
Share <?php echo $celebname_label; ?> conseils en habillement & en mode ci-dessous :
</br>

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='right' hspace="10" alt='Estilo y consejos de ropa de <?php echo $celebname_label; ?> para <?php echo date("Y"); ?> <?php if(in_array('male', get_field('sex') )): ?>del<?php else: ?>de la<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?> & <?php echo $starsign_label; ?>' "/></a>

El estilo de ropa de <?php echo $celebname_label; ?> es <?php echo $style_label; ?>, juzgando por su <?php echo $distinct_feature_label; ?>, y gusto por los colores <?php echo $colors_label; ?>. <?php if(in_array('male', get_field('sex') )): ?>El<?php else: ?>La<?php endif; ?> <?php echo $profession_label; ?> <?php if(in_array('male', get_field('sex') )): ?>nacido<?php else: ?>nacida<?php endif; ?>  en <?php echo $place_of_birth ?>, con <?php echo $age ?> años de edad con fama por <?php echo $claim_to_fame ?> es de <b><?php echo $birth_date ?></b>. 
</br><br>
¿Quieres el estilo de ropa de <?php echo $celebname_label; ?>? </br></b>
<i>Aquí está lo que necesita saber:</i>
<br>
<br>
<a rel="nofollow" href="<?php echo $style_photo?>"><img src="<?php echo $style_photo ?>" rel='nofollow' width="300" height="" border="0" align='right' hspace="40" alt='<?php echo $celebname_label; ?> en su ropa favorita' "/></a>
<?php if(in_array('male', get_field('sex') )): ?>Este<?php else: ?>Esta<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?><?php if( $endorsements ): ?> promociona marcas como <?php echo $endorsements; ?>, y<?php endif; ?> usa: <?php echo $brands ?>. </br><br>
<b>Tipo de Cuerpo:</b> <?php echo $body_label; ?></br>
<br>
<b>Ropa típica:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Altura:</b> <?php echo $height ?> cm</br>
<br>
<span style="font-size: medium;"><b>Peso:</b> <?php echo $weight ?> kilo</span></br>
<br>
<?php if( get_post_meta($post->ID, "shoe_size", true) ): ?>
<b>Tamaño de zapato:</b> <?php echo $shoe_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "dress_size", true) ): ?>
<b>Tamaño de vestido:</b> <?php echo $dress_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "bra", true) ): ?>
<b>Tamaño de sostén:</b> <?php echo $bra ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Bíceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Busto:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Cintura:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Trasero:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br>
<h2>Artículos icónicos y consejos de ropa de <?php echo $celebname_label; ?> para <?php echo date("Y"); ?> </h2>
<br>
Consejos para vestirse como <?php echo $celebname_label; ?>.<br></br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Chaquetas y Chaquetónes </h3></div>
<br>
<?php if( have_rows('jacket') ): ?>
Nuestra selección de las mejores chaquetas, chaquetónes, etc. de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('jacket') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> viste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> </br> Precio: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actualmente no hay chaquetas y chaquetónes listados.
<?php endif; ?>
		</br><br>


<?php if(in_array('male', get_field('sex') )): ?><?php else: ?><div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Faldas y Vestidos </h3></div>
<br>
<?php if( have_rows('skirts') ): ?>
Mini faldas y vestidos favoritos de <?php echo $celebname_label; ?>.
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('skirts') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> viste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Actualmente no hay mini-faldas y vestidos listados.
<?php endif; ?><br>

		</br><?php endif; ?>


<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jeans & Pantalones</h3></div>
<br>
<?php if( have_rows('jeans') ): ?>
Nuestra selección de jeans, mallas y pantalones de <?php echo $celebname_label; ?>.</br>
<br>
						<ul class="slides">

						<?php while( have_rows('jeans') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> viste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actualmente no hay jeans y pantalones listados.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Camisetas, corpetes y suéteres</h3></div>
<br>
<?php if( have_rows('tops') ): ?>
Nuestra selección de camisetas, corpetes y suéteres de <?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('tops') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> viste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Actualmente no hay camisetas, corpetes y suéteres listados.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Trajes, casuales y ropas de baño </h3></div>
<br>
<?php if( have_rows('suits') ): ?>
Nuestra selección de trajes, blazers, bañadores o bikinis de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('suits') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> viste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actualmente no hay trajes, casuales o ropas de baño listados.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Accesorios, Zapatos & Lencería/Ropa Interior </h3></div>
<br>
<?php if( have_rows('accessories') ): ?>
Nuestra selección de accesorios, zapatos y lencería/ropa interior de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('accessories') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br>
<div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> viste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Actualmente no hay accesorios, joyerías o zapatos listados.
<?php endif; ?>
		</br>

</br><br>
<i>Última actualización: <?php the_modified_date(); ?></i>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Consejos de Apariencia y Belleza </a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Estilo de vida, amigos y família </a></b></br>
<br>
Comparte los consejos de ropa y moda de <?php echo $celebname_label; ?> abajo:
</br><br>

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='right' hspace="10" alt='Estilo e dicas de roupa de <?php echo $celebname_label; ?> para <?php echo date("Y"); ?> <?php if(in_array('male', get_field('sex') )): ?>do<?php else: ?>da<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?> & <?php echo $starsign_label; ?>' "/></a>

O estilo de roupa de <?php echo $celebname_label; ?> é <?php echo $style_label; ?>, julgando pelo seu(sua) <?php echo $distinct_feature_label; ?>, e gosto pelas cores <?php echo $colors_label; ?>. <?php if(in_array('male', get_field('sex') )): ?>O<?php else: ?>A<?php endif; ?> <?php echo $profession_label; ?> <?php if(in_array('male', get_field('sex') )): ?>nascido<?php else: ?>nascida<?php endif; ?>  em <?php echo $place_of_birth ?>, com <?php echo $age ?> anos de idade e famoso por <?php echo $claim_to_fame ?> é de <b><?php echo $birth_date ?></b>. 
</br><br>
Quer o estilo de roupa de <?php echo $celebname_label; ?>? </br></b>
<i>Aqui está o que você precisa saber:</i>
<br>
<br>
<a rel="nofollow" href="<?php echo $style_photo?>"><img src="<?php echo $style_photo ?>" rel='nofollow' width="300" height="" border="0" align='right' hspace="40" alt='<?php echo $celebname_label; ?> em sua roupa favorita' "/></a>
<?php if(in_array('male', get_field('sex') )): ?>Este<?php else: ?>Esta<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?><?php if( $endorsements ): ?> promociona marcas como <?php echo $endorsements; ?>, e<?php endif; ?> usa: <?php echo $brands ?>. </br><br>
<b>Tipo de Corpo:</b> <?php echo $body_label; ?></br>
<br>
<b>Roupa típica:</b> <span style='text-transform: capitalize;'><?php echo $distinct_feature_label; ?></span></br>
<br>
<b>Altura:</b> <?php echo $height ?> cm</br>
<br>
<b>Peso:</b> <?php echo $weight ?> quilos</br>
<br>
<?php if( get_post_meta($post->ID, "shoe_size", true) ): ?>
<b>Tamanho de sapato:</b> <?php echo $shoe_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "dress_size", true) ): ?>
<b>Tamanho de vestido:</b> <?php echo $dress_size ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "bra", true) ): ?>
<b>Tamanho de sutiã:</b> <?php echo $bra ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "biceps", true) ): ?>
<b>Bíceps:</b> <?php echo $biceps ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "buste", true) ): ?>
<b>Busto:</b> <?php echo $bust ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "waist", true) ): ?>
<b>Cintura:</b> <?php echo $waist ?></br>
<br>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "butt", true) ): ?>
<b>Bunda:</b> <?php echo $butt ?></br>
<br>
<?php endif; ?>
<br>
<h2>Artigos icônicos e dicas de moda de <?php echo $celebname_label; ?> para <?php echo date("Y"); ?> </h2>
<br>
Dicas para se vestir como <?php echo $celebname_label; ?>.<br></br>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jaquetas e Casacos </h3></div>
<br>
<?php if( have_rows('jacket') ): ?>
Nossa seleção das melhores jaquetas, casacos, etc. de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('jacket') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> veste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Atualmente não há jaquetas e casacos listados.
<?php endif; ?>
		</br><br>


<?php if(in_array('male', get_field('sex') )): ?><?php else: ?><div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Saias e Vestidos </h3></div>
<br>
<?php if( have_rows('skirts') ): ?>
Mini-saias e vestidos favoritos de <?php echo $celebname_label; ?>.
</br> <br>
<br>
						<ul class="slides">

						<?php while( have_rows('skirts') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> veste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Atualmente não há mini-saias e vestidos listados.
<?php endif; ?><br>

		</br><?php endif; ?>


<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Jeans & Calças</h3></div>
<br>
<?php if( have_rows('jeans') ): ?>
Nossa seleção de jeans, leggins e calças de <?php echo $celebname_label; ?>.</br>
<br>
						<ul class="slides">

						<?php while( have_rows('jeans') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> veste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Atualmente não há jeans e calças listadas.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Camisetas, tops y suéteres</h3></div>
<br>
<?php if( have_rows('tops') ): ?>
Nossa seleção de camisetas, tops e suéteres de <?php echo $celebname_label; ?></br>
<br>
						<ul class="slides">

						<?php while( have_rows('tops') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> veste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?> Atualmente não há camisetas, tops e suéteres listados.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Trajes, roupas de lazer e de banho </h3></div>
<br>
<?php if( have_rows('suits') ): ?>
Nossa seleção de trajes, blazers, calções de banho ou biquinis de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('suits') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> veste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>) ' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Atualmente não há trajes, roupas de lazer ou de banho listados.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Acessórios, Sapatos & Lingerie/Roupa Íntima</h3></div>
<br>
<?php if( have_rows('accessories') ): ?>
Nossa seleção de acessórios, sapatos e lingerie/roupa íntima de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('accessories') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		     $photo = get_sub_field('photo');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_sub_field_object('material');
							$materialValue = get_sub_field('material');

							?> 

							<li class="slide">


							
<br>
<div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?> veste <?php echo $name; ?> (<?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?>)' "/></a>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> por <?php echo $brand; ?><?php endif; ?>.</br><br><br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?> <?php if( $materialValue ): ?>• Material: <?php if ($materialValue) {

								foreach ($materialField['choices'] as $key => $value) {

									foreach ($materialValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?><?php endif; ?></div></br><br>
<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?> Atualmente não há acessórios, jóias ou sapatos listados.
<?php endif; ?>
		</br>

</br><br>
<i>Última atualização: <?php the_modified_date(); ?></i>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Dicas de Aparência e Beleza </a></br>
- <a href='http://muzul.com/celebrity/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Estilo de vida, amigos e família </a></b></br>
<br>
Compartilhe as dicas de roupa e moda de <?php echo $celebname_label; ?> abaixo:
</br><br>


<?php endif; ?>

<?php 
                    // Ad Unit
                    if ( $ti_option['single_image_bottom_ad']['url'] == true || $ti_option['single_code_bottom_ad'] == true ) { ?>
                        <div class="single-box advertisement">
                            <?php
                            // Image Ad
                            if ( $ti_option['single_image_bottom_ad']['url'] == true ) {
                                $single_banner_botom = $ti_option['single_image_bottom_ad']; ?>
                                <a href="<?php echo $ti_option['single_image_bottom_ad_url']; ?>" rel="nofollow" target="_blank">
                                    <img src="<?php echo $single_banner_botom['url']; ?>" width="<?php echo $single_banner_botom['width']; ?>" height="<?php echo $single_banner_botom['height']; ?>" alt="<?php _e( 'Advertisement', 'themetext' ); ?>" />
                                </a>
                            <?php
                            }
                            // Code Ad
                            elseif ( $ti_option['single_code_bottom_ad'] == true ) {
                                echo $ti_option['single_code_bottom_ad'];
                            } ?>
                        </div><!-- .advertisment -->
                    <?php } ?>
<?php the_content(); ?>

                        <?php
                        $args = array(
                            'before' => '<div class="link-pages"><h3 class="title">' . __( 'Continue Reading', 'themetext' ) . '</h3>',
                            'after' => '</div>',
                            'link_before' => '<span>',
                            'link_after' => '</span>',
                            'nextpagelink'     => '&rarr;',
                            'previouspagelink' => '&larr;',
                            'next_or_number'   => 'next_and_number',
                        );
                        wp_link_pages( $args );
                        ?>
                   <!-- .entry-content -->


 </div></div><?php get_sidebar(); ?></div>


                    <?php
                    // Post Rating output at the bottom
                    if ( $ti_option['single_rating_box'] == 'rating_bottom' ) {
                        get_template_part( 'inc/single', 'rating' );
                    }
                    
                    // Show/Hide tags list
                    if ( $ti_option['single_tags_list'] == 1 ) {
                        the_tags('<div class="single-box tag-box clearfix"><h3 class="title">' . __( 'Tags', 'themetext' ) . '</h3>', '', '</div>'); 
                    }


                    
                    // Show/Hide share links
                    if ( $ti_option['single_social'] == 1 ) {
                        get_template_part( 'inc/single', 'share' );
                    }
                    
                    ?>

                

                    <?php  
                    // Navigation
                    if ( $ti_option['single_nav_arrows'] == 0 ) { // Show/Hide Previous Post / Next Post Navigation
                        $prev_post = get_previous_post();
                        $next_post = get_next_post();
                        if ( !empty( $prev_post ) && !empty( $next_post ) ) {
                        ?>
                            <nav class="single-box clearfix nav-single">
                                <?php if ( !empty( $prev_post ) ) { ?>
                                <div class="nav-previous">
                                    <?php previous_post_link ( '%link', '<i class="icomoon-chevron-left"></i><span class="sub-title">' . __( 'Previous article', 'themetext' ) . '</span>%title', TRUE ); ?>
                                </div>
                                <?php } ?>

                                <?php if ( !empty( $next_post ) && !empty( $prev_post ) ) { ?>
                                    <span class="sep"></span>
                                <?php } ?> 

                                <?php if ( !empty( $next_post ) ){ ?>
                                <div class="nav-next">
                                    <?php next_post_link( '%link', '<i class="icomoon-chevron-right"></i><span class="sub-title">' . __( 'Next article', 'themetext' ) . '</span>%title', TRUE ); ?>
                                </div>
                                <?php } ?>
                            </nav><!-- .nav-single -->
                        <?php } ?>
                    <?php } ?>


                    <?php if ( ! $single_sidebar || $single_sidebar == "post_sidebar_on" ) { // Enable/Disable post sidebar ?>
                    </div><!-- .grid-8 -->

                    <?php get_sidebar(); ?>
                </div><!-- .grids -->
                <?php }  ?>

            </div><!-- .wrapper -->

        </article>

    <?php endwhile; endif; ?>

    </main><!-- #content -->

    <?php
    // Show/Hide random posts slide dock
    if ( $ti_option['single_slide_dock'] == 1 ) {
        get_template_part( 'inc/slide', 'dock' );
    }
    ?>
    
<?php get_footer(); ?>