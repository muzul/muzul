<?php 
/**
 * The Template for displaying all single blog posts
 *
 * @package SimpleMag
 * @since 	SimpleMag 1.0
**/
get_header(); 
global $ti_option;
$single_sidebar = get_post_meta( $post->ID, 'post_sidebar', true );
?>

<?php

						$photo = get_field('photo');
						$born_on = get_field('born_on');
						$birthplace = get_field('birthplace');
						$claim_to_fame = get_field('claim_to_fame');
						$nickname= get_field('nickname');
						$text = get_field('text');
						$place_of_birth = get_field('place_of_birth');
						$birth_date = get_field('birth_date');

		                                $celebname_field = get_field_object('celebname');
						$celebname_value = get_field('celebname');
						$celebname_label = $celebname_field['choices'][ $celebname_value ];

						// fill 'profession' variable
						//
						$profession_field = get_field_object('profession');
						$profession_value = get_field('profession');
						$profession_label = '';

						if(count($profession_value) < 2) {
						$profession_label = $profession_field['choices'][ $profession_value[0] ];
						} else {
						foreach($profession_value as $val) {
						$profession_label .= $profession_field['choices'][ $val ] . ' ';
						}
						}

// fill 'colors' variable
						//
						$colors_field = get_field_object('colors');
						$colors_value = get_field('colors');
						$colors_label = '';

						if(count($colors_value) < 2) {
						$colors_label = $colors_field['choices'][ $colors_value[0] ];
						} else {
						foreach($colors_value as $val) {
						$colors_label .= $colors_field['choices'][ $val ] . ' ';
						}
						}




// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

						// fill 'hairstyle' variable
						//
						$hairstyle_field = get_field_object('hairstyle');
						$hairstyle_value = get_field('hairstyle');
						$hairstyle_label = '';

						if(count($hairstyle_value) < 2) {
						$hairstyle_label = $hairstyle_field['choices'][ $hairstyle_value[0] ];
						} else {
						foreach($hairstyle_value as $val) {
						$hairstyle_label .= $hairstyle_field['choices'][ $val ] . ', ';
						}
						}

// fill 'hair_length' variable
						//
						$hair_length_field = get_field_object('hair_length');
						$hair_length_value = get_field('hair_length');
						$hair_length_label = '';

						if(count($hair_length_value) < 2) {
						$hair_length_label = $hair_length_field['choices'][ $hair_length_value[0] ];
						} else {
						foreach($hair_length_value as $val) {
						$hair_length_label .= $hair_length_field['choices'][ $val ] . ', ';
						}
						}

// fill 'make_up_style' variable
						//
						$make_up_style_field = get_field_object('make_up_style');
						$make_up_style_value = get_field('make_up_style');
						$make_up_style_label = '';

						if(count($make_up_style_value) < 2) {
						$make_up_style_label = $make_up_style_field['choices'][ $make_up_style_value[0] ];
						} else {
						foreach($make_up_style_value as $val) {
						$make_up_style_label .= $make_up_style_field['choices'][ $val ] . ', ';
						}
						}
						

	     // fill 'hair_type' variable
						//
						$hair_type_field = get_field_object('hair_type');
						$hair_type_value = get_field('hair_type');
						$hair_type_label = $hair_type_field['choices'][ $hair_type_value[0] ];

						
						//
						// get age from birth date
						//						     
						$date = new DateTime($birth_date);
						$now = new DateTime();
						$interval = $now->diff($date);
						$age = $interval->y;
						
						//
						// fill 'sex' variable
						//
						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

// fill 'nationality' variable
						//
						$nationality_field = get_field_object('nationality');
						$nationality_value = get_field('nationality');
						$nationality_label = '';

						if(count($nationality_value) < 2) {
						$nationality_label = $nationality_field['choices'][ $nationality_value[0] ];
						} else {
						foreach($nationality_value as $val) {
						$nationality_label .= $nationality_field['choices'][ $val ] . '- ';
						}
						}

						//
						// fill 'descript' variable
						//
						$descript_field = get_field_object('descript');
						$descript_value = get_field('descript');
						$descript_label = '';

						if(count($descript_value) < 2) {
						$descript_label = $descript_field['choices'][ $descript_value[0] ];
						} else {
						foreach($descript_value as $val) {
						$descript_label .= $descript_field['choices'][ $val ] . ' ';;
						}
						}

						// fill 'face' variable
						//
						$face_field = get_field_object('face');
						$face_value = get_field('face');
						$face_label = $face_field['choices'][ $face_value[0] ];

						//
						// fill 'eye' variable
						//
						$eye_field = get_field_object('eye');
						$eye_value = get_field('eye');
						$eye_label = $eye_field['choices'][ $eye_value[0] ];
						
						//
						// fill 'hair' variable
						//
						$hair_field = get_field_object('hair');
						$hair_value = get_field('hair');
						$hair_label = $hair_field['choices'][ $hair_value[0] ];

//
						// fill 'tone' variable
						//
						$tone_field = get_field_object('tone');
						$tone_value = get_field('tone');
						$tone_label = $tone_field['choices'][ $tone_value[0] ];


						// fill 'body' variable
						//
						$body_field = get_field_object('body');
						$body_value = get_field('body');
						$body_label = $body_field['choices'][ $body_value[0] ];

			// fill 'skin_type' variable
						//
						$skin_type_field = get_field_object('skin_type');
						$skin_type_value = get_field('skin_type');
						$skin_type_label = $skin_type_field['choices'][ $skin_type_value[0] ];

// fill 'love' variable
						//
						$love_field = get_field_object('love');
						$love_value = get_field('love');
						$love_label = '';

						if(count($love_value) < 2) {
						$love_label = $love_field['choices'][ $love_value[0] ];
						} else {
						foreach($love_value as $val) {
						$love_label .= $love_field['choices'][ $val ] . '- ';
						}
						}


// fill 'extra' variable
						//
						$extra_field = get_field_object('extra');
						$extra_value = get_field('extra');
						$extra_label = '';

						if(count($extra_value) < 2) {
						$extra_label = $love_field['choices'][ $extra_value[0] ];
						} else {
						foreach($extra_value as $val) {
						$extra_label .= $extra_field['choices'][ $val ] . '- ';
						}
						}

// fill 'distinct_feature' variable
						//
						$distinct_feature_field = get_field_object('distinct_feature');
						$distinct_feature_value = get_field('distinct_feature');
						$distinct_feature_label = '';

						if(count($distinct_feature_value) < 2) {
						$distinct_feature_label = $distinct_feature_field['choices'][ $distinct_feature_value[0] ];
						} else {
						foreach($distinct_feature_value as $val) {
						$distinct_feature_label .= $distinct_feature_field['choices'][ $val ] . '- ';
						}
						}





      $height = get_field('height');
						$weight = get_field('weight');
      $endorsements= get_field('endorsements');
						$brands= get_field('brands');
						$hairstyle_photo= get_field('hairstyle_photo');
						$make_up_photo= get_field('make_up_photo');
						$no_make_up_photo= get_field('no_make_up_photo');
						$net_worth= get_field('net_worth');
						$salary= get_field('salary');
						$colleagues= get_field('colleagues');
						$management= get_field('management');
						$fam_photo= get_field('fam_photo');
						$current_partner_text= get_field('current_partner_text');
      $style_photo= get_field('style_photo');
						$dress_size= get_field('dress_size');
						$shoe_size= get_field('shoe_size');
						$bra_size= get_field('bra_size');
						$biceps= get_field('biceps');
						$bust= get_field('bust');
						$waist= get_field('waist');
						$butt= get_field('butt');
						$beach= get_field('beach');
						$house= get_field('house');
			   $partners= get_field('partners');
						$smoking_caught= get_field('smoking_caught');
						$no_makeup= get_field('no_makeup');
						$since= get_field('since');
						
						//
						// get zodiac sign from birth date
						//	
						function zodiac($birthdate) {
						
							$zodiac = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiac = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiac = "Taurus"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiac = "Gemini"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiac = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiac = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiac = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiac = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiac = "Scorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiac = "Sagittarius"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiac = "Capricorn"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiac = "Aquarius"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiac = "Pisces"; } 
						     
							return $zodiac; 

						}
						
						$starsign_label = zodiac($birth_date, $sex);

// get zodiac sign from birth date
						//	
						function zodiacde($birthdate) {
						
							$zodiacde = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacde = "Widder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacde = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacde = "Zwillinge"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacde = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacde = "Löwe"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacde = "Jungfrau"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacde = "Waage"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacde = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacde = "Schütze"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacde = "Steinbock"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacde = "Wassermann"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacde = "Fische"; } 
						     
							return $zodiacde;

						}
						
						$starsignde_label = zodiacde($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacfr($birthdate) {
						
							$zodiacfr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacfr = "Bélier"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacfr = "Taureau"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacfr = "Gémaux"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacfr = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacfr = "Lion"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacfr = "Vierge"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacfr = "Balance"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacfr = "Scorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacfr = "Sagittaire"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacfr = "Capricorne"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacfr = "Verseau"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacfr = "Poisons"; } 
						     
							return $zodiacfr;

						}

						
						$starsignfr_label = zodiacfr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacbr($birthdate) {
						
							$zodiacbr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacbr = "Carneiro"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacbr = "Touro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacbr = "Gémeos"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacbr = "Caranguejo"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacbr = "Leão"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacbr = "Virgem"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacbr = "Balança"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacbr = "Escorpião"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacbr = "Sagitário"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacbr = "Capricórnio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacbr = "Aquário"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacbr = "Peixes"; } 
						     
							return $zodiacbr;

						}

						$starsignbr_label = zodiacbr($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiaces($birthdate) {
						
							$zodiaces = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiaces = "Aries"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiaces = "Tauro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiaces = "Geminis"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiaces = "Cancer"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiaces = "Leo"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiaces = "Virgo"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiaces = "Libra"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiaces = "Escorpio"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiaces = "Sagitario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiaces = "Capricornio"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiaces = "Acuario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiaces = "Piscis"; } 
						     
							return $zodiaces;

}
						$starsignes_label = zodiaces($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacpl($birthdate, $sex) {
						
							$zodiacpl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacpl = "Baran"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacpl = "Byk"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacpl = "Bliźnięta"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacpl = "Rak"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacpl = "Lew"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacpl = "Panna"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacpl = "Waga"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacpl = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacpl = "Strzelec"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacpl = "Koziorożec"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacpl = "Wodnik"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacpl = "Ryby"; } 
						     

						     
							return $zodiacpl;

}
			
						$starsignpl_label = zodiacpl($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacda($birthdate, $sex) {
						
							$zodiacda = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacda = "Vædder"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacda = "Tyr"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacda = "Tvilling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacda = "Krebs"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacda = "Løve"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacda = "Jomfru"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacda = "Vægt"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacda = "Skorpion"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacda = "Skytte"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacda = "Stenbuk"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacda = "Vandmand"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacda = "Fisk"; } 

						     

						     
							return $zodiacda;

}
			
						$starsignda_label = zodiacda($born_on, $sex);


// get zodiac sign from birth date
						//	
						function zodiacsv($birthdate, $sex) {
						
							$zodiacsv = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacsv = " Väduren "; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacsv = "Oxen"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacsv = " Tvillingarna "; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacsv = "Kräftan"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacsv = "Lejonet"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacsv = " Jungfrun "; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacsv = " Vågen "; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacsv = " Skorpionen "; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacsv = " Skytten "; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacsv = " Stenbocken "; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacsv = " Vattumannen "; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacsv = " Fiskarna "; } 
 

						     

						     
							return $zodiacsv;

}
			
						$starsignsv_label = zodiacsv($born_on, $sex);

// get zodiac sign from birth date
						//	
						function zodiacnb($birthdate, $sex) {
						
							$zodiacnb = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnb = "Væren"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnb = "Tyren"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnb = "Tvillingene"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnb = "Krepsen"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnb = "Løven"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnb = "Jomfruen"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnb = "Vekten"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnb = "Skorpionen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnb = "Skytten"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnb = "Stenbukken"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnb = "Vannmannen"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnb = "Fiskene"; } 

						     

						     
							return $zodiacnb;

}
			
						$starsignnb_label = zodiacnb($born_on, $sex);



// get zodiac sign from birth date
						//	
						function zodiacin($birthdate, $sex) {
						
							$zodiacin = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacin = "मेष"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacin = "वृषभ"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacin = "मिथुन"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacin = "कर्क"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacin = "सिंह"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacin = "कन्या"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacin = "तुला"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacin = "वृश्चिक"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacin = "धनु"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacin = "मकर"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacin = "कुंभ"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacin = "मीन"; } 
						     
							return $zodiacin;

}
			
						$starsignin_label = zodiacin($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiactr($birthdate, $sex) {
						
							$zodiactr = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiactr = "Koç"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiactr = "Boğa"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiactr = "İkizler"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiactr = "Yengeç"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiactr = "Aslan"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiactr = "Başak"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiactr = "Terazi"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiactr = "Akrep"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiactr = "Yay"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiactr = "Oğlak"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiactr = "Kova"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiactr = "Balık"; } 
						     
							return $zodiactr;

}

						$starsigntr_label = zodiactr($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacnl($birthdate, $sex) {
						
							$zodiacnl = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacnl = "Ram"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacnl = "Stier"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacnl = "Tweeling"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacnl = "Kreeft"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacnl = "Leeuw"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacnl = "Maagd"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacnl = "Weegschaal"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacnl = "Schorpioen"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacnl = "Boogschutter"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacnl = "Steenbok"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacnl = "Waterman"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacnl = "Vissen"; } 
						     
							return $zodiacnl;

}

						$starsignnl_label = zodiacnl($born_on, $sex);

// get zodiac sign from birth date

//	
						function zodiacit($birthdate, $sex) {
						
							$zodiacit = ""; 
							      
							list ($day, $month, $year) = explode ("-", $birthdate); 
							      
							if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiacit = "Ariete"; } 
							elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiacit = "Toro"; } 
							elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiacit = "Gemelli"; } 
							elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiacit = "Cancro"; } 
							elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiacit = "Leone"; } 
							elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiacit = "Vergine"; } 
							elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiacit = "Bilancia"; } 
							elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiacit = "Scorpione"; } 
							elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiacit = "Sagittario"; } 
							elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiacit = "Capricorno"; } 
							elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiacit = "Aquario"; } 
							elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiacit = "Pesci"; } 
						     
							return $zodiacit;

}

						$starsignit_label = zodiacit($born_on, $sex);
						

						//$starsign_field = get_field_object('starsign');
						//$starsign_value = get_field('starsign');
						//$starsign_label = $starsign_field['choices'][ $starsign_value[0] ];	


						?>

    <main id="content" class="clearfix animated" role="main" itemprop="mainContentOfPage" itemscope itemtype="http://schema.org/Blog">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
<center>
            <header class="wrapper entry-header page-header">
                
                    <div style='text-align: center;'><h1><?php echo $celebname_label; ?></h1></div>
            </header>

            <?php
            // Output media only on first page if the post have pagination
            if ( $paged == 1 || $page == 1 ) {
                // Output media from every post by Full Width option
                if ( $ti_option['single_media_position'] == 'useperpost' && get_post_meta( $post->ID, 'post_media_position', true ) == 'media_full_width' || $ti_option['single_media_position'] == 'fullwidth' ){
                ?>
                <div class="entry-media">
                    <?php
                    if ( ! get_post_format() ): // Standard
                        get_template_part( 'formats/format', 'standard' );
                    elseif ( 'gallery' == get_post_format() ): // Gallery
                        get_template_part( 'formats/format', 'gallery' );
                    elseif ( 'video' == get_post_format() ): // Video
                        get_template_part( 'formats/format', 'video' );
                    elseif ( 'audio' == get_post_format() ): // Audio
                        get_template_part( 'formats/format', 'audio' );
                    endif;
                    ?>
                </div>
                <?php } else { ?>
                    <?php if ( 'gallery' == get_post_format() ) { ?>
                    <div class="entry-media">
                        <?php get_template_part( 'formats/format', 'gallery' ); ?>
                    </div>
                <?php } ?>
            <?php } } ?>

           <div class="wrapper">

<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>


                    <?php
                    // Output media only on first page if the post have pagination
                    if ( $paged == 1 || $page == 1 ) {
                        // Output media from every post by Above The Content option
                        if ( $ti_option['single_media_position'] == 'useperpost' && get_post_meta( $post->ID, 'post_media_position', true ) == 'media_above_content' || $ti_option['single_media_position'] == 'abovecontent' ) {
                        ?>
                        <div class="entry-media">
                            <?php 
                            if ( ! get_post_format() ): // Standard
                                get_template_part( 'formats/format', 'standard' );
                            elseif ( 'video' == get_post_format() ): // Video
                                get_template_part( 'formats/format', 'video' );
                            elseif ( 'audio' == get_post_format() ): // Audio
                                get_template_part( 'formats/format', 'audio' );
                            endif;
                            ?>
                        </div>
                    <?php } } ?>

                    <?php
                    // Post Rating
                    if ( $ti_option['single_rating_box'] == 'rating_top' ) {
                        get_template_part( 'inc/single', 'rating' );
                    }
                    ?>

                    <?php 
                    // Ad Unit
                    if ( $ti_option['single_image_top_ad']['url'] == true || $ti_option['single_code_top_ad'] == true ) { ?>
                    <div class="advertisement">
                        <?php
                        $single_banner_top = $ti_option['single_image_top_ad'];
                        // Image Ad
                        if ( $single_banner_top['url'] == true ) { ?>
                            <a href="<?php echo $ti_option['single_image_top_ad_url']; ?>" rel="nofollow" target="_blank">
                                <img src="<?php echo $single_banner_top['url']; ?>" width="<?php echo $single_banner_top['width']; ?>" height="<?php echo $single_banner_top['height']; ?>" alt="<?php _e( 'Advertisement', 'themetext' ); ?>" />
                            </a>
                        <?php 
                        // Code Ad
                        } elseif( $ti_option['single_code_top_ad'] == true ) {
                            echo $ti_option['single_code_top_ad'];
                        } ?>
                    </div><!-- .advertisment -->
                    <?php } ?>

                    <div class="single-box clearfix entry-content" itemprop="articleBody">
                        		

<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> earned a <?php echo $salary ?> million dollar salary, leaving the net worth at <?php echo $net_worth ?> million in <?php echo date("Y"); ?>'s ' "/></a>
<br>
<b><?php echo $celebname_label; ?> made <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php echo $net_worth ?> million dollar fortune with <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, and by endorsing brands like <?php echo $endorsements; ?><?php endif; ?>. But how does this <?php echo $descript_label; ?> <?php echo $profession_label; ?> from <?php echo $place_of_birth ?> spend <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> money, and who are the friends & family <?php if(in_array('male', get_field('sex') )): ?>he<?php else: ?>she<?php endif; ?> spends it on?</b> 
<hr><?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?><br>
<h3>Quick personal facts about <?php echo $celebname_label; ?> </br></h3>
<br>
<b>Origin:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Birth date:</b> <?php echo $birth_date ?></br>
<br>
<b>Estimated net worth:</b> <?php echo $net_worth ?> million</br>
<br>
<?php if( $salary ): ?><b>Salary:</b> <?php echo $salary ?> million</br>
<br><?php endif; ?>
<?php if(in_array('voetballer', get_field('profession') )): ?><b>Must-read:</b></br>- <a rel='nofollow' target="_blank" href="http://taddlr.com/top-15-highest-paid-soccer-players-2014/"><u>New: Top 15 highest paid footballers of 2015!</u></a></br><br><?php endif; ?>
<?php if(in_array('actrice', get_field('profession') )): ?>
<b>Must-read:</b></br>- <a href="http://muzul.com/the-10-richest-celebrities-in-hollywood-net-worth-salary/"><u>The 10 richest celebrities in Hollywood – Net worth & Salary</u></a></br><br><?php endif; ?>
<?php if(in_array('acteur', get_field('profession') )): ?>
<b>Must-read:</b></br>- <a href="http://muzul.com/the-10-richest-celebrities-in-hollywood-net-worth-salary/"><u>The 10 richest celebrities in Hollywood – Net worth & Salary</u></a></br><br><?php endif; ?>
<?php if( $nickname ): ?><b>Nickname:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Love life:</b> <?php echo $love_label; ?> - find out if he is on Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - find out if she is on Tinder?<?php else: ?><b><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Love history:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/celebrity/<?php echo $post->post_name;?>'>Full overview of <?php echo $celebname_label; ?>'s love life</a></br>
<br>
<b>Famous colleagues</b>: <?php echo $colleagues ?></br>
<br>
<b>Management:</b> <?php echo $management ?></br>
<br>
<b>Who does this <?php echo $starsign_label; ?>, age <?php echo $age ?>, hang out with?</b></br><br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h3>Family</h3></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Family Photo' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php echo $celebname_label; ?>'s family: names of father, mother, kids, brothers & sisters. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relation: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Friends</h3></div>
<br>
<?php if( have_rows('friends') ): ?>
<?php echo $celebname_label; ?>'s friends. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his<?php else: ?>her<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: 'inline-block' '>Know each other from: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Pets</h3></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?>'s pets and their names</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no pets listed.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Houses & Property</h3></div>
<br>
<?php if( have_rows('houses') ): ?>
Where does <?php echo $celebname_label; ?> live?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div>
<?php if(in_array('male', get_field('sex') )): ?>His <?php else: ?>Her<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: <?php echo $price; ?> million dollar • Includes: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Currently no houses listed.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Cars & Boats</h3></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>'s cars, boats and other vehicles. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> manufactured by <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Price: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Currently no cars listed.
<?php endif; ?>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<?php if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } ?>
<div id="taboola-below-article-thumbnails"></div>
<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({
    mode: 'thumbnails-a',
    container: 'taboola-below-article-thumbnails',
    placement: 'Below Article Thumbnails',
    target_type: 'mix'
  });
</script>
Recommended: </br><a rel='nofollow' target="_blank"  href="http://taddlr.com/10-incredible-celebrity-houses-cars/"><b><u>Most incredible celebrity houses & cars!</u></b></a></span></br><br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Look & Beauty tips</a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Clothing style & tips</a></b></br>
</br><br>
Something not right? Like the salary or net worth? Or do you wish to add a friend, house or a car? You can edit this yourself or share your suggestions in the comment form below.</br><br>
<i>Last updated: <?php the_modified_date(); ?></i>

<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> verdiende een salaris van <?php echo $salary ?> miljoen dollar, waardoor het vermogen <?php echo $net_worth ?> miljoen bedraagt in het jaar <?php echo date("Y"); ?>' "/></a>

<b><?php echo $celebname_label; ?> verdiende <?php if(in_array('male', get_field('sex') )): ?>zijn vermogen van<?php else: ?>haar vermogen van<?php endif; ?> <?php echo $net_worth ?> miljoen dollar<?php if( $endorsements ): ?>, naast met reclames voor merken zoals <?php echo $endorsements; ?>,<?php endif; ?> met <?php echo $claim_to_fame ?>. Maar waar geeft deze <?php echo $descript_label; ?> <?php echo $profession_label; ?> uit <?php echo $place_of_birth ?><?php if(in_array('male', get_field('sex') )): ?> zijn<?php else: ?> haar<?php endif; ?> geld aan uit? En wie zijn de vrienden en familie waar <?php if(in_array('male', get_field('sex') )): ?>hij<?php else: ?>zij<?php endif; ?> <?php if(in_array('male', get_field('sex') )): ?>zijn<?php else: ?>haar<?php endif; ?> vermogen aan spendeert?</b> 
<hr>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<h3>Persoonlijke feiten over <?php echo $celebname_label; ?> </br></h3>
<br>
<b>Geboorteplaats:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Geboortedatum:</b> <?php echo $birth_date ?></br>
<br>
<b>Geschatte vermogen:</b> <?php echo $net_worth ?> miljoen</br>
<br>
<?php if( $salary ): ?><b>Salaris:</b> <?php echo $salary ?> miljoen</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Bijnaam:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Liefdesleven:</b> <?php echo $love_label; ?> - ontdek of hij op Tinder zit?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - ontdek of zij op Tinder zit?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Ex-partners:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/nl/celebrity/<?php echo $post->post_name;?>'>Klik hier voor een volledig overzicht van <?php echo $celebname_label; ?>'s liefdesleven </a></br>
<br>
<b>Bekende collega’s</b>: <?php echo $colleagues ?></br>
<br>
<b>Management:</b> <?php echo $management ?></br>
<br>
<br>
<h2>Met wie brengt deze <?php echo $age ?>  jarige <?php echo $starsignnl_label; ?> allemaal tijd door?</h2></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h3>Familie</h3></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Familiefoto' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php echo $celebname_label; ?>'s familie: namen van ouders, kinderen, broers & zussen. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt=Foto van <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>zijn <?php else: ?>haar<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relatie: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Vrienden</h3></div>
<br>
<?php if( have_rows('friends') ): ?>
<?php echo $celebname_label; ?>'s vrienden. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt=Foto van <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>zijn<?php else: ?>haar<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Kennen elkaar van: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Huisdieren</h3></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?>’s huisdieren en bijbehorende namen:</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt=Foto van <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>zijn <?php else: ?>haar<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Momenteel geen huisdieren bekend.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Huizen & Landgoed</h3></div>
<br>
<?php if( have_rows('houses') ): ?>
Waar woont <?php echo $celebname_label; ?>?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Zijn <?php else: ?>Haar<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: <?php echo $price; ?> miljoen dollar • Inclusief: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Momenteel geen huizen bekend.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Auto's & Boten</h3></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>’s auto’s, boten en overige voertuigen. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> vervaardigd door <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prijs: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Momenteel geen auto’s, boten of overige voertuigen bekend.
<?php endif; ?>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Look & Schoonheidstips </a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Kledingstijl & tips</a></b></br>
</br><br>
Klopt er iets niet? Zoals het salaris of het vermogen? Of wil je graag iets toevoegen aan het profiel? Deel je suggesties door middel van een comment onderaan de pagina.</br><br>
<i>Laatste update: <?php the_modified_date(); ?></i>
<br></br>

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> verdiente <?php echo $salary ?> Millionen Dollar Gehalt und so entsprach der Nettowert <?php echo $net_worth ?> Millionen in <?php echo date("Y"); ?>'s ' "/></a>

<b><?php echo $celebname_label; ?> machte <?php if(in_array('male', get_field('sex') )): ?>sein <?php else: ?>ihr<?php endif; ?> <?php echo $net_worth ?> Millionen-Dollar-Vermögen mit <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, und  durch Marken wie <?php echo $endorsements; ?><?php endif; ?>. Aber wie gibt <?php if(in_array('male', get_field('sex') )): ?>dieser <?php else: ?>diese<?php endif; ?> <?php echo $descript_label; ?> <?php echo $profession_label; ?> aus <?php echo $place_of_birth ?> <?php if(in_array('male', get_field('sex') )): ?>sein <?php else: ?>ihr <?php endif; ?> Geld aus, und wer sind Freunde und Familie für die <?php if(in_array('male', get_field('sex') )): ?>er<?php else: ?>sie<?php endif; ?> es ausgibt?</b> 
</br><br>
<div style='float:left; margin-top:10px; padding-bottom: 7px; padding-top: 10px; padding-right: 5px; margin-right:5px;'><?php if (function_exists('adinj_print_ad')){ adinj_print_ad('random'); } ?></div>
<h3>Schnelle persönliche Fakten über <?php echo $celebname_label; ?> </br></h3>
<br>
<b>Herkunft:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Geburtsdatum:</b> <?php echo $birth_date ?></br>
<br>
<b>Geschätzter Nettowert:</b> <?php echo $net_worth ?> Millionen</br>
<br>
<?php if( $salary ): ?><b>Gehalt:</b> <?php echo $salary ?> Millionen</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Spitzname:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Liebesleben:</b> <?php echo $love_label; ?> - finde heraus, ob er auf Tinder ist?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - finde heraus, ob sie auf Tinder ist?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Liebesgeschichte:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/de/celebrity/<?php echo $post->post_name;?>'>Vollständiger Überblick über <?php echo $celebname_label; ?>'s Liebesleben</a></br>
<br>
<b>berühmte Kollegen</b>: <?php echo $colleagues ?></br>
<br>
<b>Management:</b> <?php echo $management ?></br>
<br>
<br>
<h2>Mit wem hängt dieser <?php echo $age ?> -jährige <?php echo $starsign_label; ?> ab?</h2></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h3>Familie</h3></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Familienfoto' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
<?php echo $celebname_label; ?>'s Familie: Namen von Vater, Mutter, Kinder, Brüder und Schwestern. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto von <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>seinen <?php else: ?>ihren<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Beziehung: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Freunden</h3></div>
<br>
<?php if( have_rows('friends') ): ?>
<?php echo $celebname_label; ?>'s Freunde. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto von <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>seinen<?php else: ?>ihren<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Kennen sich aus: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Haustieren</h3></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?>'s Haustiere und ihre Namen</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Foto von <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>seinen <?php else: ?>ihren<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Zur Zeit sind keine Haustiere aufgeführt.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Häuser & Eigentum</h3></div>
<br>
<?php if( have_rows('houses') ): ?>
Wo lebt <?php echo $celebname_label; ?> derzeit?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Sein <?php else: ?>Ihr <?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: <?php echo $price; ?> Millionen Dollar • Beinhaltet: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Zur Zeit sind keine Häuser aufgeführt.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Autos & Boote</h3></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>'s Autos, Boote und andere Fahrzeuge. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> hergestellt von <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Preis: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Zur Zeit sind keine Autos aufgeführt.
<?php endif; ?>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Tipps für Aussehen & Schönheit</a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Kleidungsstil & Tipps</a></b></br>
</br><br>
Etwas nicht in Ordnung? Wie z.B. das Gehalt oder der Nettowert? Oder wollen Sie einen Freund, ein Haus oder ein Auto hinzufügen? Sie können dies selbst bearbeiten oder Ihre Vorschläge im Kommentar-Formular unten teilen.</br><br>
<i>Zuletzt aktualisiert: <?php the_modified_date(); ?></i>



<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> earned a <?php echo $salary ?> million dollar salary, leaving the net worth at <?php echo $net_worth ?> million in <?php echo date("Y"); ?>'s ' "/></a>

<b><?php echo $celebname_label; ?> boucle <?php if(in_array('male', get_field('sex') )): ?>sa fortune de <?php else: ?>sa<?php endif; ?> <?php echo $net_worth ?> million de dollar avec <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, et en soutenant des marques comme <?php echo $endorsements; ?><?php endif; ?>. Mais comment ce personnage <?php echo $descript_label; ?> <?php echo $profession_label; ?> <?php echo $place_of_birth ?> dépense <?php if(in_array('male', get_field('sex') )): ?>son <?php else: ?>son<?php endif; ?> argent, et qui sont ses amis & sa famille <?php if(in_array('male', get_field('sex') )): ?> <?php else: ?> <?php endif; ?>  ?</b> 
</br><br>
<div style='float:left; margin-top:10px; padding-bottom: 7px; padding-top: 10px; padding-right: 5px; margin-right:5px;'><?php if (function_exists('adinj_print_ad')){ adinj_print_ad('random'); } ?></div>
<h3> Faits personnels en bref sur <?php echo $celebname_label; ?> </br></h3>
<br>
<b> Origine:</b> <?php echo $place_of_birth ?></br>
<br>
<b> Date de naissance:</b> <?php echo $birth_date ?></br>
<br>
<b> Valeur nette estimée:</b> <?php echo $net_worth ?> million</br>
<br>
<?php if( $salary ): ?><b> Salaire:</b> <?php echo $salary ?> million</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b> Pseudo:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b> Vie amoureuse:</b> <?php echo $love_label; ?> - savoir s’il est sur Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Love Life:</b> <?php echo $love_label; ?> - savoir si elle est sur Tinder?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b> Histoire d'amour:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/fr/celebrity/<?php echo $post->post_name;?>'> Vue d'ensemble de la vie amoureuse de <?php echo $celebname_label; ?> </a></br>
<br>
<b> Célèbres confrères </b>: <?php echo $colleagues ?></br>
<br>
<b> Gestion:</b> <?php echo $management ?></br>
<br>
<br>
<h2> Qui fait cela <?php echo $starsign_label; ?>, âge <?php echo $age ?>, passer du temps avec?</h2></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h3>Famille</h3></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt='Family Photo' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
Famille de <?php echo $celebname_label; ?> : les noms du père, mère, enfants, frères et soeurs. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relation: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Amis</h3></div>
<br>
<?php if( have_rows('friends') ): ?>
Les amis de <?php echo $celebname_label; ?> </br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his<?php else: ?>her<?php endif; ?> friend, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'> Se connaîssent mutuellement à travers: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Animaux de compagnie </h3></div>
<br>
<?php if( have_rows('pets') ): ?>
<?php echo $celebname_label; ?> Animaux de compagnie et leurs noms </br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Photo of <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>his <?php else: ?>her<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actuellement aucun animal listé.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Maisons & Propriété</h3></div>
<br>
<?php if( have_rows('houses') ): ?>
Où est ce que <?php echo $celebname_label; ?> vit?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Son<?php else: ?>Son<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $place; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: <?php echo $price; ?> million dollar • Comprend: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actuellement pas de maison listée.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Cars & Boats</h3></div>
<br>
<?php if( have_rows('car') ): ?>
<?php echo $celebname_label; ?>, ses voitures, bateaux et autres véhicules. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?><?php if( $brand ): ?> fabriqué par <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Prix: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Actuellement pas de voiture listée.
<?php endif; ?>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Conseils de Look & de Beauté </a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Conseils & style vestimentaire </a></b></br>
</br><br>
Quelque chose pas correcte ? Comme le salaire ou la valeur nette ? Ou vous souhaitez ajouter un ami, une maison ou une voiture ? Vous pouvez modifier vous-même ou partager vos suggestions dans le formulaire de commentaire ci-dessous.</br><br>
<i> Dernière mise à jour :  <?php the_modified_date(); ?></i>

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>
<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="160" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> recibió un salario de <?php echo $salary ?> millones de dólares, dejando un coste total de <?php echo $net_worth ?> millones en <?php echo date("Y"); ?> ' "/></a>

<b><?php echo $celebname_label; ?> hizo su fortuna de <?php echo $net_worth ?> millones de dólares con <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, y promocionando marcas como <?php echo $endorsements; ?><?php endif; ?>. ¿Pero como <?php if(in_array('male', get_field('sex') )): ?>este<?php else: ?>esta<?php endif; ?>  <?php echo $descript_label; ?> <?php echo $profession_label; ?> de <?php echo $place_of_birth ?> gastó su dinero, y quienes son los amigos y família con los cuales gastó?</b> 
</br><br>
<div style='float:left; margin-top:10px; padding-bottom: 7px; padding-top: 10px; padding-right: 5px; margin-right:5px;'><?php if (function_exists('adinj_print_ad')){ adinj_print_ad('random'); } ?></div>
<h3>Informaciónes personales rápidas sobre <?php echo $celebname_label; ?> </br></h3>
<br>
<b>Origen:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Fecha de nacimiento:</b> <?php echo $birth_date ?></br>
<br>
<b>Coste total estimado:</b> <?php echo $net_worth ?> millones</br>
<br>
<?php if( $salary ): ?><b>Salario:</b> <?php echo $salary ?> millones</br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Apodo:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descubrir si él está en Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descubrir si ella está enTinder?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Historial amoroso:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/celebrity/<?php echo $post->post_name;?>'>Resumen completo de la vida amorosa de <?php echo $celebname_label; ?></a></br>
<br>
<b>Colegas famosos</b>: <?php echo $colleagues ?></br>
<br>
<b>Dirección:</b> <?php echo $management ?></br>
<br>
<br>
<h2>¿Con quien <?php if(in_array('male', get_field('sex') )): ?>este<?php else: ?>esta<?php endif; ?> <?php echo $starsign_label; ?>, de <?php echo $age ?> años, pasa el rato?</h2></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h3>Família</h3></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt=Foto de Família' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
Família de <?php echo $celebname_label; ?>: nombres del padre, madre, hijos, hermanos y hermanas. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>su <?php else: ?>su<?php endif; ?> <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Relación: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Amigos</h3></div>
<br>
<?php if( have_rows('friends') ): ?>
Amigos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>su<?php else: ?>su<?php endif; ?> amigo, <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Se conocen de: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Mascotas</h3></div>
<br>
<?php if( have_rows('pets') ): ?>
Las mascotas de <?php echo $celebname_label; ?> y sus nombres</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Foto de <?php echo $celebname_label; ?> & <?php if(in_array('male', get_field('sex') )): ?>su <?php else: ?>su<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actualmente no hay mascotas listadas.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Casas y Propriedades </h3></div>
<br>
<?php if( have_rows('houses') ): ?>
Donde vive <?php echo $celebname_label; ?>?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> in <?php echo $place; ?>  ' "/></a></div><?php endif; ?>
<?php if(in_array('male', get_field('sex') )): ?>Su <?php else: ?>Su<?php endif; ?> <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> en <?php echo $place; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: <?php echo $price; ?> millones de dólares • Incluye: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Actualmente no hay casas listadas.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Carros y Barcos</h3></div>
<br>
<?php if( have_rows('car') ): ?>
Carros, barcos y otros veículos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> -  <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> fabricado por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descripción: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Color: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Precio: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Actualmente no hay carros listados.
<?php endif; ?>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Consejos de Apariencia y Belleza </a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Consejos y estilos de ropa </a></b></br>
</br><br>
¿Algo no está bien? ¿Como salario o coste total? ¿O quieres agregar un amigo, casa o carro? Usted mismo(a) puede editar esto o compartir sus sugerencias en el formulario de contacto abajo.</br><br>
<i>Última actualización: <?php the_modified_date(); ?></i>

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>

<a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="85" height="" border="0" align='right' hspace="10" alt='<?php echo $celebname_label; ?> recebeu um salário de <?php echo $salary ?> milhões de dólares, resultando em um patrimônio líquido de <?php echo $net_worth ?> milhões em <?php echo date("Y"); ?> ' "/></a>

<b><?php echo $celebname_label; ?> fez a sua fortuna de <?php echo $net_worth ?> milhões de dólares com <?php echo $claim_to_fame ?><?php if( $endorsements ): ?>, e promocionando marcas como <?php echo $endorsements; ?><?php endif; ?>. Mas como <?php if(in_array('male', get_field('sex') )): ?>este<?php else: ?>esta<?php endif; ?>  <?php echo $descript_label; ?> <?php echo $profession_label; ?> de <?php echo $place_of_birth ?> gastou seu dinheiro, e quem são os amigos e família com os quais gastou?</b> 
</br><br>
<div style='float:left; margin-top:10px; padding-bottom: 7px; padding-top: 10px; padding-right: 5px; margin-right:5px;'><?php if (function_exists('adinj_print_ad')){ adinj_print_ad('random'); } ?></div>
<h3>Dados pessoais rápidos sobre <?php echo $celebname_label; ?> </br></h3>
<br>
<b>Origem:</b> <?php echo $place_of_birth ?></br>
<br>
<b>Data de nascimiento:</b> <?php echo $birth_date ?></br>
<br>
<b>Patrimônio Líquido estimado:</b> <?php echo $net_worth ?> milhões</br>
<br>
<?php if( $salary ): ?><b>Salário:</b> <?php echo $salary ?> milhões </br>
<br><?php endif; ?>
<?php if( $nickname ): ?><b>Apelido:</b> <?php echo $nickname ?></br>
<br><?php endif; ?>
<?php if(in_array('vrijgezel_m', get_field('love') )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descobrir si ele está no Tinder?<?php elseif( (in_array('vrijgezel_f', get_field('love') ) )): ?><b>Vida amorosa:</b> <?php echo $love_label; ?> - descobrir se ela está no Tinder?<?php else: ?><?php echo $love_label; ?>:<?php endif; ?></b> <?php if( $current_partner_text ): ?><?php echo $current_partner_text ?><?php endif; ?></br>
<br>
<b>Histórico amoroso:</b> <?php echo $partners ?></br>
<br>
- <a rel='nofollow' href='http://taddlr.com/celebrity/<?php echo $post->post_name;?>'>Resumo completo da vida amorosa de <?php echo $celebname_label; ?></a></br>
<br>
<b>Colegas famosos</b>: <?php echo $colleagues ?></br>
<br>
<b>Direção:</b> <?php echo $management ?></br>
<br>
<br>
<h2>Com quem <?php if(in_array('male', get_field('sex') )): ?>este<?php else: ?>esta<?php endif; ?> <?php echo $starsign_label; ?>, de <?php echo $age ?> anos, passa seu tempo?</h2></br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:0px; background-color: #ffffff; border-color: #202F70;"><h3>Família</h3></div>
<a rel="nofollow" href="<?php echo $fam_photo?>"><img src="<?php echo $fam_photo ?>" rel='nofollow' width="300" height="" border="0" align='center' hspace="10" alt=Foto de Família' "/></a>
<br><br>
<?php if( have_rows('family') ): ?>
Família de <?php echo $celebname_label; ?>: nomes do pai, mãe, filhos, irmãos e irmãs. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('family') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display:inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> seu(sua) <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em>- <?php echo $name; ?></em>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Parentesco: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<br></br>
<?php endif; ?>

<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Amigos</h3></div>
<br>
<?php if( have_rows('friends') ): ?>
Amigos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('friends') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $meeting = get_sub_field('meeting');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$professionField = get_sub_field_object('profession');
							$professionValue = get_sub_field('profession');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='Foto de <?php echo $celebname_label; ?> e seu(sua) amigo(a), <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($professionValue) {

								foreach ($professionField['choices'] as $key => $value) {

									foreach ($professionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Se conhecem de: <?php echo $meeting; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<br></br>
<?php endif; ?>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Animais de estimação</h3></div>
<br>
<?php if( have_rows('pets') ): ?>
Os animais de estimação de <?php echo $celebname_label; ?> e seus nomes</br>
<br>
						<ul class="slides">

						<?php while( have_rows('pets') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="140" height="" border="0" hspace="10" display:'inline-block' alt='Foto de <?php echo $celebname_label; ?> e seu(sua) <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php echo $name; ?>' "/></a><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?>.</br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Atualmente não há animais de estimação listados.
<?php endif; ?>

		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Casas e Propriedades </h3></div>
<br>
<?php if( have_rows('houses') ): ?>
Onde vive <?php echo $celebname_label; ?>?</br>
<br>
						<ul class="slides">

						<?php while( have_rows('houses') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $place = get_sub_field('place');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$extraField = get_sub_field_object('extra');
							$extraValue = get_sub_field('extra');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $place; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' width="150" display:'inline-block' height="" border="0" align='left' hspace="10" alt='<?php echo $celebname_label; ?>'s <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> em <?php echo $place; ?>  ' "/></a></div><?php endif; ?> Seu (sua) <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> em <?php echo $place; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: <?php echo $price; ?> milhões de dólares • Inclui: <?php if ($extraValue) {

								foreach ($extraField['choices'] as $key => $value) {

									foreach ($extraValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " • ";
											
										endif;
									}
								}

							} ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>

<?php else: ?>Atualmente não há casas listadas.
<?php endif; ?>
		</br>

<br>
<div style="color: #333333; font-size: large; border-style: solid; padding: 7px; border-width:0px; border-bottom-width:1px; background-color: #ffffff; border-color: #202F70;"><h3>Carros e Barcos</h3></div>
<br>
<?php if( have_rows('car') ): ?>
Carros, barcos e outros veículos de <?php echo $celebname_label; ?>. </br>
<br>
						<ul class="slides">

						<?php while( have_rows('car') ): the_row(); 

							// vars
							$name = get_sub_field('name');
							$brand = get_sub_field('brand');
							$price = get_sub_field('price');
		                                        $photo = get_sub_field('photo');
                                                        $link = get_sub_field('link');
							
							$typeField = get_sub_field_object('type');
							$typeValue = get_sub_field('type');
							
							$colorField = get_sub_field_object('color');
							$colorValue = get_sub_field('color');

							$descriptionField = get_sub_field_object('description');
							$descriptionValue = get_sub_field('description');

							$materialField = get_field_object('material');
							$materialValue = get_field('material');

							?> 

							<li class="slide">


							
<br><div style="color: #ffffff; font-size: large; border-style: solid; padding: 7px; border-width:0px; background-color: #202F70; border-color: #202F70;"><h3><?php echo $name; ?></h3></div></br><br>
<?php if( $photo ): ?><div style='display: inline-block;'><a rel="nofollow" href="<?php echo $photo?>"><img src="<?php echo $photo ?>" rel='nofollow' display='inline-block' width="160" height="" border="0" align='left' hspace="10" alt='<?php echo $name; ?> ' "/></a></div><?php endif; ?>
<em><?php echo $name; ?></em> - <?php if ($typeValue) {

								foreach ($typeField['choices'] as $key => $value) {

									foreach ($typeValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo "";
											
										endif;
									}
								}

							} ?> <?php if( $brand ): ?> fabricado por <?php echo $brand; ?><?php endif; ?>.</br><br>
<br><div style='float:right; color:#ffffff; background-color: #202F70; border-style: solid; border-width:3px; border-color: #202F70; padding:4px; padding-left:7px; display: inline-block'>Descrição: <?php if ($descriptionValue) {

								foreach ($descriptionField['choices'] as $key => $value) {

									foreach ($descriptionValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Cor: <?php if ($colorValue) {

								foreach ($colorField['choices'] as $key => $value) {

									foreach ($colorValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											
										endif;
									}
								}

							} ?> - Preço: $<?php echo $price; ?></div></br><br>

<?php if ($descriptiValue) {

								foreach ($descriptiField['choices'] as $key => $value) {

									foreach ($descriptiValue as $labelValue) {
										
										if ($key == $labelValue):

											echo $value;
											echo " ";
											 
											
										endif; 
									}
								} 
							} 

endwhile; ?>
						</ul>
<?php else: ?>Atualmente não há carros listados.
<?php endif; ?>
</br>
<br>
- <b><a href='http://muzul.com/beauty/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Dicas de Aparência e Beleza </a></br>
- <a href='http://muzul.com/style/<?php echo $post->post_name;?>'><?php echo $celebname_label; ?> - Dicas e estilos de roupa </a></b></br>
</br><br>
Algo não está certo? Como o salário ou patrimônio líquido? Ou quer adicionar um amigo, casa ou carro? Você mesmo(a) pode editar isto ou compartilhar suas sugestões no formulário de contato abaixo.</br><br>
<i>Última atualização: <?php the_modified_date(); ?></i>


<?php endif; ?>
<br>

                   <!-- .entry-content -->


 <?php get_sidebar(); ?></div>


                    <?php
                    // Post Rating output at the bottom
                    if ( $ti_option['single_rating_box'] == 'rating_bottom' ) {
                        get_template_part( 'inc/single', 'rating' );
                    }
                    
                    // Show/Hide tags list
                    if ( $ti_option['single_tags_list'] == 1 ) {
                        the_tags('<div class="single-box tag-box clearfix"><h3 class="title">' . __( 'Tags', 'themetext' ) . '</h3>', '', '</div>'); 
                    }


                    
                    // Show/Hide share links
                    if ( $ti_option['single_social'] == 1 ) {
                        get_template_part( 'inc/single', 'share' );
                    }
                    
                    ?>

             


                    <?php if ( ! $single_sidebar || $single_sidebar == "post_sidebar_on" ) { // Enable/Disable post sidebar ?>
                    </div><!-- .grid-8 -->

                    <?php get_sidebar(); ?>
                </div><!-- .grids -->
                <?php }  ?>

            </div><!-- .wrapper -->

        </article>

    <?php endwhile; endif; ?>

    </main><!-- #content -->

    <?php
    // Show/Hide random posts slide dock
    if ( $ti_option['single_slide_dock'] == 1 ) {
        get_template_part( 'inc/slide', 'dock' );
    }
    ?>
    
<?php get_footer(); ?>