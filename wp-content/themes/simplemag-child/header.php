<?php
/**
 * The Header for the theme
 *
 * @package SimpleMag
 * @since 	SimpleMag 1.0
**/
?>
<!DOCTYPE html>
<!--[if lt IE 9]><html <?php language_attributes(); ?> class="oldie"><![endif]-->
<!--[if (gte IE 9) | !(IE)]><!--><html <?php language_attributes(); ?> class="modern"><!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>
 
<?php
						
$born_on = get_field('born_on');
$net_worth = get_field('net_worth');
$height = get_field('height');


                                                $hairstyle_field = get_field_object('hairstyle');
						$hairstyle_value = get_field('hairstyle');
						$hairstyle_label = '';

						if(count($hairstyle_value) < 2) {
						$hairstyle_label = $hairstyle_field['choices'][ $hairstyle_value[0] ];
						} else {
						foreach($hairstyle_value as $val) {
						$hairstyle_label .= $hairstyle_field['choices'][ $val ] . ', ';
						}
						}

                                                $celebname_field = get_field_object('celebname');
						$celebname_value = get_field('celebname');
						$celebname_label = $celebname_field['choices'][ $celebname_value ];

						$sex_field = get_field_object('sex');
						$sex_value = get_field('sex');
						$sex_label = $sex_field['choices'][ $sex_value[0] ];

?>
<?php if(is_home() || is_category() || is_tax() || is_search() || is_archive() || is_page() ) : ?>
<?php wp_title( '|', true, 'right' ); ?>
<?php elseif( get_post_meta($post->ID, "net_worth", true) ): ?>
<?php if(ICL_LANGUAGE_CODE=='en'): ?>

<?php echo $celebname_label; ?>: net worth, salary, house, car, family & friends - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<?php echo $celebname_label; ?>: inkomen, salaris, huis, auto, familie & vrienden - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<?php echo $celebname_label; ?>: Einkommen, Gehalt, Haus, Auto, Familie und Freunde - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<?php echo $celebname_label; ?>: fortune, salaire, maison, voiture, famille & amis - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>

<?php echo $celebname_label; ?>: Patrimonio, salario, casa, carro, família y amigos - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>

<?php echo $celebname_label; ?>: Patrimônio, salário, casa, carro, família e amigos - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='sv'): ?>

<?php the_title(); ?>: dejting, förmögenhet, rökning, tatuering, ursprung & längd 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='nb'): ?>

<?php the_title(); ?>: forhold, formue, røyking, tatovering & høyde 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='da'): ?>

<?php the_title(); ?>: forhold, indtægt, rygning, tatoveringer, højde 2015 - Taddlr DK

<?php elseif(ICL_LANGUAGE_CODE=='tr'): ?>

<?php the_title(); ?>:  Flört, Dövme, Köken, Boy, Kilo 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='pl'): ?>

<?php the_title(); ?>: Papierosy, Tatuaż, Wartość, Pochodzenie, Wzrost 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>

<?php the_title(); ?>:  amare, patrimonio, origini, fumare, tatuaggi & altezza 2015 - Taddlr
<?php endif; ?>
<?php elseif( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('male', get_field('sex') )): ?>
<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<?php echo $celebname_label; ?>: skin & hair type, styling products & tips - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<?php echo $celebname_label; ?>: huid- & haartype, styling & tips - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<?php echo $celebname_label; ?>: haut & Frisur, Styling-Produkte und Tipps - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<?php echo $celebname_label; ?>: type de peau & de cheveux, produits et conseils de style - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>

<?php echo $celebname_label; ?>: tipos de piel y cabello, productos y consejos de estilo - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>

<?php echo $celebname_label; ?>: tipos de pele e cabelo, produtos e dicas de estilo - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='sv'): ?>

<?php the_title(); ?>: dejting, förmögenhet, rökning, tatuering, ursprung & längd 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='nb'): ?>

<?php the_title(); ?>: forhold, formue, røyking, tatovering & høyde 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='da'): ?>

<?php the_title(); ?>: forhold, indtægt, rygning, tatoveringer, højde 2015 - Taddlr DK

<?php elseif(ICL_LANGUAGE_CODE=='tr'): ?>

<?php the_title(); ?>:  Flört, Dövme, Köken, Boy, Kilo 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='pl'): ?>

<?php the_title(); ?>: Papierosy, Tatuaż, Wartość, Pochodzenie, Wzrost 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>

<?php the_title(); ?>:  amare, patrimonio, origini, fumare, tatuaggi & altezza 2015 - Taddlr
<?php endif; ?>
<?php else: ?>

<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<?php echo $celebname_label; ?>: eye make-up, skin & hair type & styling products- 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<?php echo $celebname_label; ?>: oog make-up, huid- & haartype, styling & producten - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<?php echo $celebname_label; ?>: Augen-Make-up, Haut & Frisur, Styling & Produkte - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<?php echo $celebname_label; ?>: maquillage des yeux, type de peau et de cheveux, style & produits-2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>

<?php echo $celebname_label; ?>: maquillaje de ojos, tipos de piel y cabello, estilo y productos - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>

<?php echo $celebname_label; ?>: maquiagem de olhos, tipos de pele e cabelo, estilo e produtos - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='sv'): ?>

<?php the_title(); ?>: dejting, förmögenhet, rökning, tatuering, ursprung & längd 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='nb'): ?>

<?php the_title(); ?>: forhold, formue, røyking, tatovering & høyde 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='da'): ?>

<?php the_title(); ?>: forhold, indtægt, rygning, tatoveringer, højde 2015 - Taddlr DK

<?php elseif(ICL_LANGUAGE_CODE=='tr'): ?>

<?php the_title(); ?>:  Flört, Dövme, Köken, Boy, Kilo 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='pl'): ?>

<?php the_title(); ?>: Papierosy, Tatuaż, Wartość, Pochodzenie, Wzrost 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>

<?php the_title(); ?>:  amare, patrimonio, origini, fumare, tatuaggi & altezza 2015 - Taddlr
<?php endif; ?>

<?php endif; ?>

<?php elseif( get_post_meta($post->ID, "height", true) ): ?>

<?php if(ICL_LANGUAGE_CODE=='en'): ?>

<?php echo $celebname_label; ?>: clothing style, accessories, sizes & tips - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>

<?php echo $celebname_label; ?>: kledingstijl, accessoires, kledingmaten & tips - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>

<?php echo $celebname_label; ?>: Kleidungsstil, Zubehör, Größen & Tipps - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>

<?php echo $celebname_label; ?>: style de vêtements, accessoires, tailles & conseils - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>

<?php echo $celebname_label; ?>: estilo de ropa, accesorios, tamanhos y consejos - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>

<?php echo $celebname_label; ?>: Estilo de roupa, acessórios, tamanhos e dicas - 2015 Muzul

<?php elseif(ICL_LANGUAGE_CODE=='sv'): ?>

<?php the_title(); ?>: dejting, förmögenhet, rökning, tatuering, ursprung & längd 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='nb'): ?>

<?php the_title(); ?>: forhold, formue, røyking, tatovering & høyde 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='da'): ?>

<?php the_title(); ?>: forhold, indtægt, rygning, tatoveringer, højde 2015 - Taddlr DK

<?php elseif(ICL_LANGUAGE_CODE=='tr'): ?>

<?php the_title(); ?>:  Flört, Dövme, Köken, Boy, Kilo 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='pl'): ?>

<?php the_title(); ?>: Papierosy, Tatuaż, Wartość, Pochodzenie, Wzrost 2015 - Taddlr

<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>

<?php the_title(); ?>:  amare, patrimonio, origini, fumare, tatuaggi & altezza 2015 - Taddlr
<?php endif; ?>

<?php else: ?>
<?php wp_title( '|', true, 'right' ); ?>
<?php endif; ?></title>
<?php global $ti_option; ?>
<link rel="shortcut icon" href="<?php echo $ti_option['site_favicon']['url']; ?>" />
<link rel="apple-touch-icon-precomposed" href="<?php echo $ti_option['site_retina_favicon']['url']; ?>" />

<?php wp_head(); ?>
<meta name="google-site-verification" content="Xn6ztGUd0qc67bS70_oKsX4X9GatjO94dHeUsenf1JI" />

<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({article:'auto'});
  !function (e, f, u) {
    e.async = 1;
    e.src = u;
    f.parentNode.insertBefore(e, f);
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/mdk-muzul/loader.js');
</script>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

    <div id="pageslide" class="st-menu st-effect">
    	<a href="#" id="close-pageslide"><i class="icomoon-remove-sign"></i></a>
    </div><!-- Sidebar in Mobile View -->
    
	<?php
    // Check for a layout options: Full Width or  Boxed
    if ( $ti_option['site_layout'] == '2' ) { $site_layout = ' class="layout-boxed"'; } else { $site_layout = ' class="layout-full"'; } ?>
    <section id="site"<?php echo isset( $site_layout ) ? $site_layout : ''; ?>>
        <div class="site-content">
    
            <header id="masthead" role="banner" class="clearfix<?php if ( $ti_option['site_main_menu'] == true ) { echo ' with-menu'; } ti_top_strip_class(); ?>" itemscope itemtype="http://schema.org/WPHeader">
                
                <div class="no-print top-strip">
                    <div class="wrapper clearfix">
            
                        <?php 
                        // Hide Search and Social Icons if header variation with search is selected
                        if ( $ti_option['site_header'] != 'header_search' ) {
                            
                            // Search Form
                            get_search_form();
                        
                            // Social Profiles
                            if( $ti_option['top_social_profiles'] == 1 ) {
                                get_template_part ( 'inc/social', 'profiles' );
                            }
                        }
                        ?>
                        
                        <a href="#" id="open-pageslide" data-effect="st-effect"><i class="icomoon-menu"></i></a>
                        
                        <?php
                        // Pages Menu
                        if ( has_nav_menu( 'secondary_menu' ) ) :
                            echo '<nav class="secondary-menu" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">';
                            wp_nav_menu( array(
                                'theme_location' => 'secondary_menu',
                                'container' => false,
                            ));
                           echo '</nav>';
                         endif;
                        ?>
                    </div><!-- .wrapper -->
                </div><!-- .top-strip -->
                
                
                <div id="branding" class="animated">
                    <div class="wrapper">
                    <?php
                        /**
                         *  Header Variations
                        **/
                        
                        // Logo, Social Icons and Search
                        if ( $ti_option['site_header'] == 'header_search' ) {
                            get_template_part( 'inc/header', 'search' );
                        
                        // Logo and Ad unit
                        } elseif ( $ti_option['site_header'] == 'header_banner' ) {
                            get_template_part( 'inc/header', 'banner' );
                        
                        // Default - Centered Logo and Tagline
                        } else { 
                            get_template_part( 'inc/header', 'default' );
                        }
                    ?>
<center><div style='color: #8c919b; letter-spacing: 0.2em; font-size: 12px; font-weight: bold;'>
<?php if(ICL_LANGUAGE_CODE=='en'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Get a celebrity style - from their favorite jackets, dresses, skirts & jeans to shoes, jewelry & underwear. Learn from their measurements & tips.
<?php else: ?> 
Get a celebrity clothing style - from their favorite jackets, shirts & jeans to shoes, jewelry & underwear. Learn from their measurements & tips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Get a celebrity look - from mascara, eye liner & hair products to nail polish & fragrances. Learn from their beauty types & tips. 
<?php else: ?>
Get the celebrity look - from hair wax, moisturizer & hair products to fragrances. Learn from their beauty types & tips.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Celebrity homes - from their mother, father, kids & friends to their houses & cars, net worth & salary.
<?php endif; ?> 
<?php elseif(ICL_LANGUAGE_CODE=='nl'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Kleed je net zoals je favoriete beroemdheid – van jassen, jurken, rokjes & spijkerbroeken tot aan schoenen, sieraden & ondergoed. Kom meer te weten over kledingmaten & leer van fashion tips van beroemdheden.
<?php else: ?> 
Kleed je net zoals je favoriete beroemdheid – van jassen, t-shirts & spijkerbroeken tot aan schoenen, sieraden & ondergoed. Kom meer te weten over kledingmaten & leer van fashion tips van beroemdheden.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Krijg de look van een ster! – van mascara, eye liner & haarproducten tot aan nagellak & geurtjes. Leer van schoonheid types & tips van beroemdheden.
<?php else: ?>
Krijg de look van een ster! – van wax, gel, moisturizer & haarproducten tot aan geurtjes. Leer van schoonheid types & tips van beroemdheden.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Huizen van sterren – van de moeder, de vader, de kinderen & de vrienden tot aan de huizen & auto’s, het vermogen & salaris van je favoriete beroemdheid.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Holen Sie sich einen Promi-Stil - von ihren Lieblings-Jacken, Kleidern, Röcken und Jeans zu Schuhen, Schmuck und Unterwäsche. Lernen Sie von ihren Maßen & Tipps.
<?php else: ?> 
Holen Sie sich den Kleidungsstil eines Promis - von ihren Lieblings-Jacken, Hemden und Jeans zu Schuhen, Schmuck und Unterwäsche. Lernen Sie von ihren Maßen & Tipps.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Holen Sie sich einen Promi-Look - von Wimperntusche, Eyeliner & Haarkosmetik zu Nagellack & Düften. Lernen Sie von ihren Schönheitstypen & Tipps.
<?php else: ?>
Holen Sie sich den Promi-Look - von Haarwachs, Feuchtigkeitscreme & Haarkosmetik zu Düften. Lernen Sie von ihren Schönheitstypen & Tipps.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Promi-Häuser - von ihren Müttern, Vätern, Kindern und Freunden zu ihren Häusern und Autos, Nettowert & Gehalt.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='fr'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Obtenez un style de célébrité - depuis leur favoris vestes, 
 robes, jupes & jeans jusqu’aux chaussures, bijoux et sous-vêtements. S'inspirer de leurs mesures et conseils.
<?php else: ?> 
Obtenez un style de vêtements de célébrité - leurs vestes, chemises, jeans et chaussures préférés, leurs bijoux et sous-vêtements. S'inspirer de leurs mesures et conseils.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Obtenez un look de célébrité – mascara, eye-liner et produits capillaires  ainsi que vernis à ongles et parfums. S'inspirer de leurs types et conseils de beauté. 
<?php else: ?>
Obtenez le look de célébrité – depuis la crème pour cheveux, en passant par les crèmes hydratantes pour cheveux jusqu’aux parfums. S'inspirer de leurs types et conseils de beauté.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Maisons de célébrités - de leur mère, des père, des enfants et des amis à leurs maisons, valeur nette & voitures, salaire.
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='es'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Consigue el estilo de una celebridad - de sus chaquetas, vestidos, faldas y jeans a joyerías y ropa interior favoritos. Aprende de sus tamaños y consejos.
<?php else: ?> 
Consigue ele estilo de ropa de una celebridad - de sus chaquetas, camisas y jeans a zapatos, joyerías y ropa interior favoritos. Aprende de sus tamaños y consejos.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Consigue la apariencia de una celebridad - de rimel, delineador y productos de cabello a pinturas de uña y fragancias. Aprende de sus tipos y consejos de belleza. 
<?php else: ?>
Consigue la apariencia de una celebridad - de cera de cabello, hidratantes y productos de cabello a fragancias. Aprende de sus tipos y consejos de belleza.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Casas de celebridades - de su madre, padre, hijos y amigos a sus casas y carros, coste total y salario. 
<?php endif; ?>
<?php elseif(ICL_LANGUAGE_CODE=='pt-br'): ?>
<?php if( get_post_meta($post->ID, "height", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Consiga o estilo de roupa de uma celebridade - se duas jaquetas, vestidos, saias e jeans a jóias e roupa íntima favoritas. Aprenda de seus tamanhos e dicas.
<?php else: ?> 
Consiga o estilo de roupa de uma celebridade - se duas jaquetas, camisas, e jeans a sapatos, jóias e roupa íntima favoritas. Aprenda de seus tamanhos e dicas.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "hairstyle", true) ): ?>
<?php if(in_array('female', get_field('sex') )): ?>
Consiga a aparência de uma celebridade - de rímel, delineador e produtos de cabelo a pinturas de unha e fragrâncias. Aprenda de seus tipos e dicas de beleza 
<?php else: ?>
Consiga a aparência de uma celebridade - de cera de cabelo, hidratantes e produtos de cabelo a fragrâncias. Aprenda de seus tipos e dicas de beleza.
<?php endif; ?>
<?php endif; ?>
<?php if( get_post_meta($post->ID, "net_worth", true) ): ?>
Casas de celebridades - da mãe, pai, filhos e amigos a suas casas e carros, patrimônio líquido e salário.  
<?php endif; ?>
<?php endif; ?>
</div>
<?php if(is_home() || is_front_page() ) : ?>
<?php if (function_exists('adinj_print_ad')){ adinj_print_ad('top'); } ?>
<?php endif; ?></center>

                    </div><!-- .wrapper -->
                </div><!-- #branding -->
                
				<?php
                // Main Menu
                if ( $ti_option['site_main_menu'] == true ):
					if ( has_nav_menu( 'main_menu' ) ) :
					echo '<div class="no-print animated main-menu-container">';
						if ( $ti_option['site_fixed_menu'] == '3' && $ti_option['site_main_menu'] == true ):
							echo '<div class="main-menu-fixed">';
						endif;
							echo '<nav class="wrapper main-menu" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">';
								wp_nav_menu( array(
									'theme_location' => 'main_menu',
									'container' => false,
									'walker' => new TI_Menu()
								 ));
							echo '</nav>';
						if ( $ti_option['site_fixed_menu'] == '3' && $ti_option['site_main_menu'] == true ):
							echo '</div>';
						endif;
						echo '</div>';
					 else:
						echo '<div class="message warning"><i class="icomoon-warning-sign"></i>' . __( 'Define your site main menu', 'themetext' ) . '</div>';
					 endif;
                endif;
                ?>
            
            </header><!-- #masthead -->