<?php
/**
 * Plugin Name: Homepage Featured Post Widget
 */

add_action( 'widgets_init', 'mvp_feat_load_widgets' );

function mvp_feat_load_widgets() {
	register_widget( 'mvp_feat_widget' );
}

class mvp_feat_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function mvp_feat_widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'mvp_feat_widget', 'description' => __('A widget that displays a large post from a tag of your choice.', 'mvp_feat_widget') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'mvp_feat_widget' );

		/* Create the widget. */
		$this->__construct( 'mvp_feat_widget', __('DeVoe: Homepage Featured Post Widget', 'mvp_feat_widget'), $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		global $post;
		$title = apply_filters('widget_title', $instance['title'] );
		$tags = $instance['tags'];

		?>
		<div class="home-widget-wrap">
			<?php $recent = new WP_Query(array( 'tag' => $tags, 'posts_per_page' => '1' )); while($recent->have_posts()) : $recent->the_post(); ?>
			<div class="feat-wide-wrap left relative load-div">
				<a href="<?php the_permalink(); ?>" rel="bookmark">
				<div class="feat-wide-img left relative">
					<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
						<?php the_post_thumbnail(''); ?>
						<?php global $numpages; if(get_post_meta($post->ID, "mvp_video_embed", true)) { ?>
							<div class="feat-video">
								<i class="fa fa-video-camera fa-2"></i>
							</div><!--feat-video-->
						<?php } else if ( $numpages > 1 ) { ?>
							<div class="feat-gallery">
								<i class="fa fa-camera-retro fa-2"></i>
							</div><!--feat-gallery-->
						<?php } ?>
					<?php } ?>
				</div><!--feat-wide-img-->
				<div class="feat-wide-text">
					<?php if(get_post_meta($post->ID, "mvp_featured_headline", true)): ?>
						<h2><?php echo get_post_meta($post->ID, "mvp_featured_headline", true); ?></h2>
					<?php else: ?>
						<h2 class="standard-headline"><?php the_title(); ?></h2>
					<?php endif; ?>
					<p><?php echo excerpt(12); ?></p>
				</div><!--feat-wide-text-->
				</a>
			</div><!--feat-wide-wrap-->
			<?php endwhile; wp_reset_query(); ?>
		</div><!--home-widget-wrap-->

		<?php

	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['tags'] = strip_tags( $new_instance['tags'] );

		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Title');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Tag -->
		<p>
			<label for="<?php echo $this->get_field_id('tags'); ?>">Select tag:</label>
			<select id="<?php echo $this->get_field_id('tags'); ?>" name="<?php echo $this->get_field_name('tags'); ?>" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['tags']) echo 'selected="selected"'; ?>>Select a Tag</option>
				<?php $tags = get_tags('hide_empty=0'); ?>
				<?php foreach($tags as $tag) { ?>
				<option value='<?php echo $tag->slug; ?>' <?php if ($tag->slug == $instance['tags']) echo 'selected="selected"'; ?>><?php echo $tag->name; ?></option>
				<?php } ?>
			</select>
		</p>

	<?php
	}
}

?>