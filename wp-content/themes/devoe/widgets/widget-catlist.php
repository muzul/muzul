<?php
/**
 * Plugin Name: Homepage Tag List Widget
 */

add_action( 'widgets_init', 'mvp_catlist_load_widgets' );

function mvp_catlist_load_widgets() {
	register_widget( 'mvp_catlist_widget' );
}

class mvp_catlist_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function mvp_catlist_widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'mvp_catlist_widget', 'description' => __('A widget that displays a list of posts from a category of your choice.', 'mvp_catlist_widget') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'mvp_catlist_widget' );

		/* Create the widget. */
		$this->__construct( 'mvp_catlist_widget', __('DeVoe: Homepage Category List Widget', 'mvp_catlist_widget'), $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		global $post;
		$title = apply_filters('widget_title', $instance['title'] );
		$categories = $instance['categories'];
		$layout = $instance['layout'];
		$showcat = $instance['showcat'];
		$number = $instance['number'];
		$code = $instance['code'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		?>

		<div class="archive-list-wrap left relative">
			<?php if($code) { ?>
					<div class="archive-list-out">
						<div class="archive-list-in">
							<div class="archive-list-left left relative">
								<?php if ( $title ) { ?><h3 class="home-widget-head"><?php echo esc_html( $title ); ?></h3><?php } ?>
								<?php if($layout == 'column') { ?>
								<ul class="archive-col">
									<?php $recent = new WP_Query(array( 'cat' => $categories, 'posts_per_page' => $number )); while($recent->have_posts()) : $recent->the_post(); ?>
									<li>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php global $numpages; if(get_post_meta($post->ID, "mvp_video_embed", true)) { ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-2"></i>
													</div><!--feat-video-->
												<?php } else if ( $numpages > 1 ) { ?>
													<div class="feat-gallery">
														<i class="fa fa-camera-retro fa-2"></i>
													</div><!--feat-gallery-->
												<?php } ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-2"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<?php if($showcat) { ?>
													<div class="archive-list-info left relative">
														<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
													</div><!--archive-list-info-->
												<?php } ?>
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative w100">
												<?php if($showcat) { ?>
													<div class="archive-list-info left relative">
														<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
													</div><!--archive-list-info-->
												<?php } ?>
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; wp_reset_query(); ?>
								</ul>
								<?php } else { ?>
								<ul class="archive-list">
									<?php $recent = new WP_Query(array( 'cat' => $categories, 'posts_per_page' => $number )); while($recent->have_posts()) : $recent->the_post(); ?>
									<li>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php global $numpages; if(get_post_meta($post->ID, "mvp_video_embed", true)) { ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-2"></i>
													</div><!--feat-video-->
												<?php } else if ( $numpages > 1 ) { ?>
													<div class="feat-gallery">
														<i class="fa fa-camera-retro fa-2"></i>
													</div><!--feat-gallery-->
												<?php } ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-2"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<?php if($showcat) { ?><span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><?php } ?><span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative w100">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; wp_reset_query(); ?>
								</ul>
								<?php } ?>
							</div><!--archive-list-left-->
						</div><!--archive-list-in-->
						<div class="archive-ad-wrap relative">
							<?php echo html_entity_decode($code); ?>
						</div><!--archive-ad-wrap-->
					</div><!--archive-list-out-->
			<?php } else { ?>
							<div class="archive-list-left left relative">
								<?php if ( $title ) { ?><h3 class="home-widget-head"><?php echo esc_html( $title ); ?></h3><?php } ?>
								<?php if($layout == 'column') { ?>
								<ul class="archive-col">
									<?php $recent = new WP_Query(array( 'cat' => $categories, 'posts_per_page' => $number )); while($recent->have_posts()) : $recent->the_post(); ?>
									<li>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php global $numpages; if(get_post_meta($post->ID, "mvp_video_embed", true)) { ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-2"></i>
													</div><!--feat-video-->
												<?php } else if ( $numpages > 1 ) { ?>
													<div class="feat-gallery">
														<i class="fa fa-camera-retro fa-2"></i>
													</div><!--feat-gallery-->
												<?php } ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-2"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<?php if($showcat) { ?>
													<div class="archive-list-info left relative">
														<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
													</div><!--archive-list-info-->
												<?php } ?>
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative w100">
												<?php if($showcat) { ?>
													<div class="archive-list-info left relative">
														<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
													</div><!--archive-list-info-->
												<?php } ?>
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; wp_reset_query(); ?>
								</ul>
								<?php } else { ?>
								<ul class="archive-list">
									<?php $recent = new WP_Query(array( 'cat' => $categories, 'posts_per_page' => $number )); while($recent->have_posts()) : $recent->the_post(); ?>
									<li>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php global $numpages; if(get_post_meta($post->ID, "mvp_video_embed", true)) { ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-2"></i>
													</div><!--feat-video-->
												<?php } else if ( $numpages > 1 ) { ?>
													<div class="feat-gallery">
														<i class="fa fa-camera-retro fa-2"></i>
													</div><!--feat-gallery-->
												<?php } ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-2"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<?php if($showcat) { ?><span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><?php } ?><span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative w100">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; wp_reset_query(); ?>
								</ul>
								<?php } ?>
							</div><!--archive-list-left-->

			<?php } ?>
		</div><!--archive-list-wrap-->

		<?php

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['categories'] = strip_tags( $new_instance['categories'] );
		$instance['layout'] = strip_tags( $new_instance['layout'] );
		$instance['showcat'] = strip_tags( $new_instance['showcat'] );
		$instance['number'] = strip_tags( $new_instance['number'] );
		$instance['code'] = $new_instance['code'];

		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Title', 'showcat' => 'on', 'number' => 5, 'layout' => 'row');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:90%;" />
		</p>

		<!-- Category -->
		<p>
			<label for="<?php echo $this->get_field_id('categories'); ?>">Select category:</label>
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>All Categories</option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category) {  ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>

		<!-- Layout -->
		<p>
			<label for="<?php echo $this->get_field_id('layout'); ?>">Select layout:</label>
			<select id="<?php echo $this->get_field_id('layout'); ?>" name="<?php echo $this->get_field_name('layout'); ?>" style="width:100%;">
				<option value='row' <?php if ('row' == $instance['layout']) echo 'selected="selected"'; ?>>Row</option>
				<option value='column' <?php if ('column' == $instance['layout']) echo 'selected="selected"'; ?>>Column</option>
			</select>
		</p>

		<!-- Show Categories -->
		<p>
			<label for="<?php echo $this->get_field_id( 'showcat' ); ?>">Show categories on posts:</label>
			<input type="checkbox" id="<?php echo $this->get_field_id( 'showcat' ); ?>" name="<?php echo $this->get_field_name( 'showcat' ); ?>" <?php checked( (bool) $instance['showcat'], true ); ?> />
		</p>

		<!-- Number of posts -->
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>">Number of posts to display:</label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" size="3" />
		</p>

		<!-- Ad code -->
		<p>
			<label for="<?php echo $this->get_field_id( 'code' ); ?>">Ad code:</label>
			<textarea id="<?php echo $this->get_field_id( 'code' ); ?>" name="<?php echo $this->get_field_name( 'code' ); ?>" style="width:96%;" rows="6"><?php echo $instance['code']; ?></textarea>
		</p>

	<?php
	}
}

?>