<?php
/**
 * Plugin Name: Homepage Tag Row Widget
 */

add_action( 'widgets_init', 'mvp_tagrow_load_widgets' );

function mvp_tagrow_load_widgets() {
	register_widget( 'mvp_tagrow_widget' );
}

class mvp_tagrow_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function mvp_tagrow_widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'mvp_tagrow_widget', 'description' => __('A widget that displays a list of posts from a tag of your choice.', 'mvp_tagrow_widget') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'mvp_tagrow_widget' );

		/* Create the widget. */
		$this->__construct( 'mvp_tagrow_widget', __('DeVoe: Homepage Tag Row Widget', 'mvp_tagrow_widget'), $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		global $post;
		$title = apply_filters('widget_title', $instance['title'] );
		$tags = $instance['tags'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		?>

			<div class="horz-list-wrap left relative">
				<div class="horz-list-head left relative">
					<h3><?php echo esc_html( $title ); ?></h3>
				</div><!--horz-list-head-->
				<ul class="horz-list left relative">
						<?php $recent = new WP_Query(array( 'tag' => $tags, 'posts_per_page' => 4 )); while($recent->have_posts()) : $recent->the_post(); ?>
						<li>
							<a href="<?php the_permalink(); ?>" rel="bookmark">
							<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
								<div class="horz-list-img left relative">
									<?php the_post_thumbnail('medium-thumb'); ?>
									<?php global $numpages; if(get_post_meta($post->ID, "mvp_video_embed", true)) { ?>
										<div class="feat-video">
											<i class="fa fa-video-camera fa-2"></i>
										</div><!--feat-video-->
									<?php } else if ( $numpages > 1 ) { ?>
										<div class="feat-gallery">
											<i class="fa fa-camera-retro fa-2"></i>
										</div><!--feat-gallery-->
									<?php } ?>
								</div><!--horz-list-img-->
							<?php } ?>
							<div class="horz-list-text left relative">
								<h2><?php the_title(); ?></h2>
							</div><!--horz-list-text-->
							</a>
						</li>
						<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!--horz-list-wrap-->

		<?php

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['tags'] = strip_tags( $new_instance['tags'] );

		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Title');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:90%;" />
		</p>

		<!-- Tag -->
		<p>
			<label for="<?php echo $this->get_field_id('tags'); ?>">Select tag:</label>
			<select id="<?php echo $this->get_field_id('tags'); ?>" name="<?php echo $this->get_field_name('tags'); ?>" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['tags']) echo 'selected="selected"'; ?>>Select a Tag</option>
				<?php $tags = get_tags('hide_empty=0'); ?>
				<?php foreach($tags as $tag) { ?>
				<option value='<?php echo $tag->slug; ?>' <?php if ($tag->slug == $instance['tags']) echo 'selected="selected"'; ?>><?php echo $tag->name; ?></option>
				<?php } ?>
			</select>
		</p>


	<?php
	}
}

?>