<?php

	/* Template Name: Full Width */

?>
<?php get_header(); ?>
<div class="sec-marg-out relative">
	<div class="sec-marg-in">
		<div id="main-content-contain" class="left relative infinite-content">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div id="content-wrap" class="left relative"  itemscope itemtype="http://schema.org/Article">
					<article <?php post_class(); ?>>
						<div id="post-header" class="left relative">
							<h1 class="post-title left" itemprop="name headline"><?php the_title(); ?></h1>
						</div><!--post-header-->
						<div id="post-area" class="left relative">
							<div class="post-body-out">
								<div class="post-info-left left relative">
									<div class="post-info-left-top left relative">
										<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
											<?php mvp_share_count(); ?>
										<?php } ?>
									</div><!--post-info-left-top-->
									<div class="post-info-left-bot left relative">
									<?php $mvp_social_page = get_option('mvp_social_page'); if ($mvp_social_page == "true") { ?>
								<div class="post-soc-out">
									<div class="post-soc-in">
										<div class="post-soc-share left relative">
											<a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><div class="post-soc-fb left relative"><i class="fa fa-facebook fa-2"></i><span class="post-share-text"><?php _e( 'Share', 'mvp-text' ); ?></span></div><!--post-soc-fb--></a>
											<a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?> -&url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><div class="post-soc-twit left relative"><i class="fa fa-twitter fa-2"></i><span class="post-share-text"><?php _e( 'Tweet', 'mvp-text' ); ?></span></div><!--post-soc-twit--></a>
											<a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><div class="post-soc-pin left relative"><i class="fa fa-pinterest fa-2"></i><span class="post-share-text"><?php _e( 'Pin', 'mvp-text' ); ?></span></div><!--post-soc-pin--></a>
										</div><!--post-soc-share-->
									</div><!--post-soc-in-->
								</div><!--post-soc-out-->
									<?php } ?>
									</div><!--post-info-left-bot-->
								</div><!--post-info-left-->
								<div class="post-body-in">
									<div id="content-area" class="left relative" itemprop="articleBody">
										<div class="content-main left relative">
											<?php the_content(); ?>
											<?php wp_link_pages(); ?>
											<?php $mvp_comments_page = get_option('mvp_comments_page'); if ($mvp_comments_page == "true") { ?>
												<?php if ( comments_open() ) { ?>
													<?php comments_template(); ?>
												<?php } ?>
											<?php } ?>
										</div><!--content-main-->
									</div><!--content-area-->
								</div><!--post-body-in-->
							</div><!--post-body-out-->
						</div><!--post-area-->
					</article>
				</div><!--content-wrap-->
			<?php endwhile; endif; ?>
		</div><!--main-content-contain-->
	</div><!--sec-marg-in-->
</div><!--sec-marg-out-->
<?php get_footer(); ?>