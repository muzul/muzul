jQuery(document).ready(function($) {
"use strict";

  	// Sticky Top Bar

	var aboveHeight = $('#main-logo-wrap').outerHeight();
	    $(window).scroll(function(){
	    	if ($(window).scrollTop() > aboveHeight){
	    	$('#main-nav-wrap').addClass('main-nav-drop');
	    	$('.fly-bottom-soc').addClass('to-top-trans');
	    	$('.fly-to-top').addClass('to-top-trans');
	    	$('.fly-soc-head').addClass('to-top-trans');
	    	} else {
	    	$('#main-nav-wrap').removeClass('main-nav-drop');
	    	$('.fly-bottom-soc').removeClass('to-top-trans');
	    	$('.fly-to-top').removeClass('to-top-trans');
	    	$('.fly-soc-head').removeClass('to-top-trans');
	    	}
		});


  	// Fly-Out Navigation

	$(".fly-but-wrap").click(function(){
  		$("#fly-wrap").toggleClass("fly-menu");
  		$("#site-fixed").toggleClass("fly-content");
  		$(".fly-but-wrap").toggleClass("fly-open");

	});

	var doc = document.documentElement;
	doc.setAttribute('data-useragent', navigator.userAgent);



});