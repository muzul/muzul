<?php get_header(); ?>
<div class="sec-marg-out relative">
	<div class="sec-marg-in">
		<div id="main-content-contain" class="left relative infinite-content">
			<div id="content-wrap" class="left relative">
				<div id="post-area" class="left relative">
					<div id="content-area" class="left relative">
						<div class="content-area-out">
							<div class="content-area-in">
								<div class="content-main left relative">
									<?php if(is_single()) { if (have_posts()) : while (have_posts()) : the_post(); ?>
										<?php woocommerce_breadcrumb(); ?>
									<?php endwhile; endif; } else { ?>
										<?php woocommerce_breadcrumb(); ?>
									<?php } ?>
									<div id="woo-content">
										<?php woocommerce_content(); ?>
										<?php wp_link_pages(); ?>
									</div><!--woo-content-->
								</div><!--content-main-->
							</div><!--content-area-in-->
							<?php get_sidebar('woo'); ?>
						</div><!--content-area-out-->
					</div><!--content-area-->
				</div><!--post-area-->
			</div><!--content-wrap-->
		</div><!--main-content-contain-->
	</div><!--sec-marg-in-->
</div><!--sec-marg-out-->
<?php get_footer(); ?>