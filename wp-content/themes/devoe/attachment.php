<?php get_header(); ?>
<div class="sec-marg-out relative">
	<div class="sec-marg-in">
		<div id="main-content-contain" class="left relative infinite-content">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div id="content-wrap" class="left relative"  itemscope itemtype="http://schema.org/Article">
					<article <?php post_class(); ?>>
						<div id="post-header" class="left relative">
							<div class="post-info-top left relative">
								<span class="post-date"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>" pubdate><?php the_time(get_option('date_format')); ?></time></span>
							</div><!--post-info-top-->
							<h1 class="post-title left" itemprop="name headline"><?php the_title(); ?></h1>
							<?php if ( has_excerpt() ) { ?>
								<span class="post-excerpt"><?php the_excerpt(); ?></span>
							<?php } ?>
						</div><!--post-header-->
						<div id="post-area" class="left relative">
							<div class="post-body-out">
								<?php $mvp_author = get_option('mvp_author_box'); $mvp_social_box = get_option('mvp_social_box'); if ( $mvp_social_box == "false" && $mvp_author == "false" ) { } else { ?>
								<div class="post-info-left left relative">
									<div class="post-info-left-bot left relative">
									<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
								<div class="post-soc-out">
									<div class="post-soc-in">
										<div class="post-soc-share left relative">
											<a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><div class="post-soc-fb left relative"><i class="fa fa-facebook fa-2"></i><span class="post-share-text"><?php _e( 'Share', 'mvp-text' ); ?></span></div><!--post-soc-fb--></a>
											<a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?> -&url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><div class="post-soc-twit left relative"><i class="fa fa-twitter fa-2"></i><span class="post-share-text"><?php _e( 'Tweet', 'mvp-text' ); ?></span></div><!--post-soc-twit--></a>
											<a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><div class="post-soc-pin left relative"><i class="fa fa-pinterest fa-2"></i><span class="post-share-text"><?php _e( 'Pin', 'mvp-text' ); ?></span></div><!--post-soc-pin--></a>
										</div><!--post-soc-share-->
									</div><!--post-soc-in-->
								</div><!--post-soc-out-->
									<?php } ?>
									</div><!--post-info-left-bot-->
								</div><!--post-info-left-->
								<?php } ?>
								<div class="post-body-in">
									<div id="content-area" class="left relative" itemprop="articleBody">
										<div class="content-area-out">
											<div class="content-area-in">
												<div class="content-main left relative">
  													<?php if ( wp_attachment_is_image( $post->id ) ) : $att_image = wp_get_attachment_image_src( $post->id, "post"); ?>
														<a href="<?php echo wp_get_attachment_url($post->id); ?>" title="<?php the_title(); ?>" rel="attachment"><img src="<?php echo esc_url( $att_image[0] );?>" class="attachment-post" alt="<?php the_title(); ?>" /></a>
													<?php else : ?>
														<a href="<?php echo wp_get_attachment_url($post->ID) ?>" title="<?php echo esc_html( get_the_title($post->ID), 1 ) ?>" rel="attachment"><?php echo basename($post->guid) ?></a>
													<?php endif; ?>
												</div><!--content-main-->
											</div><!--content-area-in-->
											<?php get_sidebar(); ?>
										</div><!--content-area-out-->
									</div><!--content-area-->
								</div><!--post-body-in-->
							</div><!--post-body-out-->
						</div><!--post-area-->
					</article>
				</div><!--content-wrap-->
			<?php endwhile; endif; ?>
		</div><!--main-content-contain-->
	</div><!--sec-marg-in-->
</div><!--sec-marg-out-->
<?php get_footer(); ?>