<div class="feat-trend-wrap left relative">
	<ul class="feat-trend-list">
		<li class="trend-head"><?php echo esc_html(get_option('mvp_trend_head')); ?></li>
		<?php global $post; $tmp_post = $post; $pop_num = esc_html(get_option('mvp_trend_days')); $popular_days_ago = "$pop_num days ago"; $trend_query = get_posts(array( 'posts_per_page' => '10', 'orderby' => 'meta_value_num', 'order' => 'DESC', 'meta_key' => 'post_views_count', 'date_query' => array( array( 'after' => $popular_days_ago )) )); foreach( $trend_query as $post ) : setup_postdata($post); ?>
			<li class="trend-item">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</li>
		<?php endforeach; wp_reset_postdata(); $post = $tmp_post; ?>
	</ul>
</div><!--feat-trend-wrap-->