<?php get_header(); ?>
<?php global $author; $userdata = get_userdata($author); ?>
<div id="main-content-contain" class="left relative">
	<div class="sec-marg-out relative">
		<div class="sec-marg-in">
			<div class="home-widget-wrap">
				<div class="archive-list-wrap left relative">
					<div class="archive-list-out">
						<div class="archive-list-in">
							<div class="archive-list-left left relative">
								<div id="author-page-box" class="left relative">
									<div class="author-page-out">
										<div class="author-page-img left relative">
											<?php echo get_avatar( $userdata->user_email, '120' ); ?>
										</div><!--author-page-img-->
										<div class="author-page-in">
											<div class="author-page-text left relative">
												<h1 class="cat-head"><?php echo esc_html( $userdata->display_name ); ?></h1>
												<p><?php echo esc_html( $userdata->description ); ?></p>
												<ul class="author-social left relative">
													<?php $authordesc = $userdata->facebook; if ( ! empty ( $authordesc ) ) { ?>
														<li class="fb-item">
															<a href="<?php echo esc_url( $userdata->facebook); ?>" alt="Facebook" class="fb-but" target="_blank"><i class="fa fa-facebook-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $userdata->twitter; if ( ! empty ( $authordesc ) ) { ?>
														<li class="twitter-item">
															<a href="<?php echo esc_url($userdata->twitter); ?>" alt="Twitter" class="twitter-but" target="_blank"><i class="fa fa-twitter-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $userdata->pinterest; if ( ! empty ( $authordesc ) ) { ?>
														<li class="pinterest-item">
															<a href="<?php echo esc_url($userdata->pinterest); ?>" alt="Pinterest" class="pinterest-but" target="_blank"><i class="fa fa-pinterest-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $userdata->googleplus; if ( ! empty ( $authordesc ) ) { ?>
														<li class="google-item">
															<a href="<?php echo esc_url($userdata->googleplus); ?>" alt="Google Plus" class="google-but" target="_blank"><i class="fa fa-google-plus-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $userdata->instagram; if ( ! empty ( $authordesc ) ) { ?>
														<li class="instagram-item">
															<a href="<?php echo esc_url($userdata->instagram); ?>" alt="Instagram" class="instagram-but" target="_blank"><i class="fa fa-instagram fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $userdata->linkedin; if ( ! empty ( $authordesc ) ) { ?>
														<li class="linkedin-item">
															<a href="<?php echo esc_url($userdata->linkedin); ?>" alt="Linkedin" class="linkedin-but" target="_blank"><i class="fa fa-linkedin-square fa-2"></i></a>
														</li>
													<?php } ?>
												</ul>
											</div><!--author-page-text-->
										</div><!--author-page-in-->
									</div><!--author-page-out-->
								</div><!--author-page-box-->
								<?php if(get_option('mvp_arch_layout') == 'Column' ) { ?>
								<ul class="archive-col infinite-content">
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
									<li class="infinite-post">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-3"></i>
													</div><!--feat-video-->
												<?php endif; ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-3"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<div class="archive-list-info left relative">
													<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
												</div><!--archive-list-info-->
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative w100">
												<div class="archive-list-info left relative">
													<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
												</div><!--archive-list-info-->
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; endif; ?>
									<div class="nav-links">
										<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
									</div><!--nav-links-->
								</ul>
								<?php } else { ?>
								<ul class="archive-list infinite-content">
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
									<li class="infinite-post">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-3"></i>
													</div><!--feat-video-->
												<?php endif; ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-3"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative w100">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; endif; ?>
									<div class="nav-links">
										<?php if (function_exists("pagination")) { pagination($wp_query->max_num_pages); } ?>
									</div><!--nav-links-->
								</ul>
								<?php } ?>
							</div><!--archive-list-left-->
						</div><!--archive-list-in-->
						<?php get_sidebar(); ?>
					</div><!--archive-list-out-->
				</div><!--archive-list-wrap-->
			</div><!--home-widget-wrap-->
		</div><!--sec-marg-in-->
	</div><!--sec-marg-out-->
</div><!--main-content-content-->
<?php get_footer(); ?>