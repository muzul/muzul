<div class="fly-but-wrap">
	<span></span>
	<span></span>
	<span></span>
	<span></span>
</div><!--fly-but-wrap-->
<div id="fly-wrap">
		<div class="fly-bottom-wrap">
			<span class="fly-soc-head left relative"><?php _e( 'Connect', 'mvp-text' ); ?></span>
			<ul class="fly-bottom-soc left relative">
				<?php if(get_option('mvp_facebook')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_facebook')); ?>" alt="Facebook" target="_blank">
						<li class="fb-soc">
							<i class="fa fa-facebook-square fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_twitter')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_twitter')); ?>" alt="Twitter" target="_blank">
						<li class="twit-soc">
							<i class="fa fa-twitter fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_pinterest')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_pinterest')); ?>" alt="Pinterest" target="_blank">
						<li class="pin-soc">
							<i class="fa fa-pinterest fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_instagram')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_instagram')); ?>" alt="Instagram" target="_blank">
						<li class="inst-soc">
							<i class="fa fa-instagram fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_google')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_google')); ?>" alt="Google Plus" target="_blank">
						<li class="goog-soc">
							<i class="fa fa-google-plus fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_youtube')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_youtube')); ?>" alt="Youtube" target="_blank">
						<li class="yt-soc">
							<i class="fa fa-youtube-play fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_linkedin')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_linkedin')); ?>" alt="LinkedIn" target="_blank">
						<li class="link-soc">
							<i class="fa fa-linkedin fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_tumblr')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_tumblr')); ?>" alt="Tumblr" target="_blank">
						<li class="tum-soc">
							<i class="fa fa-tumblr fa-2"></i>
						</li>
					</a>
				<?php } ?>
				<?php if(get_option('mvp_rss')) { ?>
					<a href="<?php echo esc_url(get_option('mvp_rss')); ?>" target="_blank">
					<li class="rss-soc">
						<i class="fa fa-rss fa-2"></i>
					</li>
				</a>
				<?php } else { ?>
					<a href="<?php bloginfo('rss_url'); ?>" target="_blank">
						<li class="rss-soc">
							<i class="fa fa-rss fa-2"></i>
						</li>
					</a>
				<?php } ?>
			</ul>
			<div class="fly-to-top back-to-top left relative">
				<i class="fa fa-angle-up fa-3"></i>
				<span class="to-top-text"><?php _e( 'To Top', 'mvp-text' ); ?></span>
			</div><!--fly-to-top-->
		</div><!--fly-bottom-wrap-->
		<div class="fly-search-out">
			<div class="fly-search-in">
				<div class="fly-but-search left relative">
					<?php get_search_form(); ?>
				</div><!--fly-but-search-->
			</div><!--fly-search-in-->
		</div><!--fly-search-out-->
		<div id="fly-menu-wrap">
				<div class="fly-menu-out">
					<div class="fly-menu-in">
						<nav class="main-menu left relative">
							<?php wp_nav_menu(array('theme_location' => 'main-menu')); ?>
						</nav>
					</div><!--fly-menu-in-->
				</div><!--fly-menu-out-->
		</div><!--fly-menu-wrap-->
</div><!--fly-wrap-->