<?php get_header(); ?>
<div id="main-content-contain" class="left relative infinite-content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div id="content-wrap" class="left relative"  itemscope itemtype="http://schema.org/Article">
<div class="sec-marg-out4 relative">
	<div class="sec-marg-in4">
					<article <?php post_class(); ?>>
						<div id="post-header" class="left relative">
							<div class="sec-marg-out relative">
								<div class="sec-marg-in">
									<div class="post-info-top left relative">
										<span class="post-cat"><a href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></a></span><span class="post-date"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>" pubdate><?php the_time(get_option('date_format')); ?></time></span>
									</div><!--post-info-top-->
									<h1 class="post-title left" itemprop="name headline"><?php the_title(); ?></h1>
									<?php if ( has_excerpt() ) { ?>
										<span class="post-excerpt"><?php the_excerpt(); ?></span>
									<?php } ?>
								</div><!--sec-marg-in-->
							</div><!--sec-marg-out-->
						</div><!--post-header-->
						<div id="post-area" class="left relative">
							<div class="post-body-out">
								<?php $mvp_author = get_option('mvp_author_box'); $mvp_social_box = get_option('mvp_social_box'); if ( $mvp_social_box == "false" && $mvp_author == "false" ) { } else { ?>
								<div class="post-info-left left relative">
									<div class="sec-marg-out relative">
										<div class="sec-marg-in">
									<div class="post-info-left-top left relative">
										<?php $mvp_author = get_option('mvp_author_box'); if ($mvp_author == "true") { ?>
											<div class="author-info left relative">
												<div class="author-img left relative">
													<?php echo get_avatar( get_the_author_meta('email'), '120' ); ?>
												</div><!--author-img-->
												<span class="author-name vcard fn" itemprop="author"><?php the_author_posts_link(); ?></span>
											</div><!--author-info-->
										<?php } ?>
										<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
											<?php mvp_share_count(); ?>
										<?php } ?>
									</div><!--post-info-left-top-->
									<div class="post-info-left-bot left relative">
									<?php $mvp_social_box = get_option('mvp_social_box'); if ($mvp_social_box == "true") { ?>
								<div class="post-soc-out">
									<div class="post-soc-in">
										<div class="post-soc-share left relative">
											<div class="soc-count-mobi">
												<?php mvp_share_count(); ?>
											</div><!--soc-count-mobi-->
											<a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><div class="post-soc-fb left relative"><i class="fa fa-facebook fa-2"></i><span class="post-share-text"><?php _e( 'Share', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $fb_share = get_fb( $post_id ); if($fb_share==0) { } else { echo esc_html( $fb_share ); } ?></span></div><!--post-soc-fb--></a>
											<a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?> -&url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><div class="post-soc-twit left relative"><i class="fa fa-twitter fa-2"></i><span class="post-share-text"><?php _e( 'Tweet', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $twit_share = get_tweets( $post_id ); if($twit_share==0) { } else { echo esc_html( $twit_share ); } ?></span></div><!--post-soc-twit--></a>
											<a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><div class="post-soc-pin left relative"><i class="fa fa-pinterest fa-2"></i><span class="post-share-text"><?php _e( 'Pin', 'mvp-text' ); ?></span><span class="post-share-text"><?php $post_id = get_the_ID(); $pin_share = get_pinterest( $post_id ); if($pin_share==0) { } else { echo esc_html( $pin_share ); } ?></span></div><!--post-soc-pin--></a>
										</div><!--post-soc-share-->
									</div><!--post-soc-in-->
								</div><!--post-soc-out-->
									<?php } ?>
									<?php $prev_next = get_option('mvp_prev_next'); if ($prev_next == "true") { ?>
										<div class="prev-next-posts left relative">
											<?php mvp_prev_next_links(); ?>
										</div><!--prev-next-posts-->
									<?php } ?>
											</div><!--post-info-left-bot-->
										</div><!--sec-marg-in-->
									</div><!--sec-marg-out-->
								</div><!--post-info-left-->
								<?php } ?>
								<div class="post-body-in">
									<div id="content-area" class="left relative" itemprop="articleBody">
										<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "wide" || $mvp_post_temp == "wide-full" ) { } else { ?>
										<?php $mvp_featured_img = get_option('mvp_featured_img'); if ($mvp_featured_img == "true") { ?>
						<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
							<div id="video-embed" class="left relative sec-feat">
								<?php echo get_post_meta($post->ID, "mvp_video_embed", true); ?>
							</div><!--video-embed-->
						<?php else: ?>
							<?php $mvp_show_hide = get_post_meta($post->ID, "mvp_featured_image", true); if ($mvp_show_hide == "hide") { ?>
							<?php } else { ?>
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div id="featured-image" class="left relative sec-feat">
										<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb', array( 'class' => 'unlazy' ) ); ?>
										<img itemprop="image" src="<?php echo $thumb['0']; ?>" />
										<?php if(get_post_meta($post->ID, "mvp_photo_credit", true)): ?>
											<div id="featured-caption">
												<?php echo get_post_meta($post->ID, "mvp_photo_credit", true); ?>
											</div><!--featured-caption-->
										<?php endif; ?>
									</div><!--featured-image-->
								<?php } ?>
							<?php } ?>
						<?php endif; ?>
										<?php } ?>
										<?php } ?>
									<div class="content-area-cont left relative">
										<div class="sec-marg-out relative">
											<div class="sec-marg-in">
										<div class="content-area-out">
											<div class="content-area-in">
												<div class="content-main left relative">
													<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "wide" || $mvp_post_temp == "wide-full" ) { } else { ?>
										<?php $mvp_featured_img = get_option('mvp_featured_img'); if ($mvp_featured_img == "true") { ?>
						<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
							<div id="video-embed" class="left relative prim-feat">
								<?php echo get_post_meta($post->ID, "mvp_video_embed", true); ?>
							</div><!--video-embed-->
						<?php else: ?>
							<?php $mvp_show_hide = get_post_meta($post->ID, "mvp_featured_image", true); if ($mvp_show_hide == "hide") { ?>
							<?php } else { ?>
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div id="featured-image" class="left relative prim-feat">
										<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb', array( 'class' => 'unlazy' ) ); ?>
										<img itemprop="image" src="<?php echo $thumb['0']; ?>" />
										<?php if(get_post_meta($post->ID, "mvp_photo_credit", true)): ?>
											<div id="featured-caption">
												<?php echo get_post_meta($post->ID, "mvp_photo_credit", true); ?>
											</div><!--featured-caption-->
										<?php endif; ?>
									</div><!--featured-image-->
								<?php } ?>
							<?php } ?>
						<?php endif; ?>
										<?php } ?>
										<?php } ?>
													<?php the_content(); ?>
													<?php wp_link_pages(); ?>
													<div class="posts-nav-link">
														<?php posts_nav_link(); ?>
													</div><!--posts-nav-link-->
													<div class="post-tags">
														<span class="post-tags-header"><?php _e( 'Related Items', 'mvp-text' ); ?></span><span itemprop="keywords"><?php the_tags('','','') ?></span>
													</div><!--post-tags-->
													<?php if(get_option('mvp_article_ad')) { ?>
														<div id="article-ad">
															<?php $articlead = get_option('mvp_article_ad'); if ($articlead) { echo html_entity_decode($articlead); } ?>
														</div><!--article-ad-->
													<?php } ?>
													<?php comments_template(); ?>
												</div><!--content-main-->
											</div><!--content-area-in-->
											<?php $mvp_post_temp = get_post_meta($post->ID, "mvp_post_template", true); if ( $mvp_post_temp == "full" || $mvp_post_temp == "wide-full" ) { } else { ?>
												<?php get_sidebar(); ?>
											<?php } ?>
										</div><!--content-area-out-->
												</div><!--sec-marg-in-->
											</div><!--sec-marg-out-->
										</div><!--content-area-cont-->
									</div><!--content-area-->
								</div><!--post-body-in-->
							</div><!--post-body-out-->
						</div><!--post-area-->
					</article>
	</div><!--sec-marg-in4-->
</div><!--sec-marg-out4-->
				</div><!--content-wrap-->
				<?php $mvp_show_more = get_option('mvp_show_more'); if ($mvp_show_more == "true") { ?>
				<div class="content-bot-wrap left relative">
					<div class="sec-marg-out relative">
						<div class="sec-marg-in">
					<h4 class="post-header">
						<span class="post-header"><?php _e( 'More in', 'mvp-text' ); ?> <?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span>
					</h4>
				<div class="archive-list-wrap left relative">
					<div class="archive-list-out">
						<div class="archive-list-in">
							<div class="archive-list-left left relative">
								<ul class="archive-list">
									<?php $mvp_more_num = esc_html(get_option('mvp_more_num')); $category = get_the_category(); $current_cat = $category[0]->cat_ID; $recent = new WP_Query(array( 'cat' => $current_cat, 'posts_per_page' => $mvp_more_num, 'post__not_in' => array( $post->ID ) )); while($recent->have_posts()) : $recent->the_post(); ?>
									<li>
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="archive-list-img left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('medium-thumb'); ?>
												<?php if(get_post_meta($post->ID, "mvp_video_embed", true)): ?>
													<div class="feat-video">
														<i class="fa fa-video-camera fa-3"></i>
													</div><!--feat-video-->
												<?php endif; ?>
												</a>
												<div class="archive-list-share">
													<span class="archive-share-but left"><i class="fa fa-share-square-o fa-3"></i></span>
													<div class="archive-share-contain left relative">
														<span class="archive-share-fb"><a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fa fa-facebook fa-2"></i></a></span><span class="archive-share-twit"><a href="#" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fa fa-twitter fa-2"></i></a></span></span><span class="archive-share-pin"><a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumb' ); echo esc_url( $thumb['0'] ); ?>&amp;description=<?php the_title(); ?>', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post"><i class="fa fa-pinterest fa-2"></i></a></span>
													</div><!--archive-share-contain-->
												</div><!--archive-list-share-->
											</div><!--archive-list-img-->
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } else { ?>
											<div class="archive-list-text left relative">
												<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
												<p><?php echo excerpt(22); ?></p>
												<div class="archive-list-info left relative">
													<span class="archive-list-author left"><?php the_author(); ?></span><span class="archive-list-date left"><?php the_time(get_option('date_format')); ?></span>
												</div><!--archive-list-info-->
											</div><!--archive-list-text-->
										<?php } ?>
									</li>
									<?php endwhile; wp_reset_query(); ?>
								</ul>
							</div><!--archive-list-left-->
						</div><!--archive-list-in-->
						<?php $more_ad = get_option('mvp_more_ad'); if ($more_ad) { ?>
							<div class="archive-ad-wrap relative">
								<?php $more_ad = get_option('mvp_more_ad'); if ($more_ad) { echo html_entity_decode($more_ad); } ?>
							</div><!--archive-ad-wrap-->
						<?php } ?>
					</div><!--archive-list-out-->
				</div><!--archive-list-wrap-->
					</div><!--sec-marg-in-->
				</div><!--sec-marg-out-->
			</div><!--content-bot-wrap-->
		<?php } ?>
		<?php setCrunchifyPostViews(get_the_ID()); ?>
	<?php endwhile; endif; ?>
</div><!--main-content-contain-->
<?php get_footer(); ?>