<?php

	/* Template Name: Author List */

?>
<?php get_header(); ?>
<div id="main-content-contain" class="left relative">
	<div class="sec-marg-out relative">
		<div class="sec-marg-in">
			<div class="home-widget-wrap">
				<div class="archive-list-wrap left relative">
					<div class="archive-list-out">
						<div class="archive-list-in">
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								<h1 class="cat-head"><?php the_title(); ?></h1>
							<?php endwhile; endif; ?>
							<div class="archive-list-left left relative">
								<?php $allUsers = get_users('orderby=post_count&order=DESC'); $users = array(); foreach($allUsers as $currentUser) { if(!in_array( 'subscriber', $currentUser->roles )) { $users[] = $currentUser; } } foreach($users as $user) { ?>
								<div id="author-page-box" class="left relative">
									<div class="author-page-out">
										<div class="author-page-img left relative">
											<?php echo get_avatar( $user->user_email, '120' ); ?>
										</div><!--author-page-img-->
										<div class="author-page-in">
											<div class="author-page-text left relative">
												<h2 class="author-list-head"><a href="<?php echo get_author_posts_url( $user->ID ); ?>"><?php echo esc_html( $user->display_name ); ?></a></h2>
												<p><?php echo esc_html( $user->description ); ?></p>
												<ul class="author-social left relative">
													<?php $authordesc = $user->facebook; if ( ! empty ( $authordesc ) ) { ?>
														<li class="fb-item">
															<a href="<?php echo esc_url( $user->facebook ); ?>" alt="Facebook" class="fb-but" target="_blank"><i class="fa fa-facebook-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $user->twitter; if ( ! empty ( $authordesc ) ) { ?>
														<li class="twitter-item">
															<a href="<?php echo esc_url( $user->twitter ); ?>" alt="Twitter" class="twitter-but" target="_blank"><i class="fa fa-twitter-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $user->pinterest; if ( ! empty ( $authordesc ) ) { ?>
														<li class="pinterest-item">
															<a href="<?php echo esc_url( $user->pinterest ); ?>" alt="Pinterest" class="pinterest-but" target="_blank"><i class="fa fa-pinterest-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $user->googleplus; if ( ! empty ( $authordesc ) ) { ?>
														<li class="google-item">
															<a href="<?php echo esc_url( $user->googleplus ); ?>" alt="Google Plus" class="google-but" target="_blank"><i class="fa fa-google-plus-square fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $user->instagram; if ( ! empty ( $authordesc ) ) { ?>
														<li class="instagram-item">
															<a href="<?php echo esc_url( $user->instagram ); ?>" alt="Instagram" class="instagram-but" target="_blank"><i class="fa fa-instagram fa-2"></i></a>
														</li>
													<?php } ?>
													<?php $authordesc = $user->linkedin; if ( ! empty ( $authordesc ) ) { ?>
														<li class="linkedin-item">
															<a href="<?php echo esc_url( $user->linkedin ); ?>" alt="Linkedin" class="linkedin-but" target="_blank"><i class="fa fa-linkedin-square fa-2"></i></a>
														</li>
													<?php } ?>
												</ul>
											</div><!--author-page-text-->
										</div><!--author-page-in-->
									</div><!--author-page-out-->
								</div><!--author-page-box-->
								<?php } ?>
							</div><!--archive-list-left-->
						</div><!--archive-list-in-->
						<?php get_sidebar(); ?>
					</div><!--archive-list-out-->
				</div><!--archive-list-wrap-->
			</div><!--home-widget-wrap-->
		</div><!--sec-marg-in-->
	</div><!--sec-marg-out-->
</div><!--main-content-content-->
<?php get_footer(); ?>