<?php get_header(); ?>
<div class="sec-marg-out relative">
	<div class="sec-marg-in">
		<div id="main-content-contain" class="left relative infinite-content">
			<div id="content-wrap" class="left relative">
				<div id="post-area" class="left relative">
					<div id="content-area" class="left relative">
						<div class="content-main left relative">
							<div id="post-404" class="left relative">
								<h1><?php _e( 'Error', 'mvp-text' ); ?> 404!</h1>
								<?php _e( 'The page you requested does not exist or has moved.', 'mvp-text' ); ?>
							</div><!--post-404-->
						</div><!--content-main-->
					</div><!--content-area-->
				</div><!--post-area-->
			</div><!--content-wrap-->
		</div><!--main-content-contain-->
	</div><!--sec-marg-in-->
</div><!--sec-marg-out-->
<?php get_footer(); ?>